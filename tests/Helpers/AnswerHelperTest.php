<?php

use App\Helpers\AnswerHelper;
use App\Repositories\AnswerRepository;
use App\Repositories\StatRepository;

/**
 * Class AnswerHelperTest
 */
class AnswerHelperTest extends TestCase
{
    /**
     * ./bin/phpunit --filter AnswerHelperTest::testCheckAnswer
     *
     * @param array $expected   Ожидаемый результат
     * @param int   $answer_id  Идентификатор ответа
     * @param bool  $showAnswer Флаг показа ответа
     * @param bool  $showHint   Флаг показа подсказки
     *
     * @see          app\Helpers\AnswerHelper::checkAnswer
     * @dataProvider providerCheckAnswer
     */
    public function testCheckAnswer($expected, $answer_id, $showAnswer, $showHint)
    {
        $answerRepository = new AnswerRepository();
        $statRepository = $this->getMockBuilder(StatRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $helper = new AnswerHelper($answerRepository, $statRepository);
        $result = $helper->checkAnswer($answer_id, $showAnswer, $showHint);

        self::assertEquals($expected, $result);
    }

    /**
     * Provider данных пользователей
     *
     * @return array[]
     */
    public function providerCheckAnswer()
    {
        return [
            'when answer wrong and show answer and show hint' => [
                [
                    'right' => false,
                    'question' => 329,
                    'n' => 5,
                    'id' => 1183,
                    'answer' => 1184,
                    'hint' => 'Разметка <a href="/road-mark/#p1.17" target="_blank">1.17</a> (в виде желтой зигзагообразной линии) применяется для обозначения мест остановок маршрутных ТС и стоянок такси. В данной ситуации вы можете остановиться для посадки или высадки пассажиров в обозначенной разметкой <a href="/road-mark/#p1.17" target="_blank">1.17</a> зоне, если не создадите помех движению маршрутных автобусов или троллейбусов.',
                ],
                1183,
                true,
                true,
            ],
            'when answer wrong and show answer' => [
                [
                    'right' => false,
                    'question' => 329,
                    'n' => 5,
                    'id' => 1183,
                    'answer' => 1184,
                ],
                1183,
                true,
                false,
            ],
            'when answer wrong' => [
                [
                    'right' => false,
                    'question' => 329,
                    'n' => 5,
                    'id' => 1183,
                ],
                1183,
                false,
                false,
            ],
            'when answer right' => [
                [
                    'right' => true,
                    'question' => 329,
                    'n' => 5,
                    'id' => 1184,
                ],
                1184,
                false,
                false,
            ],
        ];
    }
}
