<?php

use App\Helpers\UserHelper;
use App\Models\School;
use App\Models\User;
use App\Repositories\UserRepository;

/**
 * Class UserHelperTest
 */
class UserHelperTest extends TestCase
{
    /**
     * ./bin/phpunit --filter UserHelperTest::testSetSchool
     *
     * @return void
     *
     * @see app\Helpers\UserHelper::setSchool
     * @dataProvider providerUserSetSchoolData
     */
    public function testSetSchool($expected, $user, $saved)
    {
        $repository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['setSchoolToUser', 'getByIds'])
            ->getMock();

        $repository->method('setSchoolToUser')->willReturn($saved);
        $repository->method('getByIds')->willReturn(new User());

        $helper = new UserHelper($repository);
        $school = new School();
        $data = $helper->setSchool($user, $school);

        self::assertEquals($expected, $data);
    }

    /**
     * Provider данных пользователей
     *
     * @return array[]
     */
    public function providerUserSetSchoolData()
    {
        return [
            'when user is array of ids is true'                   => [
                true,
                [1],
                true
            ],
            'when user is id is true'                             => [
                true,
                1,
                true
            ],
            'when user is object and is instanceof is true'       => [
                true,
                new User(),
                true
            ],
            'when user is object and is not instance of is false' => [
                false,
                new School(),
                true
            ],
            'when user is string is false'                        => [
                false,
                'string',
                true
            ],
            'when user is null is false'                          => [
                false,
                null,
                true
            ],
            'when user is bool is false'                          => [
                false,
                false,
                true
            ],
            'when user is array of strings is false'                          => [
                false,
                ['asd', 'asd'],
                false
            ],
        ];
    }
}
