<?php

use App\Helpers\StatHelper;
use App\Models\User;
use App\Repositories\AnswerRepository;

/**
 * Class StatHelperTest
 */
class StatHelperTest extends TestCase
{
    const EXPECTED = [
        'count_answers' =>
            [
                'right' => 44,
                'wrong' => 49,
            ],
        'complete_questions' =>
            [
                'right' => 1,
                'wrong' => 1,
            ],
        'topWrongAnswer' =>
            [
                0 =>
                    [
                        'question_id' => 285,
                        'question_title' => 'В каком случае водитель совершит вынужденную остановку?',
                        'question_n' => 1,
                        'wrong_answer_text' => 'Остановившись непосредственно перед пешеходным переходом, чтобы уступить дорогу пешеходу',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 3,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                1 =>
                    [
                        'question_id' => 288,
                        'question_title' => 'Этот знак разрешает Вам ставить на стоянку легковой автомобиль с использованием тротуара:',
                        'question_n' => 4,
                        'wrong_answer_text' => 'Только на правой стороне дороги до ближайшего по ходу движения перекрестка.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 3,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                2 =>
                    [
                        'question_id' => 287,
                        'question_title' => 'Этот дорожный знак указывает',
                        'question_n' => 3,
                        'wrong_answer_text' => 'Расстояние до конца тоннеля',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                3 =>
                    [
                        'question_id' => 294,
                        'question_title' => 'С какой скоростью Вы можете продолжить движение вне населенного пункта по левой полосе на легковом автомобиле?',
                        'question_n' => 10,
                        'wrong_answer_text' => 'Не более 50 км/ч.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                4 =>
                    [
                        'question_id' => 291,
                        'question_title' => 'Обязаны ли Вы в данной ситуации подать сигнал правого поворота?',
                        'question_n' => 7,
                        'wrong_answer_text' => 'Да.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                5 =>
                    [
                        'question_id' => 315,
                        'question_title' => 'Разрешено ли Вам обогнать мотоциклиста?',
                        'question_n' => 11,
                        'wrong_answer_text' => 'Разрешено.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 2,
                    ],
                6 =>
                    [
                        'question_id' => 286,
                        'question_title' => 'В каких направлениях Вам разрешено продолжить движение?',
                        'question_n' => 2,
                        'wrong_answer_text' => 'В любых',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                7 =>
                    [
                        'question_id' => 289,
                        'question_title' => 'Эта разметка, нанесенная на полосе движения:',
                        'question_n' => 5,
                        'wrong_answer_text' => 'Предоставляет Вам преимущество при перестроении на правую полосу.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 2,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                8 =>
                    [
                        'question_id' => 293,
                        'question_title' => 'По какой траектории Вам разрешено выполнить разворот?',
                        'question_n' => 9,
                        'wrong_answer_text' => 'Только по Б.',
                        'wrong_answer_n' => 2,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
                9 =>
                    [
                        'question_id' => 314,
                        'question_title' => 'В каких случаях Вы можете наезжать на прерывистые линии разметки, разделяющие проезжую часть на полосы движения?',
                        'question_n' => 10,
                        'wrong_answer_text' => 'Только при движении в темное время суток.',
                        'wrong_answer_n' => 2,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 2,
                    ],
                10 =>
                    [
                        'question_id' => 317,
                        'question_title' => 'Вы намерены повернуть налево. Кому следует уступить дорогу?',
                        'question_n' => 13,
                        'wrong_answer_text' => 'Только пешеходам.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 2,
                    ],
                11 =>
                    [
                        'question_id' => 328,
                        'question_title' => 'Какие из знаков устанавливают в начале дороги с односторонним движением?',
                        'question_n' => 4,
                        'wrong_answer_text' => 'Только А.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 3,
                    ],
                12 =>
                    [
                        'question_id' => 809,
                        'question_title' => 'Эта разметка обозначает:',
                        'question_n' => 5,
                        'wrong_answer_text' => 'Номер дороги или маршрута.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 27,
                    ],
                13 =>
                    [
                        'question_id' => 933,
                        'question_title' => 'Разрешено ли Вам выполнить разворот по указанным траекториям?',
                        'question_n' => 9,
                        'wrong_answer_text' => 'Разрешено только по Б.',
                        'wrong_answer_n' => 2,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 33,
                    ],
                14 =>
                    [
                        'question_id' => 501,
                        'question_title' => 'Вы можете использовать противотуманные фары совместно с ближним или дальним светом фар:',
                        'question_n' => 17,
                        'wrong_answer_text' => 'Только в условиях недостаточной видимости.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 11,
                    ],
                15 =>
                    [
                        'question_id' => 813,
                        'question_title' => 'По какой траектории Вы можете выполнить разворот?',
                        'question_n' => 9,
                        'wrong_answer_text' => 'Только по Б.',
                        'wrong_answer_n' => 2,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 27,
                    ],
                16 =>
                    [
                        'question_id' => 504,
                        'question_title' => 'Административная ответственность установлена за нарушение Правил дорожного движения или правил эксплуатации транспортного средства, повлекшее причинение:',
                        'question_n' => 20,
                        'wrong_answer_text' => 'Легкого вреда здоровью человека либо незначительного материального ущерба.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 11,
                    ],
                17 =>
                    [
                        'question_id' => 1049,
                        'question_title' => 'Увеличение длины штриха прерывистой линии разметки информирует Вас:',
                        'question_n' => 5,
                        'wrong_answer_text' => 'О начале зоны, где запрещены любые маневры.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 39,
                    ],
                18 =>
                    [
                        'question_id' => 1030,
                        'question_title' => 'В каком случае водитель автомобиля имеет преимущество перед другими участниками движения?',
                        'question_n' => 6,
                        'wrong_answer_text' => 'Только при включенном проблесковом маячке синего или бело-лунного цвета.',
                        'wrong_answer_n' => 1,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 38,
                    ],
                19 =>
                    [
                        'question_id' => 295,
                        'question_title' => 'Может ли водитель легкового автомобиля в населенном пункте выполнить опережение грузовых автомобилей по такой траектории?',
                        'question_n' => 11,
                        'wrong_answer_text' => 'Нет.',
                        'wrong_answer_n' => 2,
                        'wrong_answer_count' => 1,
                        'category_id' => 1,
                        'subject_id' => 1,
                        'ticket_id' => 1,
                    ],
            ],
        'topWrongQuestion' =>
            [
                2 => 1,
            ],
    ];

    const DATA = [
        'count_answers' => [
            'right' => 44,
            'wrong' => 49
        ],
        'complete_questions' => [
            'right' => 1,
            'wrong' => 1
        ],
        'topWrongAnswer' => [
            328 => [
                'answer_id' => 1179,
                'count' => 1
            ],
            809 => [
                'answer_id' => 2587,
                'count' => 1
            ],
            1030 => [
                'answer_id' => 3229,
                'count' => 1
            ],
            1049 => [
                'answer_id' => 3288,
                'count' => 1
            ],
            504 => [
                'answer_id' => 1700,
                'count' => 1
            ],
            813 => [
                'answer_id' => 2599,
                'count' => 1
            ],
            501 => [
                'answer_id' => 1690,
                'count' => 1
            ],
            933 => [
                'answer_id' => 2947,
                'count' => 1
            ],
            285 => [
                'answer_id' => 1058,
                'count' => 3
            ],
            286 => [
                'answer_id' => 1062,
                'count' => 2
            ],
            287 => [
                'answer_id' => 1065,
                'count' => 2
            ],
            288 => [
                'answer_id' => 1066,
                'count' => 3
            ],
            289 => [
                'answer_id' => 1069,
                'count' => 2
            ],
            315 => [
                'answer_id' => 1144,
                'count' => 2
            ],
            317 => [
                'answer_id' => 1148,
                'count' => 1
            ],
            314 => [
                'answer_id' => 1141,
                'count' => 1
            ],
            291 => [
                'answer_id' => 1075,
                'count' => 2
            ],
            293 => [
                'answer_id' => 1081,
                'count' => 1
            ],
            294 => [
                'answer_id' => 1083,
                'count' => 2
            ],
            295 => [
                'answer_id' => 1087,
                'count' => 1
            ]
        ],
        'topWrongQuestion' => [2 => 1]
    ];

    /**
     * ./bin/phpunit --filter StatHelperTest::testFillQuestions
     *
     * @return void
     *
     * @see app\Helpers\StatHelper::fillQuestions

     */
    public function testFillQuestions()
    {
        $repository = $this->getMockBuilder(AnswerRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAnswersByIds'])
            ->getMock();

        $user = new User();
        $collection = new \Illuminate\Database\Eloquent\Collection();
        $collection->add($user);
        $collection->add($user);

        $repository->method('getAnswersByIds')->willReturn($collection);

        $class = new StatHelper();
        $result = $this->invokeMethod($class, 'fillQuestions', [self::DATA]);

        self::assertEquals(self::EXPECTED, $result);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
