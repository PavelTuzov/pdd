<?php

use App\Helpers\QuestionHelper;
use App\Repositories\QuestionRepository;
use App\Repositories\StatRepository;

/**
 * Class AnswerHelperTest
 */
class QuestionHelperTest extends TestCase
{
    /**
     * Количество билетов
     */
    const COUNT_OF_TICKETS = 40;

    /**
     * Первый билет
     */
    const FIRST_OF_TICKETS = 1;

    /**
     * ./bin/phpunit --filter QuestionHelperTest::testGetStat
     *
     * @see          app\Helpers\QuestionHelper::getStat
     */
    public function testGetStat()
    {
        $questionRepository = new QuestionRepository();
        $statRepository = $this->getMockBuilder(StatRepository::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $questions = $questionRepository->getTickets();
        $helper = new QuestionHelper();
        $result = $helper::getStat($questions, $statRepository);

        self::assertObjectHasAttribute('stat', $result[self::FIRST_OF_TICKETS]);
        self::assertObjectHasAttribute('stat', $result[self::COUNT_OF_TICKETS]);
    }
}
