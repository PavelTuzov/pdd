<?php
Route::get('test', 'Controller@test')->name('test');

Route::get('question/{id}', 'QuestionController@view')->name('question.item');
Route::post('question/timeout', 'QuestionController@timeOut')->name('question.timeout');
Route::get('tickets', 'QuestionController@tickets')->name('tickets');
Route::get('tickets/{id}', 'QuestionController@ticket')->name('tickets.question');
Route::get('examination/', 'QuestionController@exam')->name('examination');
Route::get('errors', 'QuestionController@errorsPage')->name('errors.page');
Route::get('top100hardest', 'QuestionController@top100')->name('top100');
Route::post('getErrors', 'QuestionController@getErrors')->name('errors.get');
Route::get('rules/{n?}', 'ReferenceController@rules')->name('rules');
Route::get('rule/{n}', 'ReferenceController@rule')->name('rules.item');
Route::get('subject/{id}', 'QuestionController@subject')->name('question.subject');
Route::get('signs/{category_id?}', 'SignController@category')->name('signs');
Route::get('sign/{id}', 'SignController@item')->name('sign.item');

//Route::get('/', 'PageController@index')->name('index');
Route::get('/', 'PageController@index')->name('home');

Route::post('additional', 'QuestionController@requestAdditionalQuestions')->name('question.additional');
Route::post('/stats/save', 'StatController@save')->name('stat.save');
Route::post('answers/{id}', 'AnswerController@checkAnswer')->name('answer.get');
Route::get('/cities/list', 'CityController@getList')->name('city.list');
Route::get('/school/list/{city_id}', 'SchoolController@getList')->name('school.list');

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'user'], function () {
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('stat', 'UserController@stat')->name('user.stat');
    Route::get('settings', 'UserController@edit')->name('user.settings');
    Route::get('options', 'UserController@options')->name('user.options');
    Route::post('save', 'UserController@save')->name('user.save');
    Route::post('save_options', 'UserController@saveOptions')->name('user.save_options');

});

Route::group(['middleware' => 'manager', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('tickets', 'AdminController@tickets')->name('admin.tickets');
    Route::get('tickets/{id}/', 'AdminController@ticket')->name('admin.ticket');
    Route::get('questions/{ticket_id?}', 'AdminController@questions')->name('admin.questions');
    Route::get('question/{ticket_id?}/{id?}', 'AdminController@question')->name('admin.question');
    Route::post('question/{ticket_id?}/{id?}', 'AdminController@question')->name('admin.question.post');
    Route::post('changeAnswerOrder', 'AdminController@changeAnswerOrder')->name('admin.question.order');
    Route::post('save_question/{id?}', 'AdminController@saveQuestion')->name('admin.question.save');

    Route::get('reference/{id?}', 'AdminController@reference')->name('admin.reference.item');
    Route::get('references/{id?}', 'AdminController@references')->name('admin.reference.list');
    Route::post('save_reference/{id?}', 'AdminController@saveReference')->name('admin.reference.save');
    Route::get('get_reference', 'AdminController@getReferenceList')->name('admin.reference.json');
    Route::post('get_referenceById', 'AdminController@getReferenceById')->name('admin.reference_by_id.json');

    Route::get('sign/{id?}', 'AdminController@sign')->name('admin.sign.item');
    Route::get('signs/{id?}', 'AdminController@signs')->name('admin.sign.list');
    Route::post('save_sign/{id?}', 'AdminController@saveSign')->name('admin.sign.save');
    Route::get('get_signs_by_query', 'AdminController@getSignsByQuery')->name('admin.sign.query');
    Route::get('get_rules_by_query', 'AdminController@getRulesByQuery')->name('admin.rules.query');

    Route::get('stat/{period?}', 'AdminController@stat')->name('admin.stat.index');
});

Route::group(['middleware' => 'manager'], function() {
    Route::get('/user/edit/{id}', 'UserController@edit')->name('user.edit');
});

Route::group(['middleware' => 'manager', 'prefix' => 'bot'], function() {
    Route::get('/getme', 'TelegramController@getMe')->name('bot.me');
    Route::get('/getupdate', 'TelegramController@getUpdate')->name('bot.update');
    Route::get('/hook/add', 'TelegramController@addHook')->name('bot.hook.add');
    Route::get('/hook/remove', 'TelegramController@removeHook')->name('bot.hook.remove');
    Route::get('/hook/info', 'TelegramController@getWebHookAnswer')->name('bot.hook.info');
});

Route::post('/bot/hook/{token}', 'TelegramController@webHook')->name('bot.hook.post');
