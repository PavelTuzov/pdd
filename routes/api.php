<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([], function () {
    Route::post('/auth/app', 'Api\AuthController@authenticateApp');
    Route::get('application-data', 'Api\HomeController@appData')->middleware('auth.api.app');
    Route::get('/', 'AdminController@index')->name('api.auth');
    Route::post('/auth/user', 'Api\AuthController@authenticateUser')->middleware('auth.api.app');
    Route::post('/auth/user/logout', 'Api\AuthController@logoutUser')->middleware('auth.api.user');

    Route::get('/tickets', 'Api\TicketController@tickets')->middleware('auth.api.app');
});
