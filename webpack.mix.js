let mix = require('laravel-mix');
//home/pavel/PhpstormProjects/inpdd/resources/assets/less/../semantic/src/themes/themes/default/assets/fonts/brand-icons.eot
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var path = require("path");
var webpack = require("webpack");
var semantic = require('./semantic.json');

mix.js(
    [
        'resources/assets/js/bootstrap.js',
        'resources/assets/js/app.js'
    ],
    'public/js/app.js'
)
.sass(
    'resources/assets/sass/app.scss',
    'public/css/app.css'
);
/*

mix.styles([
    '../../public/js/components/semantic/dist/components/reset.min.css',
    '../../public/js/components/semantic/dist/components/site.min.css',
    '../../public/js/components/semantic/dist/components/button.min.css',
    '../../public/js/components/semantic/dist/components/container.min.css',
    '../../public/js/components/semantic/dist/components/divider.min.css',
    //'../../public/js/components/semantic/dist/components/dimmer.min.css',
    //'../../public/js/components/semantic/dist/components/checkbox.min.css',
    '../../public/js/components/semantic/dist/components/dropdown.min.css',
    '../../public/js/components/semantic/dist/components/form.min.css',
    '../../public/js/components/semantic/dist/components/grid.min.css',
    //'../../public/js/components/semantic/dist/components/header.min.css',
    //'../../public/js/components/semantic/dist/components/image.min.css',
    '../../public/js/components/semantic/dist/components/input.min.css',
    //'../../public/js/components/semantic/dist/components/icon.css',
    //'../../public/js/components/semantic/dist/components/item.min.css',
    '../../public/js/components/semantic/dist/components/card.min.css',
    '../../public/js/components/semantic/dist/components/label.min.css',
    //'../../public/js/components/semantic/dist/components/list.min.css',
    //'../../public/js/components/semantic/dist/components/loader.min.css',
    //'../../public/js/components/semantic/dist/components/menu.min.css',
    '../../public/js/components/semantic/dist/components/message.min.css',
    //'../../public/js/components/semantic/dist/components/modal.min.css',
    //'../../public/js/components/semantic/dist/components/popup.min.css',
    //'../../public/js/components/semantic/dist/components/search.min.css',
    '../../public/js/components/semantic/dist/components/segment.min.css',
    //'../../public/js/components/semantic/dist/components/sticky.min.css',
    '../../public/js/components/semantic/dist/components/tab.min.css',
    //'../../public/js/components/semantic/dist/components/table.min.css',
    //'../../public/js/components/semantic/dist/components/transition.min.css',
    '../../public/js/components/semantic/dist/components/icon.min.css',
    '../../public/js/components/alertify.js/themes/alertify.core.css',
    '../../public/js/components/alertify.js/themes/alertify.default.css',
    '../../public/js/components/nprogress/nprogress.css',
    'css/!*!/!*.css',
    'css/!*!/!*!/!*.css',
    'css/!*.css',
], 'public/css/app.css', 'resources/assets')
//.less('app.less')
    .scripts([
        '../../public/js/components/jquery/dist/jquery.min.js',
        '../../public/js/components/semantic/dist/components/dropdown.min.js',
        '../../public/js/components/semantic/dist/components/checkbox.min.js',
        '../../public/js/components/semantic/dist/components/form.min.js',
        //'../../public/js/components/semantic/dist/components/modal.min.js',
        //'../../public/js/components/semantic/dist/components/dimmer.min.js',
        //'../../public/js/components/semantic/dist/components/popup.min.js',
        '../../public/js/components/semantic/dist/components/search.min.js',
        '../../public/js/components/semantic/dist/components/site.min.js',
        '../../public/js/components/semantic/dist/components/state.min.js',
        //'../../public/js/components/semantic/dist/components/sticky.min.js',
        '../../public/js/components/semantic/dist/components/tab.min.js',
        //'../../public/js/components/semantic/dist/components/transition.min.js',
        //'../../public/js/components/semantic/dist/components/visibility.min.js',
        '../../public/js/components/semantic/dist/components/api.min.js',
        '../../public/js/components/semantic/dist/components/search.min.js',
        '../../public/js/components/alertify.js/lib/alertify.min.js',
        '../../public/js/components/nprogress/nprogress.js',
        '../../public/js/components/hammerjs/hammer.js',
        'js/config.js',
        'js/router.js',
        'js/app.js'
    ], 'public/js/app.js', 'resources/assets')
    .version(["public/css/app.css", "public/js/app.js"]);

*/
