var
    gulp  = require('gulp'),
    watch = require('./resources/assets/semantic/tasks/watch'),
    build = require('./resources/assets/semantic/tasks/build'),
    semantic = require('./semantic.json')
;
// import task with a custom task name
gulp.task('build', function() {
    gulp.start('build-ui');
    gulp.src([semantic.paths.output.themes + '/**/*']).pipe(gulp.dest('./public/css/themes'));
});
gulp.task('build-ui', build);
gulp.task('watch-ui', watch);
