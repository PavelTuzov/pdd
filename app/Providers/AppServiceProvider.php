<?php

namespace App\Providers;

use App\Helpers\CityHelper;
use App\Helpers\MenuHelper;
use App\Helpers\SettingsHelper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param CityHelper $cities
     * @param SettingsHelper $settings
     * @return void
     */
    public function boot(CityHelper $cities, SettingsHelper $settings)
    {
        $city_id = $cities::getCurrentCityId();
        $city = $cities->getCityById($city_id);

        view()->share('city_id', $city_id);
        view()->share('city', $city);
        view()->share('cities', $cities->getCities());
        view()->share('settings', $settings);
        //view()->share('schools', $schools->getSchools());
        view()->share('menu', MenuHelper::getMenu(MenuHelper::$mainLinks));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
