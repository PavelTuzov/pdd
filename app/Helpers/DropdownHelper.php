<?php

namespace App\Helpers;

class DropdownHelper
{
    /**
     * Возвращает список для dropdown элементов
     *
     * @param array  $data  Данные для ответа
     * @param string $name  Имя
     * @param string $value Значение
     * @param string $text  Текст
     *
     * @return array
     */
    public static function generateList($data, $name = 'name', $value = 'id', $text = 'name')
    {
        $success = false;
        $results = [];

        if (count($data) > 0) {
            $success = true;

            foreach ($data as $item) {
                $results[] = [
                    'name' => $item->$name,
                    'value' => $item->$value,
                    'text' => $item->$text
                ];
            }
        }

        return [
            'success' => $success,
            'results' => $results
        ];
    }
}