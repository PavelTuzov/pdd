<?php

namespace App\Helpers;

use App\Repositories\RewardRepository;

/**
 * Class RewardHelper
 */
class RewardHelper
{
    /**
     * Репозиторий наград
     *
     * @var RewardRepository
     */
    private $rewardRepository;

    /**
     * RewardHelper constructor.
     *
     * @param RewardRepository $rewardRepository Репозиторий наград
     */
    public function __construct(RewardRepository $rewardRepository)
    {
        $this->rewardRepository = $rewardRepository;
    }
}
