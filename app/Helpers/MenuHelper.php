<?php

namespace App\Helpers;

use App\Models\Entity\MenuItem;
use Route;

/**
 * Class MenuHelper
 */
class MenuHelper
{
    /**
     * Массив ссылок
     *
     * @var array
     */
    public static $mainLinks = [
        [
            'url' => '',
            'title' => 'Главная',
            'name' => 'home',
            'icon' => 'home'
        ],
        [
            'url' => '/examination',
            'name' => 'examination',
            'title' => 'Экзамен',
            'active' => false,
            'icon' => 'student'
        ],
        [
            'url' => '/tickets',
            'name' => 'tickets',
            'title' => 'Билеты',
            'active' => false,
            'icon' => 'tasks'
        ],
        [
            'url' => '/rules',
            'name' => 'rules',
            'title' => 'Правила',
            'active' => false,
            'icon' => 'book'
        ],
        [
            'url' => '/signs',
            'name' => 'signs',
            'title' => 'Знаки',
            'active' => false,
            'icon' => 'map signs'
        ]
    ];

    /**
     * Ссылки раздела пользователя
     *
     * @var array
     */
    public static $userLinks = [
        [
            'url' => '/user/settings',
            'name' => 'user.settings',
            'title' => 'Настройки аккаунта',
            'active' => false,
            'icon' => 'setting'
        ],
        [
            'url' => '/user/options',
            'name' => 'user.options',
            'title' => 'Опции',
            'active' => false,
        ],
        [
            'url' => '/user/history',
            'name' => 'user.history',
            'title' => 'История',
            'active' => false,
        ]
    ];

    /**
     * Массив объектов меню
     *
     * @var array
     */
    private $menu;

    /**
     * Возвращает сгенерированное меню
     *
     * @param array $links Массив элементов меню
     *
     * @return array
     */
    public static function getMenu($links)
    {
        $menu = [];
        foreach ($links as $menuItem) {
            $menu[] = new MenuItem($menuItem);
        }

        return self::checkActive($menu);
    }

    /**
     * Устанавливает активный пункт меню
     *
     * @param array $menu Меню
     *
     * @return mixed
     */
    private static function checkActive($menu)
    {
        foreach ($menu as $k => $item) {
            $menu[$k]->setActive(true);
        }

        return $menu;
    }
}
