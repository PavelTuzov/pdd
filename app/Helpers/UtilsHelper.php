<?php

namespace App\Helpers;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UtilsHelper
 */
class UtilsHelper
{
    const PERIOD_ONE_MONTH = 'monthly';
    const PERIOD_ONE_DAY = 'daily';
    const PERIOD_ONE_HOUR = 'hourly';

    /**
     * Возвращает 404
     *
     * @return NotFoundHttpException
     */
    public static function err404()
    {
        return abort(404);
    }

    /**
     * Генерирует строку
     *
     * @param int $length Длина
     *
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Возвращает период из даты
     *
     * @param array $params Параметры
     *
     * @return mixed
     */
    public static function getPeriodFromData($params)
    {
        if ($params['offsetUnit'] === 'subMonth' && $params['offset'] === 1) {
            return self::PERIOD_ONE_MONTH;
        }

        if ($params['offsetUnit'] === 'subDays' && $params['offset'] === 1) {
            return self::PERIOD_ONE_DAY;
        }

        if ($params['offsetUnit'] === 'subHour' && $params['offset'] === 1) {
            return self::PERIOD_ONE_HOUR;
        }

        return null;
    }

    /**
     * Функция для сортировки usort
     *
     * @param $a
     * @param $b
     *
     * @return mixed
     */
    public static function sort($a, $b)
    {
        return $b['wrong_answer_count'] - $a['wrong_answer_count'];
    }
}
