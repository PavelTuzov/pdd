<?php

namespace App\Helpers;

use App\Repositories\CityRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Класс по работе с городами
 */
class CityHelper
{
    /**
     * Идентификатор города по-умолчанию
     */
    const DEFAULT_CITY = 1;

    /**
     * CityHelper constructor.
     *
     * @param CityRepository $repository Репозиторий городов
     */
    public function __construct(CityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Возвращает коллекцию городов
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCities()
    {
        if (env('CACHE_DRIVER') === 'memcached') {
            return Cache::tags(['cities'])->rememberForever(
                'cities',
                function () {
                    return $this->repository->getAllCities();
                }
            );
        }

        return $this->repository->getAllCities();
    }

    /**
     * Возвращает идентификатор текущего города
     *
     * @return int
     */
    public static function getCurrentCityId()
    {
        return session('city_id', self::DEFAULT_CITY);
    }

    /**
     * Возвращает город по идентификатору
     *
     * @param int $cityId Идентификатор города
     *
     * @return \App\Models\City
     */
    public function getCityById($cityId)
    {
        return $this->repository->getCityById($cityId);
    }
}
