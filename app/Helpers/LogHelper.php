<?php

namespace App\Helpers;

use Monolog\Logger;

/**
 * Class LogHelper
 */
class LogHelper
{
    /**
     * Уровень инфо
     */
    const DEBUG_LEVEL_INFO = 'info';

    /**
     * Уровень дебаг
     */
    const DEBUG_LEVEL_DEBUG = 'debug';

    /**
     * Монолог
     *
     * @var Logger
     */
    public $logger;

    /**
     * LogHelper constructor.
     */
    public function __construct()
    {
        $this->logger = new Logger(getenv('APP_DEBUG') ? self::DEBUG_LEVEL_DEBUG : self::DEBUG_LEVEL_INFO);
    }

    /**
     * Логирует в дебаг
     *
     * @param mixed $data Данные для логгирования
     *
     * @return void
     */
    public function debug($data)
    {
        $this->logger->debug($data);
    }

    /**
     * Логирует в инфо
     *
     * @param array $data Данные для логгирования
     *
     * @return void
     */
    public function info($data)
    {
        $this->logger->info($data);
    }
}
