<?php

namespace App\Helpers;

use App\Models\Stat;
use App\Models\StatAnswer;
use App\Repositories\AnswerRepository;
use App\Repositories\StatRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class QuestionHelper
 */
class AnswerHelper
{

    /**
     * Репозиторий ответов
     *
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * Репозиторий статистики
     *
     * @var StatRepository
     */
    private $statRepository;

    /**
     * AnswerHelper constructor.
     *
     * @param AnswerRepository $answerRepository Репозиторий ответов
     * @param StatRepository   $statRepository   Репозиторий статистики
     */
    public function __construct(AnswerRepository $answerRepository, StatRepository $statRepository)
    {
        $this->answerRepository = $answerRepository;
        $this->statRepository = $statRepository;
    }

    /**
     * Проверяет ответ
     *
     * @param int  $id         Идентификатор ответа
     * @param bool $showAnswer Показывать ответ?
     * @param bool $showHint   Показывать подсказку?
     *
     * @return array
     */
    public function checkAnswer($id, $showAnswer, $showHint)
    {
        $with = 'question';
        if ($showAnswer === true) {
            $with = ['question', 'withAnswer'];
            if ($showHint === true) {
                $with = ['question', 'withAnswer'];
            }
        }

        $answer = $this->answerRepository->checkAnswer($id, $with);
        $return = [
            'right' => $answer->right,
            'question' => $answer->question->id,
            'n' => $answer->question->n,
            'id' => $answer->id,
        ];

        if ($showAnswer === true) {
            $return['answer'] = $answer->withAnswer->id;

            if ($showHint === true) {
                $return['hint'] = $answer->question->hint;
            }
        }

        return $return;
    }

    /**
     * Сохраняет данные об ответе
     *
     * @param array $data Данные об ответе
     *
     * @return bool
     */
    public function saveAnswerStat($data)
    {
        $date = new Carbon();
        $stat = new StatAnswer();
        $stat->is_right = $data['right'];
        $stat->answer_id = $data['id'];
        $stat->question_id = $data['question'];
        $stat->user_id = Auth::check() !== false ? Auth::user()->id : null;
        $stat->date = $date->toDateTimeString();

        return $stat->save();
    }
}
