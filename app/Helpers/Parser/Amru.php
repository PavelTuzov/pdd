<?php

namespace App\Helpers\Parser;

use App\Models\Parsed\Sign;
use App\Models\Parsed\SignCategory;
use GuzzleHttp\Client;
use App\Models\Parsed\Question;
use GuzzleHttp\Cookie\FileCookieJar;
use Monolog\Logger;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Amru
 */
class Amru implements Resource {

    /**
     * Идентификатор ответа
     *
     * @var
     */
    private $answer_id = 1;

    /**
     * Идентификатор объекта
     *
     * @var int
     */
    private $object_id = 1;

    /**
     * Идентификатор типа
     *
     * @var int
     */
    private $type_id = 1;

    /**
     * Идентификатор категории
     *
     * @var int
     */
    private $category_id = 1;

    /**
     * Данные
     *
     * @var array
     */
    private $data;

    /**
     * Клиент
     *
     * @var Client
     */
    private $client;

    /**
     * Итерация
     *
     * @var int
     */
    private $iteration;

    /**
     * Результирующий массив
     *
     * @var array
     */
    private $models;

    /**
     * Лимит итераций
     *
     * @var int
     */
    private $limit = 40;

    /**
     * Логер
     *
     * @var Logger
     */
    private $logger;

    /**
     * Html парсер
     *
     * @var Crawler
     */
    private $crawler;

    /**
     * Ссылка на ресурс
     *
     * @var string
     */
    const URL = 'https://pdd.am.ru';

    /**
     * Шаблон ссылки к вопросу
     *
     * @var string
     */
    const QUESTION_URL_TEMPLATE = '%s/int/controls/get-question/?category=%s&type=%s&object=%s';

    /**
     * Шаблон ссылки к правилам
     *
     * @var string
     */
    const RULES_URL_TEMPLATE = '%s/rules/';

    /**
     * Шаблон ссылки к изображениям знаков
     *
     * @var string
     */
    const IMAGES_SIGNS_URL_TEMPLATE = '%s/pdd/sign/%s/%s';

    /**
     * Существующие размеры изображений
     *
     * @var array
     */
    const IMAGES_SIZES = [
        'IMAGE_SIZE_XL' => 'xl',
        'IMAGE_SIZE_L'  => 'l',
        'IMAGE_SIZE_M'  => 'm',
        'IMAGE_SIZE_S'  => 's',
        'IMAGE_SIZE_XS' => 'xs'
    ];

    /**
     * Требуемый размер изрбражения
     *
     * @var string
     */
    const IMAGE_SIZE_NEED = 'IMAGE_SIZE_XL';

    /**
     * Шаблон ссылки к знакам
     *
     * @var string
     */
    const SIGNS_URL_TEMPLATE = '%s/road-signs/';

    /**
     * Xpath элементов знаков
     *
     * @var array
     */
    const SIGN_BLOCKS = [
        'SIGN_CATEGORY'    => '//div[contains(@class, "pdd-index-blocks__row")]/a[contains(@class, "pdd-index-block")]',
        'SIGN_N'           => '//td[1]',
        'SIGN_BLOCK'       => '//tr[contains(@class, "au-accordion-trigger")]',
        'SIGN_DESCRIPTION' => '//div[contains(@class, "au-accordion__target")]',
        'SIGN_TITLE'       => '//h3[contains(@class, "au-accordion__header")]'
    ];

    /**
     * Amru constructor
     *
     * @param Client $client Клиент
     * @param Logger $logger Логер
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * Устанавливает экземпляр парсера
     *
     * @param Crawler $crawler экземпляр парсера
     *
     * @return void
     */
    public function setCrawler($crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * Получает правила
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getRules()
    {
        return $this->getRulesData();
    }

    /**
     * Получает информацию по знакам
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function getSigns()
    {
        $this->getSignsData();
        $this->fillSignsData();

        return $this->models;

    }

    /**
     * Получает содержимое страницы знаков
     *
     * @return string
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    private function getSignsData()
    {
        $response = $this->client->request('GET', $this->getSignsPath());
        $body = $response->getBody()->getContents();

        $this->data = $this->parseSigns($body);
    }


    /**
     * Получает содержимое страницы правил
     *
     * @return string
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    private function getRulesData()
    {
        $response = $this->client->request('GET', $this->getRulesPath());
        $body = $response->getBody()->getContents();

        return $this->parseRules($body);
    }

    /**
     * Получает содержимое страницы
     *
     * @param string $url Содержимое страницы
     *
     * @return string
     * @throws \RuntimeException
     */
    private function getPageData($url)
    {
        $response = $this->client->request('GET', $url);

        return $response->getBody()->getContents();
    }

    /**
     * Парсит страницу со знаками
     *
     * @param string $html Страница
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     *
     * @return array
     */
    private function parseSigns($html)
    {
        $data = [];
        $this->crawler->addHtmlContent($html);
        $links = $this->crawler->filterXPath(self::SIGN_BLOCKS['SIGN_CATEGORY'])->each(function (Crawler $node, $i) {
            return [
                'n' => $i,
                'title' => trim($node->text()),
                'url'   =>  self::URL . $node->attr('href'),
                'image' => $this->changeImageSize($node->filter('img')->attr('src'), self::IMAGES_SIZES[self::IMAGE_SIZE_NEED]),
            ];
        });

        foreach($links as $k => $link) {
            $data[$k] = $link;
            $data[$k]['children'] = $this->parseSignsCategory($link['url']);
        }

        return $data;
    }

    private function parseSignsCategory($url)
    {
        $this->crawler->clear();
        $this->crawler->addHtmlContent($this->getPageData($url));

        $signs = $this->crawler->filterXPath(self::SIGN_BLOCKS['SIGN_BLOCK'])->each(function (Crawler $node) {
            $this->logger->debug('Сохранение знака ' . $node->text());
            $count = $node->filterXPath(self::SIGN_BLOCKS['SIGN_DESCRIPTION'])->count();
            return [
                'n' => $node->filterXPath(self::SIGN_BLOCKS['SIGN_N'])->text(),
                'title' => $node->filterXPath(self::SIGN_BLOCKS['SIGN_TITLE'])->text(),
                'text' => ($count > 0) ? $this->clearProviderMarks($node->filterXPath(self::SIGN_BLOCKS['SIGN_DESCRIPTION'])->html()) : null,
                'image' => $node->filter('img')->each(function (Crawler $img) {
                    return $this->changeImageSize($img->attr('src'), self::IMAGES_SIZES[self::IMAGE_SIZE_NEED]);
                })
            ];
        });

        return $signs;
    }


    /**
     * Парсит страицу с правилами
     *
     * @param $html
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    private function parseRules($html)
    {
        $this->crawler->addHtmlContent($html);
        $this->crawler->filterXPath('//h2[contains(@id, "p")]')->each(function (Crawler $node, $i) {
            $this->models['rule_category'][$i] = $node->text();
            $this->models['rule_item'][$i]['text'] = '';
            $this->models['rule_item'][$i]['text'] .= $node->nextAll()->parents()->html();
        });


        dd($this->models['rule_item']);exit();

    }

    /**
     * Возвращает урл страницы с правилами
     *
     * @return string
     */
    private function getRulesPath()
    {
        $path = sprintf(
            self::RULES_URL_TEMPLATE,
            self::URL
        );

        return $path;
    }

    private function getSignsPath()
    {
        $path = sprintf(
            self::SIGNS_URL_TEMPLATE,
            self::URL
        );

        return $path;
    }

    /**
     * Получает результат билетов
     *
     * @return Question[]
     *
     * @throws \RuntimeException
     */
    public function getTickets() {
        $this->iteration++;
        $this->getTicketsData();

        if($this->fillTicketsData() === false) {
            $this->iteration = 0;
            $this->object_id++;
        }

        if ($this->object_id > $this->limit) {
            return $this->models;
        }

        return $this->getTickets();
    }

    /**
     * Получает данные с сайта am.ru
     *
     * @return void
     * @throws \RuntimeException
     */
    private function getTicketsData()
    {
        $cookie = new FileCookieJar('./storage/cookie');
        $response = $this->client->request('GET', $this->getTicketsPath(), [
            'cookies' => $cookie
        ]);
        $body = $response->getBody()->getContents();
        $this->data = json_decode($body, true);
    }

    private function getTicketsPath()
    {
        $path = sprintf(
            self::QUESTION_URL_TEMPLATE,
            self::URL,
            $this->category_id,
            $this->type_id,
            $this->object_id
        );

        if ($this->iteration > 1) {
            $path = $path . '&answer=' . $this->answer_id;
        }

        return $path;
    }


    /**
     * Заполнение полученными данными модели билетов
     *
     * @return bool
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function fillTicketsData()
    {
        if (array_key_exists('data', $this->data) === false) {
            return false;
        }

        if (array_key_exists('complete', $this->data['data']) === true) {
            return false;
        }

        $question = new Question($this->data);
        $question->setTicket($this->object_id);
        $question->setType($this->type_id);
        $question->setCategory($this->category_id);
        $this->models[] = $question;
        $this->logger->debug('Добавлен: ' . $this->data['data']['question']['text']);

        return true;
    }

    /**
     * Заполнение полученными данными модели знаков
     *
     * @return bool
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function fillSignsData()
    {
        foreach ($this->data as $category_item) {
            $category = new SignCategory($category_item);
            $sign = [];
            if (array_key_exists('children', $category_item) === true && count($category_item['children']) > 0) {
                foreach ($category_item['children'] as $sign_item) {
                    $this->logger->debug('Заполнение модели знака ' . $sign_item['title']);
                    $sign[] = new Sign($sign_item);
                }
            }

            $category->setSigns($sign);
            $this->models[]  = $category;
        }

        return true;
    }

    /**
     * Возвращает ссылку на изображение с требуемым размером
     *
     * @param string $url  Ссылка на изображение
     * @param string $size Код размера изображения
     *
     * @return string
     */
    private function changeImageSize($url, $size)
    {
        $segments = explode('/', $url);
        $segments[count($segments)-2] = $size;

        return implode('/', $segments);
    }

    /**
     * Аггрегирует замену и очистку отметок об источнике
     *
     * @param string $html Содержимое
     *
     * @return string
     */
    private function clearProviderMarks($html)
    {
        $html = $this->clearTrash($html);
        $html = $this->replaceSignIcons($html);
        
        return $html;
    }

    private function clearTrash($html)
    {
        $data = [
            'class="au-text"',
            'style="color: black;"',
            ' style="font-size: 14px;"',
            'style="font-size: 14px;"',
            '<br>',
            '  '
        ];

        return str_replace($data, '', $html);
    }

    private function replaceSignIcons($html)
    {
        $replace = [
            'pdd-inline-sign__icon',
            'https://media.am.ru/pdd/sign/',
            'pdd-inline-sign__text',
            'pdd-inline-sign__popup',
            'pdd-inline-sign',
            '<p><span>',
            '</span></p>'
        ];
        
        $replaced = [
            'inline-sign__icon',
            '/images/signs/',
            'inline-sign__label',
            'inline-sign__popup',
            'inline-sign',
            '<p>',
            '</p>'
        ];
        
        return str_replace($replace, $replaced, $html);
    }
}