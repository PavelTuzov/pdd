<?php

namespace App\Helpers;
use App\Helpers\Parser\Amru;
use App\Models\Answer;
use App\Models\Parsed\Question;
use App\Models\Question as QuestionModel;
use App\Models\Sign;
use App\Models\SignCategory;
use App\Models\SignImage;
use App\Repositories\StatRepository;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Monolog\Logger;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class QuestionHelper
 */
class QuestionHelper
{

    /**
     * Наполняет массив с вопросами информацией о количестве ответов
     *
     * @param Collection     $questions  Массив вопросов
     * @param StatRepository $repository Репозиторий статистики
     *
     * @return mixed
     */
    public static function getStat($questions, StatRepository $repository)
    {
        if (Auth::check() === false) {
            return $questions;
        }

        $userData = $repository->getStat(Auth::user()->id);
        foreach ($questions as $k => $question) {
            if (isset($userData[$k])) {
                $question->stat = $userData[$k];
            }
        }

        return $questions;
    }
}
