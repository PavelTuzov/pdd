<?php

namespace App\Helpers;

use App\Helpers\Bot\Hooks\BotHookCommand;
use App\Models\Answer;
use App\Models\Question;
use App\Repositories\AnswerRepository;
use App\Repositories\BotRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class BotHelper
 */
class BotHelper
{
    /**
     * Текст остановки вопросов билета
     */
    const CHAT_TEXT_STOP = [
        '/stop',
        '/стоп',
    ];

    /**
     * Текст инициализации чата
     */
    const CHAT_TEXT_START = '/start';

    /**
     * Текст отсутствия вопроса
     */
    const NO_QUESTION = 'Такого вопроса не существует';

    /**
     * Текст отсутствия билета
     */
    const NO_TICKET = 'Такого билета не существует';

    /**
     * Время на ответ, сек
     */
    const TIME_TOANSWER = 60;

    /**
     * Количество билетов
     */
    const COUNT_OF_TICKETS = 40;

    /**
     * Количество вопросов в билете
     */
    const COUNT_OF_QUESTIONS = 20;

    /**
     * Шаблон ответа при неправильном ответе
     */
    const WRONG_MESSAGE_TEMPLATE = "❌ Неправильно.\n\nПравильный ответ - %s";

    /**
     * Шаблон ответа при правильном ответе
     */
    const RIGHT_MESSAGE_TEMPLATE = '✅ Правильно';

    /**
     * Шаблон ответа при отказе от прохождения билета
     */
    const STOP_MESSAGE_TEMPLATE = 'Вы прекратили прохождение билета';

    /**
     * Шаблон приветственного сообщения
     */
    const GREETINGS_MESSAGE_TEMPLATE = "Привет, курсант. Чтобы получить вопрос, тебе нужно ввести номер билета и номер вопроса. Например: \n\n ☑️ Билет 1 вопрос 2 \n или \n ☑ 1 2\n\nЕсли хочешь получить все вопросы билета, то необходимо ввести\n ☑ Билет {номер билета}";

    /**
     * Шаблон сообщения с прогрессом
     */
    const STATUS_MESSAGE_TEMPLATE = "Вы ответили на все вопросы билета %d \n\nОшибок допущено: %d";

    /**
     * Префикс сессии
     */
    const SESSION_TICKET_KEY = 'ticket_';

    /**
     * Репозиторий бота
     *
     * @var BotRepository
     */
    private $botRepository;

    /**
     * Репозиторий вопросов
     *
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * Репозиторий ответов
     *
     * @var AnswerRepository
     */
    private $answerRepository;

    /**
     * Флаг редактированного сообщения
     *
     * @var bool
     */
    private $isEditedMessage = false;

    /**
     * Ключ доступа к массиву сообщения
     *
     * @var string
     */
    public static $keyMessage = 'message';

    /**
     * Ключ доступа к массиву редактируемого сообщения
     *
     * @var string
     */
    public static $keyEditedMessage = 'edited_message';

    /**
     * Актуальный ключ доступа к массиву сообщения
     *
     * @var string
     */
    public static $actualKeyMessage;

    /**
     * Запрос
     *
     * @var mixed
     */
    private $request;


    /**
     * Флаг группы
     *
     * @var bool
     */
    private $isGroup = false;

    /**
     * BotHelper constructor.
     *
     * @param BotRepository      $botRepository      Репозиторий
     * @param QuestionRepository $questionRepository Репозиторий вопросов
     * @param AnswerRepository   $answerRepository   Репозиторий ответо
     */
    public function __construct(BotRepository $botRepository, QuestionRepository $questionRepository, AnswerRepository $answerRepository)
    {
        $this->botRepository = $botRepository;
        $this->questionRepository = $questionRepository;
        $this->answerRepository = $answerRepository;
    }

    /**
     * Выполняет команду
     *
     * @param BotHookCommand $command Команда
     *
     * @return mixed
     */
    public function execCommand(BotHookCommand $command)
    {
        return $command->exec();
    }

    /**
     * Пороверяет, новый ли это подписчик и сохраняет его
     * в случае положительного результата
     *
     * @param array $result Массив данных обновления
     *
     * @return bool
     */
    private function checkIsNewUser($result)
    {
        $text = $this->getMessageFromResult($result);

        if ($text === self::CHAT_TEXT_START) {
            $this->sendGreetings($result);

            return $this->botRepository->saveChatSubscriber($result);
        }

        return false;
    }


    /**
     * Проверяет текст на стоп-слово для билета
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function checkForStopText($result)
    {
        $text = $this->getMessageFromResult($result);

        if (in_array($text, self::CHAT_TEXT_STOP, true) === true) {
            return true;
        }

        return false;
    }

    /**
     * Парсит текст сообщения пользователя
     *
     * @param array $result Массив данных
     *
     * @return mixed
     */
    private function parseText($result)
    {
        if ($this->checkForStopText($result) !== false) {
            $this->clearTicketSession($result);

            return $this->sendStopText($result);
        }

        if ($this->checkIsNewUser($result) !== false) {
            return $result;
        }

        if ($this->checkForQuestion($result) !== false) {
            return $result;
        }

        if ($this->checkForTicket($result) !== false) {
            return $result;
        }

        if ($this->checkForAnswer($result) !== false) {
            return $result;
        }


        return false;
    }

    /**
     * @param $result
     * @param $ticket
     * @param $n
     * @param int $errors
     * @return bool
     */
    private function processTicket($result, $ticket, $n, $errors = 0)
    {
        if ($n > self::COUNT_OF_QUESTIONS) {
            $this->sendProgressMessage($result, $ticket, $errors);
            $this->clearTicketSession($result);

            return false;
        }

        $question = $this->questionRepository->getQuestionByNumber($ticket, $n);
        $data = [
            'n' => $n,
            'errors' => $errors,
            'ticket' => $ticket,
            'last_question_id' => $question['question']->id
        ];

        $this->addTicketSession($result, $data);

        return $this->processQuestion($result, $question, $data['n']);
    }

    /**
     * Обрабатывает отправку билета
     *
     * @param array $result   Массив данных
     * @param array $question Идентификатор билета
     * @param int   $n        Номер вопроса
     *
     * @return bool
     */
    private function processQuestion($result, $question, $n)
    {
        if ($question !== false) {
            if (strlen($question['question']->image) > 0) {
                return $this->sendQuestionWithImage($question, $result);
            }

            return $this->sendQuestion($question, $result);
        }

        return $this->sendNoTicket($result);
    }

    /**
     * Проверяет запрос на отправку вопросов билета
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function checkForTicket($result)
    {
        $text = $this->getMessageFromResult($result);
        $hasText = (strpos($text, 'вопрос') === false && strpos($text, 'билет') !== false);
        if ($hasText === true) {
            preg_match_all('!\d+!', $text, $ticket);

            if (isset($ticket[0][0])) {
                $ticket = $ticket[0][0];

                return $this->processTicket($result, $ticket, 1, 0);
            }
        }

        return false;
    }

    /**
     * Проверяет запрос на отправку вопроса
     *
     * @param array $result Массив данных
     *
     * @return mixed
     */
    private function checkForQuestion($result)
    {
        $text = $this->getMessageFromResult($result);

        $hasText = (strpos($text, 'вопрос') !== false && strpos($text, 'билет') !== false);
        $hasNoTextAndTwoDigits = is_numeric(str_replace(' ', '', $text));
        if ($hasText === true || $hasNoTextAndTwoDigits === true) {
            preg_match_all('!\d+!', $text, $n);

            if (count($n[0]) >= 2) {
                $ticket = $n[0][0];
                $n = $n[0][1];

                $question = $this->questionRepository->getQuestionByNumber($ticket, $n);

                return $this->processQuestion($result, $question, $n);
            }

            return $this->sendNoQuestions($result);
        }

        return false;
    }


    /**
     * Проверяет ответ на вопрос
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function checkForAnswer($result)
    {
        $text = $this->getMessageFromResult($result);
        $hasNoText = (in_array($text, self::CHAT_TEXT_STOP, true) === false && strpos($text, self::CHAT_TEXT_START) === false);

        if ($hasNoText) {
            $session = $this->checkTicketSession($result);
            if ($session !== false) {
                $answers = $this->answerRepository->getAnswersByQuestion($session->value['last_question_id']);
            } else {
                $question = $this->botRepository->getLastQuestion($result);
                $this->botRepository->checkQuestionAnswered($question);
                $answers = $this->answerRepository->getAnswersByQuestion($question->question_id);
            }

            if (count($answers) === 0) {
                return false;
            }

            $right = false;

            foreach ($answers as $answer) {
                if (trim($answer->answer) === trim($result[self::$actualKeyMessage]['text'])) {
                    $this->sendResultOnAnswer($result, $answer);
                    $right = $answer->right;
                }
            }

            if ($session !== false) {
                return $this->processTicket(
                    $result,
                    $session->value['ticket'],
                    $session->value['n'] + 1,
                    ($right === false) ? ($session->value['errors'] + 1) : $session->value['errors']
                );
            }
        }

        return false;
    }

    /**
     * Отправляет подписчику вопрос
     *
     * @param array $question Вопрос
     * @param array $result   Массив данных
     *
     * @return bool
     */
    private function sendQuestion($question, $result)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => $question['question']->question . $this->getAnswersAsText($question),
            'reply_markup' => $this->prepareReplyMarkupForQuestion($question)
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => $question['question']->question,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s'),
            'question_id' => $question['question']->id,
            'answered' => false,
        ]);
    }

    /**
     * Отсправляет текст об отмене прохождения билета
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function sendStopText($result)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::STOP_MESSAGE_TEMPLATE,
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::STOP_MESSAGE_TEMPLATE,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s'),
            'question_id' => null,
            'answered' => false,
        ]);
    }

    /**
     * Отправляет подписчику вопрос
     *
     * @param array $question Вопрос
     * @param array $result   Массив данных
     *
     * @return bool
     */
    private function sendQuestionWithImage($question, $result)
    {
        $response = Telegram::sendPhoto([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'photo' => getenv('APP_URL') . '/images/questions/' . $question['question']->image,
            'caption' => $question['question']->question . $this->getAnswersAsText($question),
            'reply_markup' => $this->prepareReplyMarkupForQuestion($question)
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => $question['question']->question,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s'),
            'question_id' => $question['question']->id,
            'answered' => false,
        ]);
    }

    /**
     * Отправляет подписчику текст, что такого вопроса нет
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function sendNoQuestions($result)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::NO_QUESTION,
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::NO_QUESTION,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s'),
        ]);
    }


    /**
     * Отправляет подписчику текст, что такого билета нет
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    private function sendNoTicket($result)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::NO_TICKET,
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::NO_QUESTION,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Отправляет пользователю результат ответа на вопрос
     *
     * @param array  $result Массив данных
     * @param Answer $answer Экземпляр ответа
     *
     * @return bool
     */
    private function sendResultOnAnswer($result, $answer)
    {
        if ($answer->right === true) {
            $text = self::RIGHT_MESSAGE_TEMPLATE;
        } else {
            $right = $this->answerRepository->checkAnswer($answer->id, ['withAnswer']);
            $text = sprintf(self::WRONG_MESSAGE_TEMPLATE, $right->withAnswer->answer);
        }

        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => $text,
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => $text,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Отправляет сообщение с прогрессом
     *
     * @param array $result Массив данных
     * @param array $ticket Сессия
     * @param int   $errors Ошибки
     *
     * @return bool
     */
    public function sendProgressMessage($result, $ticket, $errors)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => sprintf(self::STATUS_MESSAGE_TEMPLATE, $ticket, $errors),
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::STATUS_MESSAGE_TEMPLATE,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Отправляет приветственной сообщение
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    public function sendGreetings($result)
    {
        $response = Telegram::sendMessage([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::GREETINGS_MESSAGE_TEMPLATE,
            'reply_markup' => $this->disableMarkup()
        ]);

        return $this->botRepository->saveChatAnswer([
            'chat_id' => $result[self::$actualKeyMessage]['chat']['id'],
            'text' => self::GREETINGS_MESSAGE_TEMPLATE,
            'message_id' => $response->getMessageId(),
            'date' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Возвращает текст сообщения
     *
     * @param array $result Массив данных
     *
     * @return string
     */
    private function getMessageFromResult($result)
    {
        $text = false;

        if (isset($result[self::$actualKeyMessage]['text'])) {
            $text = $result[self::$actualKeyMessage]['text'];
        }

        if ($text !== false) {
            $text = mb_strtolower($text);
        }

        return $text;
    }

    /**
     * Проверяет массив данных на факт редактирования сообщения
     *
     * @param array $result Массив данных
     *
     * @return array
     */
    private function checkIsEdited($result)
    {
        if (isset($result[self::$keyMessage])) {
            self::$actualKeyMessage = self::$keyMessage;
        }

        if (isset($result[self::$keyEditedMessage])) {
            self::$actualKeyMessage = self::$keyEditedMessage;
            $this->isEditedMessage = true;
        }

        return $result;
    }

    /**
     * Проверяет, отправлен ли был стикер или сообщение
     *
     * @param array $result Массив данных
     *
     * @return array
     */
    private function checkIsSticker($result)
    {
        if (isset($result[self::$actualKeyMessage]['sticker']['emoji'])) {
            $result[self::$actualKeyMessage]['text'] = $result[self::$actualKeyMessage]['sticker']['emoji'];
        }

        return $result;
    }


    /**
     * Проверяет, отправлен ли был документ или сообщение
     *
     * @param array $result Массив данных
     *
     * @return array
     */
    private function checkIsDocument($result)
    {
        if (isset($result[self::$actualKeyMessage]['document']['file_name'])) {
            $result[self::$actualKeyMessage]['text'] = 'file: ' . $result[self::$actualKeyMessage]['document']['file_id'];
        }

        return $result;
    }

    /**
     * Формирует клавиаутуру ответов
     *
     * @param array $question Вопрос и ответы на него
     *
     * @return string
     */
    private function prepareReplyMarkupForQuestion($question)
    {
        $response = ['one_time_keyboard' => true];

        foreach ($question['answers'] as $answer) {
            $response['keyboard'][] = [$answer->answer];
        }

        return json_encode($response);
    }

    private function prepareReplyMarkup()
    {
        return json_encode([
            'one_time_keyboard' => true,
            'keyboard' => [
                ['билеты', 'знаки']
            ]
        ]);
    }

    /**
     * Отключает клавиатуру
     *
     * @return string
     */
    private function disableMarkup()
    {
        return json_encode(['remove_keyboard' => true]);
    }

    /**
     * Возвращает строку с ответами
     *
     * @param Question $question Объкт вопроса
     *
     * @return string
     */
    private function getAnswersAsText($question)
    {
        $text = " \n\n";

        if (isset($question['answers']) && count($question['answers'])) {
            foreach ($question['answers'] as $answer) {
                $text .= ' 🔘 ' . $answer->n . ' - ' . $answer->answer . "\n";
            }
        }

        return $text;
    }

    /**
     * Добавление сессии
     *
     * @param array $result Массив данных
     * @param array $data   Сохраняемые данные
     *
     * @return bool
     */
    private function addTicketSession($result, $data)
    {
        if ($data['n'] > self::COUNT_OF_QUESTIONS) {
            return false;
        }

        $session = $this->checkTicketSession($result);
        if ($session !== false) {
            return $this->botRepository->updateSession($session, $data);
        }

        return $this->botRepository->putSession(self::SESSION_TICKET_KEY . $result[self::$actualKeyMessage]['chat']['id'], $data);
    }

    /**
     * Возвращает номер вопроса в цикле билета из сессии
     *
     * @param array $result Массив данных
     *
     * @return mixed
     */
    private function checkTicketSession($result)
    {
        return $this->botRepository->getSession(self::SESSION_TICKET_KEY . $result[self::$actualKeyMessage]['chat']['id']);
    }

    private function clearTicketSession($result)
    {
        return $this->botRepository->removeSession(self::SESSION_TICKET_KEY . $result[self::$actualKeyMessage]['chat']['id']);
    }

    /**
     * Проверяет, группа ли это
     *
     * @param $result
     *
     * @return mixed
     */
    private function checkIsGroup($result)
    {
        if (isset($result[self::$actualKeyMessage]['chat']['type']) && $result[self::$actualKeyMessage]['chat']['type'] === 'group') {
            $this->isGroup = true;
        }

        $this->botRepository->setIsGroup($this->isGroup);

        return $result;
    }
}
