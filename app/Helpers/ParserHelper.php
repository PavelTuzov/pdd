<?php

namespace App\Helpers;
use App\Helpers\Parser\Amru;
use App\Models\Answer;
use App\Models\Parsed\Question;
use App\Models\Question as QuestionModel;
use App\Models\Sign;
use App\Models\SignCategory;
use App\Models\SignImage;
use GuzzleHttp\Client;
use Monolog\Logger;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ParserHelper
 */
class ParserHelper
{

    /**
     * Результат парсинга
     *
     * @var array
     */
    private $result;

    /**
     * Уровень логирования
     *
     * @var int
     */
    private $verbosity;

    /**
     * Клиент газели
     *
     * @var Client
     */
    private $client;

    /**
     * Источник
     *
     * @var Resource;
     */
    private $provider;

    /**
     * Парсер
     *
     * @var
     */
    private $crawler;

    /**
     * Уровень дебага
     */
    const DEBUG_LEVEL = 'INFO';

    /**
     * Возвращает результат парсинга
     *
     * @return bool
     * @throws \RuntimeException
     */
    public function getTickets()
    {
        $logger = new Logger(self::DEBUG_LEVEL);
        $this->client = new Client();
        $this->provider = new Amru($this->client, $logger);
        $this->result = $this->provider->getTickets();

        return $this->saveTickets();
    }

    /**
     * Устанавливает уровень логирования
     *
     * @param int $verbosity Уровень логирования
     *
     * @return void
     */
    public function setVerbosity($verbosity)
    {
        $this->verbosity = $verbosity;
    }

    /**
     * Сохраняет полученные данные в базу
     *
     * @return mixed
     */
    private function saveTickets()
    {
        if (is_array($this->result) === false) {
            return false;
        }

        foreach ($this->result as $item) {
            $question = new QuestionModel();
            $question->question   = $item->getQuestion();
            $question->image      = $item->getImage();
            $question->n          = $item->getN();
            $question->ticket_id  = $item->getTicket();
            $question->subject_id = $item->getType();
            $question->category_id = $item->getCategory();
            $question->hint       = $item->getHint();
            $question->save();

            $answers = $item->getAnswers();

            foreach ($answers as $answer_item) {
                $answer = new Answer();
                $answer->question_id = $question->id;
                $answer->n           = $answer_item['orderNumber'];
                $answer->right       = (bool) $answer_item['isRight'];
                $answer->answer      = $answer_item['text'];
                $answer->save();
            }
        }

        return $this->result;
    }

    /**
     * Получает знаки
     *
     * @return mixed
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function getSigns()
    {
        $logger = new Logger(self::DEBUG_LEVEL);
        $this->client = new Client();
        $this->provider = new Amru($this->client, $logger);
        $this->crawler = new Crawler();
        $this->provider->setCrawler($this->crawler);
        $this->result = $this->provider->getSigns();

        return $this->saveSigns();
    }

    /**
     * Получает правила
     *
     * @return bool|int
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function getRules()
    {
        $logger = new Logger(self::DEBUG_LEVEL);
        $this->client = new Client();
        $this->provider = new Amru($this->client, $logger);
        $this->crawler = new Crawler();
        $this->provider->setCrawler($this->crawler);
        $this->result = $this->provider->getRules();

        return $this->saveRules();
    }

    /**
     * Сохраняет правила
     *
     * @return bool|int
     */
    private function saveRules()
    {
        if (is_array($this->result) === false) {
            return false;
        }

        return 1;
    }

    /**
     * Сохраняет знаки
     *
     * @return array|bool
     */
    private function saveSigns()
    {
        if (is_array($this->result) === false) {
            return false;
        }

        foreach ($this->result as $item) {
            $category = new SignCategory();
            $category->title = $item->getTitle();
            $category->image = $item->getImage();
            $category->n     = $item->getN();

            $category->save();
            $signs = $item->getSigns();

            foreach ($signs as $sign_item) {
                $sign = new Sign();
                $sign->n           = $sign_item->getN();
                $sign->text        = $sign_item->getText();
                $sign->title       = $sign_item->getTitle();
                $sign->category_id = $category->id;
                $sign->save();

                if (count($sign_item->getImage()) > 0) {
                    foreach ($sign_item->getImage() as $k => $image_item) {
                        $image = new SignImage();
                        $image->n = ($k + 1);
                        $image->sign_id = $sign->id;
                        $image->title = $sign->title;
                        $image->url = $image_item;

                        $image->save();
                    }
                }
            }
        }

        return $this->result;

    }
}
