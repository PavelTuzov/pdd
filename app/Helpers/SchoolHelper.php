<?php

namespace App\Helpers;

use App\Repositories\SchoolRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Класс по работе со школами
 */
class SchoolHelper
{
    /**
     * SchoolHelper constructor.
     *
     * @param SchoolRepository $repository Репозиторий школ
     */
    public function __construct(SchoolRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Возвращает коллекцию школ
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws \BadMethodCallException
     */
    public function getSchools()
    {
        if (env('CACHE_DRIVER') === 'memcached') {
            return Cache::tags(['schools'])->rememberForever(
                'schools',
                function () {
                    return $this->repository->getAllSchools();
                }
            );
        }

        return $this->repository->getAllSchools();
    }

    public function getSchoolsByCityId($cityId)
    {
        if (env('CACHE_DRIVER') === 'memcached') {
            return Cache::tags(['schools', 'schools_' . $cityId])->rememberForever(
                'schools_city_' . $cityId,
                function () use ($cityId) {
                    return $this->repository->getSchoolsByCity($cityId);
                }
            );
        }

        return $this->repository->getAllSchools();
    }
}