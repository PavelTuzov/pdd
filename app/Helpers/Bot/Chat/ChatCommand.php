<?php

namespace App\Helpers\Bot\Chat;

/**
 * Class ChatCommand
 */
abstract class ChatCommand
{
    /**
     * Выполняет команду
     *
     * @return mixed
     */
    public abstract function exec();
}
