<?php

namespace App\Helpers\Bot;

/**
 * This object represents a phone contact.
 */
class Contact
{
    /**
     * Contact's phone number
     *
     * @var string
     */
    private $phone_number;

    /**
     * Contact's first name
     *
     * @var string
     */
    private $first_name;

    /**
     * Optional. Contact's last name
     *
     * @var string
     */
    private $last_name;

    /**
     * Optional. Contact's user identifier in Telegram
     *
     * @var int
     */
    private $user_id;
}
