<?php

namespace App\Helpers\Bot;

/**
 * This object represents a Telegram user or bot.
 */
class User
{

    /**
     * Unique identifier for this user or bot
     *
     * @var int
     */
    private $id;

    /**
     * True, if this user is a bot
     *
     * @var bool
     */
    private $is_bot;

    /**
     * User‘s or bot’s first name
     *
     * @var string
     */
    private $first_name;

    /**
     * Optional. User‘s or bot’s last name
     *
     * @var string
     */
    private $last_name;

    /**
     * Optional. User‘s or bot’s username
     *
     * @var string
     */
    private $username;

    /**
     * Optional. IETF language tag of the user's language
     *
     * @var string
     */
    private $language_code;

    /**
     * User constructor.
     *
     * @param array $data Данные
     */
    public function __construct($data)
    {
        $this->setId($data['id']);
        $this->setIsBot(array_key_exists('is_bot', $data) ? $data['is_bot'] : null);
        $this->setFirstName(array_key_exists('first_name', $data) ? $data['first_name'] : null);
        $this->setLastName(array_key_exists('last_name', $data) ? $data['last_name'] : null);
        $this->setUsername(array_key_exists('username', $data) ? $data['username'] : null);
        $this->setLanguageCode(array_key_exists('language_code', $data) ? $data['language_code'] : null);
    }

    /**
     * Возвращает идентификатор пользователя
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Устанавливает идентификатор пользователя
     *
     * @param int $id Идентификатор пользователя
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Возвращает флаг бота
     *
     * @return boolean
     */
    public function isIsBot()
    {
        return $this->is_bot;
    }

    /**
     * Устанавливает флаг бота
     *
     * @param boolean $is_bot Флаг бота
     *
     * @return void
     */
    public function setIsBot($is_bot)
    {
        $this->is_bot = $is_bot;
    }

    /**
     * Возвращает имя
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Устанавливает Имя
     *
     * @param string $first_name Имя
     *
     * @return void
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * Возвращает фамилию пользователя
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Устанавливает фамилию
     *
     * @param string $last_name Фамилия
     *
     * @return void
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * Возвращает юзернейм пользователя
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Устанавливает юзернейм
     *
     * @param string $username Юзернейм
     *
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Возвращает код языка
     *
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->language_code;
    }

    /**
     * Устанавливает код языка
     *
     * @param string $language_code Код языка
     *
     * @return void
     */
    public function setLanguageCode($language_code)
    {
        $this->language_code = $language_code;
    }
}
