<?php

namespace App\Helpers\Bot\Hooks;

use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Выполняет команду информации о боте
 */
class BotInfoHookCommand extends BotHookCommand
{
    /**
     * Возвращает информацию о боте
     *
     * @return array
     */
    public function exec()
    {
        $response = Telegram::getMe();

        return [
            'botId' => $response->getId(),
            'firstName' => $response->getFirstName(),
            'username' => $response->getUsername()
        ];
    }
}
