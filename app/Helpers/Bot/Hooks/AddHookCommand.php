<?php

namespace App\Helpers\Bot\Hooks;

use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Команда добавления хука
 */
class AddHookCommand extends BotHookCommand
{

    /**
     * Шаблон запроса
     */
    const URL_TEMPLATE = '%s/bot/hook/%s';

    /**
     * Выполняет команду
     *
     * @return array
     */
    public function exec()
    {
        return Telegram::setWebhook([
            'url' => sprintf(self::URL_TEMPLATE, getenv('APP_URL'), getenv('TELEGRAM_BOT_TOKEN'))
        ]);
    }
}
