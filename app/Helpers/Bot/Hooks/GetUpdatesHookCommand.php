<?php

namespace App\Helpers\Bot\Hooks;

use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Выполняет команду получения обновлений
 */
class GetUpdatesHookCommand extends BotHookCommand
{
    /**
     * Возвращает информацию о боте
     *
     * @return array
     */
    public function exec()
    {
        return Telegram::getUpdates();
    }
}
