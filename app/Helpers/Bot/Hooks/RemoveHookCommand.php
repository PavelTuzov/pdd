<?php

namespace App\Helpers\Bot\Hooks;

use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Команда удаления хука
 */
class RemoveHookCommand extends BotHookCommand
{

    /**
     * Выполняет команду
     *
     * @return array
     */
    public function exec()
    {
        return Telegram::removeWebhook();
    }
}
