<?php

namespace App\Helpers\Bot\Hooks;


/**
 * Class BotHookCommand
 */
abstract class BotHookCommand
{
    /**
     * Выполняет команду
     *
     * @return mixed
     */
    abstract public function exec();
}
