<?php

namespace App\Helpers\Bot\Hooks;

use App\Helpers\Bot\Response;
use App\Helpers\Bot\Update;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Monolog\Logger;


/**
 * Выполняет команду информации о боте
 */
class AcceptHookCommand extends BotHookCommand
{

    /**
     * Экземпляр запроса
     *
     * @var Request
     */
    private $request;

    /**
     * Токен
     *
     * @var string
     */
    private $token;

    /**
     * Хэлпер логгер
     *
     * @var LogHelper
     */
    private $logHelper;

    /**
     * AcceptHookCommand constructor.
     *
     * @param Request $request Запрос
     * @param string  $token   Строка
     */
    public function __construct(Request $request, $token)
    {
        $this->request = $request;
        $this->token = $token;
        $this->logHelper = new LogHelper();
    }

    /**
     * Возвращает информацию о боте
     *
     * @return array
     */
    public function exec()
    {
        if ($this->token !== getenv('TELEGRAM_BOT_TOKEN')) {
            return abort(403);
        }

        $response = new Update($this->request->all());

        $this->logHelper->debug($response);


        //$this->botRepository->saveChatMessage($response);

        return ['status' => 'ok'];
    }
}
