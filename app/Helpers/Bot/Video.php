<?php

namespace App\Helpers\Bot;

/**
 * This object represents a video file.
 */
class Video
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;

    /**
     * Video width as defined by sender
     *
     * @var int
     */
    private $width;

    /**
     * Video height as defined by sender
     *
     * @var int
     */
    private $height;

    /**
     * Duration of the video in seconds as defined by sender*
     *
     * @var int
     */
    private $duration;

    /**
     * Optional. Video thumbnail
     *
     * @var PhotoSize
     */
    private $thumb;

    /**
     * Optional. MIME type of the file as defined by sender
     *
     * @var string
     */
    private $mime_type;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
