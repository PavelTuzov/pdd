<?php

namespace App\Helpers\Bot;

/**
 * This object contains information about an incoming pre-checkout query.
 */
class PreCheckoutQuery
{
    /**
     * Unique identifier for this query
     *
     * @var int
     */
    private $id;

    /**
     * User who sent the query
     *
     * @var User
     */
    private $from;

    /**
     * Three-letter ISO 4217 currency code
     *
     * @var string
     */
    private $currency;

    /**
     * Total price in the smallest units of the currency (integer, not float/double).
     * For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows
     * the number of digits past the decimal point for each currency (2 for the majority of currencies).
     *
     * @var int
     */
    private $total_amount;

    /**
     * Bot specified invoice payload
     *
     * @var string
     */
    private $invoice_payload;

    /**
     * Optional. Identifier of the shipping option chosen by the user
     *
     * @var string
     */
    private $shipping_option_id;

    /**
     * Optional. Order info provided by the user
     *
     * @var OrderInfo
     */
    private $order_info;
}
