<?php

namespace App\Helpers\Bot;

/**
 * This object represents a point on the map.
 */
class Location
{
    /**
     * Longitude as defined by sender
     *
     * @var float
     */
    private $longitude;

    /**
     * Latitude as defined by sender
     *
     * @var float
     */
    private $latitude;
}
