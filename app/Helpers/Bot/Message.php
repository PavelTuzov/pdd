<?php

namespace App\Helpers\Bot;

/**
 * Class Message
 */
class Message
{
    /**
     * Unique message identifier inside this chat
     *
     * @var int
     */
    private $message_id;

    /**
     * Optional. Sender, empty for messages sent to channels
     *
     * @var User
     */
    private $from;

    /**
     * Date the message was sent in Unix time
     *
     * @var int
     */
    private $date;

    /**
     * Conversation the message belongs to
     *
     * @var Chat
     */
    private $chat;

    /**
     * Optional. For forwarded messages, sender of the original message
     *
     * @var User
     */
    private $forward_from;

    /**
     * Optional. For messages forwarded from channels, information about the original channel
     *
     * @var Chat
     */
    private $forward_from_chat;

    /**
     * Optional. For messages forwarded from channels, identifier of the original message in the channel
     *
     * @var int
     */
    private $forward_from_message_id;

    /**
     * Optional. For messages forwarded from channels, signature of the post author if present
     *
     * @var string
     */
    private $forward_signature;

    /**
     * Optional. For forwarded messages, date the original message was sent in Unix time
     *
     * @var int
     */
    private $forward_date;

    /**
     * Optional. For replies, the original message. Note that the Message object in this field will not contain further
     * reply_to_message fields even if it itself is a reply.
     *
     * @var Message
     */
    private $reply_to_message;

    /**
     * Optional. Date the message was last edited in Unix time
     *
     * @var int
     */
    private $edit_date;

    /**
     * Optional. Signature of the post author for messages in channels
     *
     * @var string
     */
    private $author_signature;

    /**
     * Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters.
     *
     * @var string
     */
    private $text;

    /**
     * Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
     *
     * @var MessageEntity[]
     */
    private $entities;

    /**
     * Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in
     * the caption
     *
     * @var MessageEntity[]
     */
    private $caption_entities;

    /**
     * Optional. Message is an audio file, information about the file
     *
     * @var Audio
     */
    private $audio;

    /**
     * Message constructor.
     *
     * @param array $data Данные
     */
    public function __construct($data)
    {

    }
}
