<?php

namespace App\Helpers\Bot;

/**
 * This object represents a shipping address.
 */
class ShippingAddress
{

    /**
     * ISO 3166-1 alpha-2 country code
     *
     * @var string
     */
    private $country_code;

    /**
     * State, if applicable
     *
     * @var string
     */
    private $state;

    /**
     * City
     *
     * @var string
     */
    private $city;

    /**
     * First line for the address
     *
     * @var string
     */
    private $street_line1;

    /**
     * Second line for the address
     *
     * @var string
     */
    private $street_line2;

    /**
     * Address post code
     *
     * @var string
     */
    private $post_code;
}
