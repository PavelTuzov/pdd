<?php

namespace App\Helpers\Bot;

/**
 * This object represents an incoming update.
 * At most one of the optional parameters can be present in any given update.
 */
class Update
{
    /**
     * The update‘s unique identifier. Update identifiers start from a certain positive number and increase sequentially.
     * This ID becomes especially handy if you’re using Webhooks, since it allows you to ignore repeated updates or to
     * restore the correct update sequence, should they get out of order.
     *
     * @var int
     */
    private $update_id;

    /**
     * Optional. New incoming message of any kind — text, photo, sticker, etc.
     *
     * @var Message
     */
    private $message;

    /**
     * Optional. New version of a message that is known to the bot and was edited
     *
     * @var Message
     */
    private $edited_message;

    /**
     * Optional. New incoming channel post of any kind — text, photo, sticker, etc.
     *
     * @var Message
     */
    private $channel_post;

    /**
     * Optional. New version of a channel post that is known to the bot and was edited
     *
     * @var Message
     */
    private $edited_channel_post;


    /**
     * Optional. New incoming inline query
     *
     * @var InlineQuery
     */
    private $inline_query;

    /**
     * Optional. The result of an inline query that was chosen by a user and sent to their chat partner.
     * Please see our documentation on the feedback collecting for details on how to enable these updates for your bot.
     *
     * @var ChosenInlineResult
     */
    private $chosen_inline_result;

    /**
     * Optional. New incoming callback query
     *
     * @var CallbackQuery
     */
    private $callbackQuery;

    /**
     * Optional. New incoming shipping query. Only for invoices with flexible price
     *
     * @var ShippingQuery;
     */
    private $shippingQuery;

    /**
     * Optional. New incoming pre-checkout query. Contains full information about checkout
     *
     * @var
     */
    private $pre_checkout_query;

    /**
     * Response constructor.
     *
     * @param array $result Результат запроса
     */
    public function __construct($result)
    {
        $this->checkIsGroup($result);
        $result = $this->checkIsEdited($result);
        $result = $this->checkIsSticker($result);
        $result = $this->checkIsDocument($result);
        $this->parseText($result);
    }
}
