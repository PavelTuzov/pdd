<?php

namespace App\Helpers\Bot;

/**
 * This object represents information about an order.
 */
class OrderInfo
{

    /**
     * Optional. User name
     *
     * @var string
     */
    private $name;


    /**
     * Optional. User's phone number
     *
     * @var string
     */
    private $phone_number;


    /**
     * Optional. User email
     *
     * @var string
     */
    private $email;


    /**
     * Optional. User shipping address
     *
     * @var ShippingAddress
     */
    private $shipping_address;

}
