<?php

namespace App\Helpers\Bot;

/**
 * TThis object represents one size of a photo or a file / sticker thumbnail.
 */
class PhotoSize
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;

    /**
     * Photo width
     *
     * @var int
     */
    private $witdh;

    /**
     * Photo height
     *
     * @var int
     */
    private $height;

    /**
     * Optional. Title of the audio as defined by sender or by audio tags
     *
     * @var string
     */
    private $title;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
