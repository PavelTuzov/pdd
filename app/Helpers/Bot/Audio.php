<?php

namespace App\Helpers\Bot;

/**
 * This object represents an audio file to be treated as music by the Telegram clients.
 */
class Audio
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;

    /**
     * Duration of the audio in seconds as defined by sender
     *
     * @var int
     */
    private $duration;

    /**
     * Optional. Performer of the audio as defined by sender or by audio tags
     *
     * @var string
     */
    private $performer;

    /**
     * Optional. Title of the audio as defined by sender or by audio tags
     *
     * @var string
     */
    private $title;

    /**
     * Optional. MIME type of the file as defined by sender
     *
     * @var string
     */
    private $mime_type;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
