<?php

namespace App\Helpers\Bot;

/**
 * This object represents a video message (available in Telegram apps as of v.4.0).
 */
class VideoNote
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;


    /**
     * Video width and height as defined by sender
     *
     * @var int
     */
    private $length;

    /**
     * Duration of the video in seconds as defined by sender*
     *
     * @var int
     */
    private $duration;

    /**
     * Optional. Video thumbnail
     *
     * @var PhotoSize
     */
    private $thumb;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
