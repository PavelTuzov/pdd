<?php

namespace App\Helpers\Bot;

/**
 * This object represents an incoming inline query. When the user sends an empty query, your bot could return some default or trending results.
 */
class InlineQuery
{
    /**
     * Unique identifier for this query
     *
     * @var int
     */
    private $id;

    /**
     * Sender
     *
     * @var User
     */
    private $from;

    /**
     * Optional. Sender location, only for bots that request user location
     *
     * @var Location
     */
    private $location;

    /**
     * Text of the query (up to 512 characters)
     *
     * @var string
     */
    private $query;

    /**
     * Offset of the results to be returned, can be controlled by the bot
     *
     * @var string
     */
    private $offset;
}
