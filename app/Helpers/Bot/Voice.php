<?php

namespace App\Helpers\Bot;

/**
 * This object represents a voice note.
 */
class Voice
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;

    /**
     * Duration of the audio in seconds as defined by sender
     *
     * @var int
     */
    private $duration;

    /**
     * Optional. MIME type of the file as defined by sender
     *
     * @var string
     */
    private $mime_type;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
