<?php

namespace App\Helpers\Bot;

/**
 * This object represents a chat.
 */
class Chat
{
    /**
     * Unique identifier for this chat. This number may be greater than 32 bits and some programming languages may have
     * difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or
     * double-precision float type are safe for storing this identifier.
     *
     * @var int
     */
    private $id;

    /**
     * Type of chat, can be either “private”, “group”, “supergroup” or “channel”
     *
     * @var string
     */
    private $type;

    /**
     * Optional. Title, for supergroups, channels and group chats
     *
     * @var string
     */
    private $title;

    /**
     * Optional. Username, for private chats, supergroups and channels if available
     *
     * @var string
     */
    private $username;

    /**
     * Optional. First name of the other party in a private chat
     *
     * @var string
     */
    private $first_name;

    /**
     * Optional. Last name of the other party in a private chat
     *
     * @var string
     */
    private $last_name;

    /**
     * Optional. True if a group has ‘All Members Are Admins’ enabled.
     *
     * @var bool
     */
    private $all_members_are_administrators;

    /**
     * Optional. Chat photo. Returned only in getChat.
     *
     * @var ChatPhoto
     */
    private $photo;

    /**
     * Optional. Description, for supergroups and channel chats. Returned only in getChat.
     *
     * @var string
     */
    private $description;


    /**
     * Optional. Chat invite link, for supergroups and channel chats. Returned only in getChat.
     *
     * @var string
     */
    private $invite_link;

    /**
     * Optional. Pinned message, for supergroups. Returned only in getChat.
     *
     * @var Message
     */
    private $message;

    /**
     * Optional. For supergroups, name of group sticker set. Returned only in getChat.
     *
     * @var string
     */
    private $sticker_set_name;

    /**
     * Optional. True, if the bot can change the group sticker set. Returned only in getChat.
     *
     * @var string
     */
    private $can_set_sticker_set;

    /**
     * Возвращает идентификатор
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Устанавливает идентификатор чата
     *
     * @param int $id Идентификатор чата
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Возвращает тип чата
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Устанавливает тип чата
     *
     * @param string $type Тип чата
     *
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Возвращает заголовок чата
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Устанавливает заголовок чата
     *
     * @param string $title Заголовок чата
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return boolean
     */
    public function isAllMembersAreAdministrators()
    {
        return $this->all_members_are_administrators;
    }

    /**
     * @param boolean $all_members_are_administrators
     */
    public function setAllMembersAreAdministrators($all_members_are_administrators)
    {
        $this->all_members_are_administrators = $all_members_are_administrators;
    }

    /**
     * @return ChatPhoto
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param ChatPhoto $photo
     */
    public function setPhoto(ChatPhoto $photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInviteLink()
    {
        return $this->invite_link;
    }

    /**
     * @param string $invite_link
     */
    public function setInviteLink($invite_link)
    {
        $this->invite_link = $invite_link;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Message $message
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getStickerSetName()
    {
        return $this->sticker_set_name;
    }

    /**
     * @param string $sticker_set_name
     */
    public function setStickerSetName($sticker_set_name)
    {
        $this->sticker_set_name = $sticker_set_name;
    }

    /**
     * @return string
     */
    public function getCanSetStickerSet()
    {
        return $this->can_set_sticker_set;
    }

    /**
     * @param string $can_set_sticker_set
     */
    public function setCanSetStickerSet($can_set_sticker_set)
    {
        $this->can_set_sticker_set = $can_set_sticker_set;
    }

}
