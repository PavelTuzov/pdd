<?php

namespace App\Helpers\Bot;

/**
 * This object represents a general file (as opposed to photos, voice messages and audio files).
 */
class Document
{
    /**
     * Unique identifier for this file
     *
     * @var string
     */
    private $file_id;

    /**
     * Optional. Document thumbnail as defined by sender
     *
     * @var PhotoSize
     */
    private $thumb;


    /**
     * Optional. Original filename as defined by sender
     *
     * @var string
     */
    private $file_name;

    /**
     * Optional. MIME type of the file as defined by sender
     *
     * @var string
     */
    private $mime_type;

    /**
     * Optional. File size
     *
     * @var int
     */
    private $file_size;
}
