<?php

namespace App\Helpers\Bot;

/**
 * This object contains information about an incoming shipping query.
 */
class ShippingQuery
{
    /**
     * Unique query identifier
     *
     * @var string
     */
    private $id;

    /**
     * User who sent the query
     *
     * @var User
     */
    private $from;

    /**
     * Bot specified invoice payload
     *
     * @var string
     */
    private $invoice_payload;

    /**
     * User specified shipping address
     *
     * @var ShippingAddress
     */
    private $shippingAddress;
}
