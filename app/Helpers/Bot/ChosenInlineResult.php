<?php

namespace App\Helpers\Bot;

/**
 * Represents a result of an inline query that was chosen by the user and sent to their chat partner.
 */
class ChosenInlineResult
{
    /**
     * The unique identifier for the result that was chosen
     *
     * @var string
     */
    private $result_id;

    /**
     * The user that chose the result
     *
     * @var User
     */
    private $from;

    /**
     * Optional. Identifier of the sent inline message. Available only if there is an inline keyboard attached to the
     * message. Will be also received in callback queries and can be used to edit the message.
     *
     * @var string
     */
    private $inline_message_id;

    /**
     * Optional. Sender location, only for bots that require user location
     *
     * @var Location
     */
    private $location;

    /**
     * The query that was used to obtain the result
     *
     * @var string
     */
    private $query;
}
