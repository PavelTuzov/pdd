<?php

namespace App\Helpers;

use App\Helpers\UtilsHelper;
use App\Models\Answer;
use App\Repositories\AnswerRepository;
use App\Repositories\StatRepository;
use Auth;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserHelper
 */
class StatHelper
{
    /**
     * Количество неправильных ответов в топе
     */
    const TOP_ANSWERS_COUNT = 20;

    /**
     * Количество неправильных вопросов в топе
     */
    const TOP_QUESTION_COUNT = 10;

    /**
     * StatHelper constructor.
     */
    public function __construct()
    {
        $this->statRepository = new StatRepository();
        $this->answerRepository = new AnswerRepository();
    }

    /**
     * Экземпляр запроса
     *
     * @var \Request
     */
    private $requestFilter;

    /**
     * Устанавливает запрос
     *
     * @param \Request $request Запрос
     *
     * @return $this
     */
    public function setRequest($request)
    {
        $this->requestFilter = $request;

        return $this;
    }

    /**
     * Возвращает статистику
     *
     * @return Collection
     */
    public function getStat()
    {
        $filter = $this->requestFilter->data;

        return $this->statRepository->getStat(Auth::user()->id);
    }

    /**
     * Возвращает статистику по пользователю
     *
     * @return array
     */
    public function getStatByUser()
    {
        $data = $this->statRepository->getStat(Auth::user()->id);
        $count = count($data);
        $errors = $this->getCountOfErrors($data);
        $delta = $this->getDeltaOfErrors($count, $errors);
        return [
            'data' => $data,
            'count' => $count,
            'errors' => $errors,
            'delta' => $delta,
        ];

    }

    /**
     * Возвращает количество ошибок
     *
     * @param Collection $data Массив объектов статистики
     *
     * @return int
     */
    private function getCountOfErrors(Collection $data)
    {
        $count = 0;
        foreach ($data as $k => $v) {
            $count += $v->errors;
        }

        return $count;
    }

    /**
     * Возвращает среднее количество ошибок
     *
     * @param int $count  Количество вопросов
     * @param int $errors Количество ошибок
     *
     * @return float|int
     */
    private function getDeltaOfErrors($count, $errors)
    {
        return ($count > 0 && $errors > 0) ? ($count / $errors) : 1;
    }

    /**
     * Возвращает данные статистики за период
     *
     * @param array $params Параметры
     *
     * @return mixed
     */
    public function accumulateData($params)
    {
        $questions = $this->statRepository->getQuestionDataForPeriod($params['from'], $params['to']);
        $answers = $this->statRepository->getAnswerDataForPeriod($params['from'], $params['to']);

        $data = [
            'count_answers' => $this->getCountOfAnswers($answers),
            'complete_questions' => $this->getCountOfQuestions($questions),
            'topWrongAnswer' => $this->getTopWrongAnswer($answers),
            'topWrongQuestion' => $this->getTopWrongQuestion($questions)
        ];

        $data['saved'] = $this->statRepository->saveStatSnapshot($data, $params);

        return $data;
    }

    /**
     * Возвращает количество ответов
     *
     * @param array $answers Коллекция ответов
     *
     * @return array
     */
    private function getCountOfAnswers($answers)
    {
        $count = 0;
        foreach ($answers as $answer) {
            $count += ($answer->is_right === false) ? 1 : 0;
        }

        return [
            'right' => $count,
            'wrong' => count($answers) - (int)$count
        ];
    }

    /**
     * Врщвращает количество вопросов
     *
     * @param array $questions Коллекция вопросов
     *
     * @return array
     */
    private function getCountOfQuestions($questions)
    {
        $right = 0;

        foreach ($questions as $question) {
            $right += ($question->errors === 0) ? 1 : 0;
        }

        return [
            'right' => $right,
            'wrong' => count($questions) - (int)$right
        ];
    }

    /**
     * Возвращает топ неправильных ответов
     *
     * @param array $answers Коллекция ответов
     *
     * @return array
     */
    private function getTopWrongAnswer($answers)
    {
        $tmp = [];
        foreach ($answers as $answer) {
            if ((bool)$answer->is_right === false) {
                $tmp[$answer->question_id]['answer_id'] = $answer->answer_id;
                isset($tmp[$answer->question_id]['count']) ? $tmp[$answer->question_id]['count']++ : $tmp[$answer->question_id]['count'] = 1;
            }
        }

        return array_slice($tmp, 0, self::TOP_ANSWERS_COUNT, true);
    }

    /**
     * Возвращает топ неправильныз вопросов
     *
     * @param array $questions Коллекция вопросов
     *
     * @return array
     */
    private function getTopWrongQuestion($questions)
    {
        $tmp = [];
        foreach ($questions as $question) {
            if ($question->errors > 0) {
                array_key_exists($question->ticket_id, $tmp) ? $tmp[$question->ticket_id]++ : $tmp[$question->ticket_id] = 1;
            }
        }

        return array_slice($tmp, 0, self::TOP_QUESTION_COUNT, true);
    }

    /**
     * Возвращает снэпшот статистики по периоду
     *
     * @param string $period Период
     *
     * @return array
     */
    public function getLastStatSnapshotByPeriod($period)
    {
        $data = $this->statRepository->getLastStatSnapshotByPeriod($period);
        $data['date'] = $data->from . ' - ' . $data->to;
        $data['id'] = $data->id;
        $data['data'] = $this->fillQuestions(unserialize($data->snapshot));

        return $data;
    }

    /**
     * Наполняет массив ответов вопросами
     *
     * @param array $data Массив ответов из снопшота статистики
     *
     * @return mixed
     */
    private function fillQuestions($data)
    {
        $answers = $data['topWrongAnswer'];
        $answer_items = $this->answerRepository->getAnswersByIds(array_unique(array_values(array_map(
            function ($e) {
                return $e['answer_id'];
            },
            $answers
        ))));

        foreach ($answers as $k => $answer) {
            $data['topWrongAnswer'][$k] = [
                'question_id' => $answer_items[$k]->question_id,
                'question_title' => $answer_items[$k]->question->question,
                'question_n' => $answer_items[$k]->question->n,
                'wrong_answer_text' => $answer_items[$k]->answer,
                'wrong_answer_n' => $answer_items[$k]->n,
                'wrong_answer_count' => $answer['count'],
                'category_id' => $answer_items[$k]->question->category_id,
                'subject_id' => $answer_items[$k]->question->subject_id,
                'ticket_id' => $answer_items[$k]->question->ticket_id
            ];
        }

        usort($data['topWrongAnswer'], [UtilsHelper::class, 'sort']);

        return $data;
    }
}
