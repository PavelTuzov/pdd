<?php

namespace App\Helpers;

use App\Models\School;
use App\Models\User;
use App\Repositories\UserRepository;
use Auth;

/**
 * Class UserHelper
 */
class UserHelper
{

    /**
     * Дефолтные настройки пользователя
     */
    const DEFAULT_USER_OPTIONS = [
        'autoNextOnSuccess' => false,
        'randAnswers' => false,
    ];

    /**
     * Модель пользователя
     *
     * @var User
     */
    private $user;

    /**
     * Репозиторий пользовательских данных
     *
     * @var UserRepository
     */
    private $repository;

    /**
     * UserHelper constructor.
     *
     * @param UserRepository $repository Репозиторий пользователей
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Устанавливает пользователя
     *
     * @param User $user Пользователь
     *
     * @return void
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Возвращает экземпляр пользователя
     *
     * @param int $id Идентификатор пользователя
     *
     * @return User
     */
    public function getUser($id)
    {
        return $this->repository->getById($id);
    }

    /**
     * Возвращает данные о пользователе
     *
     * @return User
     */
    public function getAuthorizedUserData()
    {
        return Auth::user();
    }

    /**
     * Устанавливает опции поверх дефолтных
     *
     * @param array $options Опции
     *
     * @return mixed
     */
    public function setOptions($options)
    {
        $data = [];
        foreach (self::DEFAULT_USER_OPTIONS as $k => $v) {
            if (is_array($options) && array_key_exists($k, $options)) {
                $data[$k] = (bool)$options[$k];
            } else {
                $data[$k] = (bool)$v;
            }
        }

        return $data;
    }

    /**
     * Устанавливает школу для пользователей
     *
     * @param array|int|User $userData Пользователи
     * @param School         $school   Школа
     *
     * @return bool
     */
    public function setSchool($userData, $school)
    {
        if (is_object($school) === false || $school instanceof School === false) {
            return false;
        }

        if (is_array($userData)) {
            $users = $this->repository->getByIds($userData);
            $result = [];
            foreach ($users as $user) {
                $result[] = $this->repository->setSchoolToUser($user, $school);
            }

            return !in_array(false, $result, true);
        }

        if (is_int($userData)) {
            $user = $this->repository->getById($userData);
            return $this->repository->setSchoolToUser($user, $school);
        }

        if (is_object($userData) && $userData instanceof User) {
            return $this->repository->setSchoolToUser($userData, $school);
        }

        return false;
    }

    /**
     * Удаляет школу у пользователя
     *
     * @param array|int| User $userData Пользователи
     *
     * @return bool
     */
    public function unsetSchool($userData)
    {
        if (is_array($userData)) {
            $users = $this->repository->getByIds($userData);
            foreach ($users as $user) {
                $this->repository->unsetSchoolToUser($user);
            }
        }

        if (is_int($userData)) {
            $user = $this->repository->getById($userData);
            $this->repository->unsetSchoolToUser($user);
        }

        if (is_object($userData) && $userData instanceof User) {
            return $this->repository->unsetSchoolToUser($userData);
        }

        return false;
    }
}
