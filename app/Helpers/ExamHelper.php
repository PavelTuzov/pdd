<?php

namespace App\Helpers;

use App\Repositories\IRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * Class ExamHelper
 */
class ExamHelper
{
    /**
     * Репозиторий
     *
     * @var QuestionRepository
     */
    private $repository;

    private $usedTickets = [];

    /**
     * Иденьтифакторы блоков
     */
    const BLOCK_IDS = [
        1,
        2,
        3,
        4
    ];

    /**
     * Подготавливает данные для жкзамена
     *
     * @return Collection
     * @throws \BadMethodCallException
     */
    public function prepareData()
    {
        $items = [];
        foreach (self::BLOCK_IDS as $k => $block) {
            $ticket = $this->getRandTicket();
            $hash = $block . $ticket;

            $data = Cache::tags(['tickets'])->rememberForever(
                $hash,
                function () use ($block, $ticket) {
                    return $this->repository->getQuestionsForExamByBlock($block, $ticket, 5)->keyBy('id')->all();
                }
            );

            $items = array_merge($items, $data);
        }

        return collect($items);
    }

    /**
     * Устанавливает репозиторий для работы
     *
     * @param IRepository $repository Репозиторий
     *
     * @return void
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Возвращает случайный билет
     *
     * @return int
     */
    private function getRandTicket()
    {
        $ticket = mt_rand(1, 40);

        if (in_array($ticket, $this->usedTickets, true)) {
            $ticket = $this->getRandTicket();
        }

        $this->usedTickets[] = $ticket;

        return $ticket;
    }
}