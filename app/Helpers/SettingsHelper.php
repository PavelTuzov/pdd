<?php

namespace App\Helpers;

use App\Repositories\SettingsRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Класс по работе с настройками
 */
class SettingsHelper
{

    /**
     * Возвращает коллекцию школ
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws \BadMethodCallException
     */
    public static function getSettings()
    {
        $repository = new SettingsRepository;
        if (env('CACHE_DRIVER') === 'memcached') {
            return Cache::tags(['settings'])->rememberForever(
                'schools',
                function () use ($repository) {
                    return $repository->getAllSettings();
                }
            );
        }

        return $repository->getAllSettings();
    }

    /**
     * Возвращает значение параметра
     *
     * @param $name
     * @param $arguments
     *
     * @return null
     */
    public static function __callStatic($name, $arguments)
    {
        $settings = self::getSettings();

        if (array_key_exists($name, $settings)) {
            return $settings[$name]['value'];
        }

        return null;
    }
}
