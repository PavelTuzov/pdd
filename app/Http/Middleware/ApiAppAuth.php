<?php

namespace App\Http\Middleware;

use App\Models\Application;
use Closure;
use Debugbar;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class ApiAppAuth
 */
class ApiAppAuth
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Debugbar::disable();
        $authToken = $request->bearerToken();

        try {
            $this->payloadIsValid(
                $payload = (array) JWT::decode($authToken, getenv('APP_KEY'), ['HS256'])
            );

            $app = Application::where('key', $payload['sub'])->firstOrFail();
        } catch (\Firebase\JWT\ExpiredException $e) {
            return response('token_expired', 401);
        } catch (\Throwable $e) {
            return response('token_invalid', 401);
        }

        if ($app->is_active === false) {
            return response('app_inactive', 403);
        }

        // Получив инстанс аутентифицированного приложения
        // передаем его в Request. Это позволит нам
        // иметь легкий доступ к  инстансу приложения повсеместно.
        $request->merge(['__authenticatedApp' => $app]);

        return $next($request);
    }

    private function payloadIsValid($payload)
    {
        $validator = Validator::make($payload, [
            'iss' => 'required|in:' . getenv('API_NAME'),
            'sub' => 'required',
        ]);

        if (! $validator->passes()) {
            throw new \InvalidArgumentException;
        }
    }
}
