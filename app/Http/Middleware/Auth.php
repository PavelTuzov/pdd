<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class Auth
 */
class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param Request  $request Запрос
     * @param \Closure $next    Замыкание
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return $next($request);
        }

        return redirect('/')->with('notify', 'Вы не авторизованы');
    }
}