<?php

namespace App\Http\Controllers;

use App\Repositories\SignRepository;

/**
 * Class AnswerController
 */
class SignController extends Controller
{

    /**
     * Репозиторий знаков
     *
     * @var SignRepository
     */
    private $signRepository;

    /**
     * SignController constructor.
     *
     * @param SignRepository $signRepository Репозиторий знаков
     */
    public function __construct(SignRepository $signRepository)
    {
        $this->signRepository = $signRepository;
    }

    /**
     * Выводит отображение категории знаков
     *
     * @param int $category_id Идентификатор категории
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($category_id = null)
    {
        $data['root'] = false;
        if ($category_id === null) {
            $data['root'] = true;
            $data['items'] = $this->signRepository->getSignRootList();
        } else {
            $data['items'] = $this->signRepository->getSignsByCategory($category_id);
        }

        return view('sign.list', $data);
    }

    /**
     * Возвращает представление дня одного идентификатора
     *
     * @param int $id Идентификатор знака
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item($id)
    {
        $data['item'] = $this->signRepository->getSignById($id);

        return view('sign.item', $data);
    }
}
