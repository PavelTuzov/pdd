<?php

namespace App\Http\Controllers;

use App\Handlers\ImageHandler;
use App\Helpers\StatHelper;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Reference;
use App\Models\Sign;
use App\Models\Ticket;
use App\Repositories\QuestionRepository;
use App\Repositories\ReferenceRepository;
use App\Repositories\SignRepository;
use App\Repositories\StatRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator as Validate;
use Illuminate\View\View;

/**
 * Class AdminController
 */
class AdminController extends Controller
{
    private static $paginate = 15;

    private $options = [
        'menu' => ['prefix' => '/admin']
    ];

    const MENU = [
        'index' => [
            'url' => '/',
            'title' => 'Главная страница',
        ],
        'questions' => [
            'url' => '/questions',
            'title' => 'Вопросы',
        ],
        'references' => [
            'url' => '/references',
            'title' => 'Правила'
        ],
        'signs' => [
            'url' => '/signs',
            'title' => 'Знаки'
        ],
        'stat' => [
            'url' => '/stat',
            'title' => 'Статистика'
        ]
    ];

    /**
     * Репозиторий для работы с хранилищем с правилами
     *
     * @var ReferenceRepository
     */
    private $referenceRepository;

    /**
     * Репозиторий для работы с хранилищем вопросов
     *
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * Репозиторий для работы с хранилищем знаков
     *
     * @var SignRepository
     */
    private $signRepository;

    /**
     * Репозиторий для работы с хранилищем статистики
     *
     * @var StatHelper
     */
    private $statHelper;

    /**
     * Конструктор
     *
     * @param ReferenceRepository $referenceRepository Репозиторий правил
     * @param QuestionRepository  $questionRepository  Репозиторий вопросов
     * @param SignRepository      $signRepository      Репозиторий знаков
     * @param StatHelper          $statHelper          Хэлпер статистики
     */
    public function __construct(
        ReferenceRepository $referenceRepository,
        QuestionRepository $questionRepository,
        SignRepository $signRepository,
        StatHelper $statHelper
    ) {
        $this->middleware('auth');
        $this->referenceRepository = $referenceRepository;
        $this->signRepository      = $signRepository;
        $this->questionRepository  = $signRepository;
        $this->statHelper = $statHelper;
    }

    /**
     * Отображает главную страницу админ-панели
     *
     * @return Factory|View
     */
    public function index()
    {
        $data['menu'] = $this->buildMenu();
        return view('admin.pages.index', $data);
    }

    /**
     * Строит меню
     *
     * @return array
     */
    private function buildMenu()
    {
        $menu = [];
        if (count(self::MENU) > 0) {
            foreach (self::MENU as $k => $v) {
                if (array_key_exists('prefix', $this->options['menu'])) {
                    $v['url'] = $this->options['menu']['prefix'] . $v['url'];
                }

                $menu[$k] = $v;
            }
        }

        return $menu;
    }

    /**
     * Показывает билеты
     *
     * @return Factory|View
     *
     * @throws \InvalidArgumentException
     */
    public function tickets()
    {
        $data['items'] = Ticket::paginate(self::$paginate);
        $data['menu'] = $this->buildMenu();

        return view('admin.pages.tickets', $data);
    }

    /**
     * Показывает билет
     *
     * @param int $id Идентификатор
     *
     * @return View
     */
    public function ticket($id)
    {
        $data['item'] = Ticket::where('id', $id)->first();
        $data['menu'] = $this->buildMenu();

        return view('admin.pages.ticket_edit', $data);
    }

    /**
     * Показывает вопросы
     *
     * @param Request $request   Запрос
     * @param int     $ticket_id Идентификатор вопроса
     *
     * @return View
     */
    public function questions(Request $request, $ticket_id = 1)
    {
        $data['items'] = Question::where('ticket_id', $ticket_id)
            ->orderBy('n', 'ASC')
            ->get();
        $data['menu'] = $this->buildMenu();
        $data['tickets'] = Question::select(['ticket_id'])
            ->groupBy('ticket_id')
            ->orderBy('ticket_id', 'ASC')
            ->get();

        return view('admin.pages.questions', $data);
    }

    /**
     * Сохраняет вопрос
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор вопроса
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function saveQuestion(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $validator = Validate::make(
                $request->request->all(),
                [
                    'question' => ['required', 'min:5'],
                    'category_id' => ['required'],
                    'ticket_id' => ['required'],
                    'n' => ['required'],
                ]
            );

            $errors = [];

            if ($validator->fails() === true) {
                $errors = $validator->messages();
            }

            if ($id === null) {
                $data['item'] = new Question();
            } else {
                $data['item'] = Question::where('id', $id)->with('answers')->first();
            }

            $data['item']->fill($request->request->all());
            if ($request->hasFile('image')) {
                $data['item']->image = $this->attachImage($request, 'questions');
            }


            if (count($errors) === 0 && $data['item']->save()) {
                    if ($this->saveAnswersForQuestion($data['item'], $request)){

                        $put = [
                            'last_item' => ((int)$data['item']->n <= 19) ? ($data['item']->n+1) : 1,
                            'ticket_id' => $data['item']->ticket_id
                        ];
                        $request->session()->put('ticket', $put);
                    }
                if (env('CACHE_DRIVER') === 'memcached') {
                    Cache::clear('ticker');
                }
            }

            return redirect()->action(
                'AdminController@question',
                [
                    'ticket_id' => $data['item']->ticket_id,
                    'id' => $data['item']->id
                ]
            );
        }
    }


    /**
     * Показывает вопрос
     *
     * @param Request $request   Запрос
     * @param int     $ticket_id Идентификатор билета
     * @param int     $id        Идентификатор вопроса
     *
     * @return View
     * @throws \Exception
     */
    public function question(Request $request, $ticket_id = null, $id = null)
    {
        if ($id === null) {
            $data['item'] = new Question();
        } else {
            $data['item'] = Question::where('id', $id)->with(['withHint'])->first();
        }

        $data['menu'] = $this->buildMenu();
        $data['tickets'] = Question::select(['ticket_id'])
            ->groupBy('ticket_id')
            ->orderBy('ticket_id', 'ASC')
            ->get();
        $data['categories'] = $this->getCategories();
        $data['subjects'] = $this->getSubjects();
        $data['tickets_list'] = $this->extrudeData($data['tickets']);
        $data['preset'] = $request->session()->has('ticket') ? $request->session()->get('ticket') : ['ticket_id' => null, 'last_item' => null];


        return view('admin.pages.question', $data);
    }


    /**
     * Меняет порядок ответов
     *
     * @param Request $request Запрос
     *
     * @return void
     */
    public function changeAnswerOrder(Request $request)
    {
        $order = $request->get('order');
        $order = json_decode($order);
        $order = explode(',', $order[0]);
        foreach ($order as $k => $v) {
            $save = Answer::find($v);
            $save->n = ($k + 1);
            $save->save();
        }
    }

    /**
     * Показывает страницу со списком правил
     *
     * @param Request $request Запрос
     * @param int     $root_id Идентификатор коневого пункта правил
     *
     * @return View
     */
    public function references(Request $request, $root_id = 1)
    {
        $data['menu'] = $this->buildMenu();
        $data['root_items'] = $this->referenceRepository->getRootReferences();
        $data['items'] = $this->referenceRepository->getReferenceTree($root_id);

        return view('admin.pages.references', $data);
    }

    /**
     * Показывает страницу с правилом
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор
     *
     * @return View
     */
    public function reference(Request $request, $id = null)
    {
        $data['menu'] = $this->buildMenu();
        $data['root_items'] = $this->referenceRepository->getRootReferences();
        if ($id !== null) {
            $data['item'] = $this->referenceRepository->getReferenceById($id);
        } else {
            $data['item'] = new Reference();
        }

        return view('admin.pages.reference', $data);
    }

    /**
     * Возвращает список правил в автокомплит
     *
     * @param Request $request Запрос
     *
     * @return mixed
     */
    public function getReferenceList(Request $request)
    {
        $query = $request->query('query');
        $data['items'] = $this->referenceRepository->getReferenceList($query);

        return $data;
    }

    /**
     * Возваращает правило 
     *
     * @param Request $request Запрос
     *
     * @return mixed
     */
    public function getReferenceById(Request $request)
    {
        $query = (int) $request->query('id');

        return $this->referenceRepository->getReferenceById($query);
    }

    /**
     * Сохраняет правило
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор вопроса
     *
     * @return RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function saveReference(Request $request, $id = null)
    {
        $data = [
            'id' => $id,
            'text' => $request->text,
            'parent_id' => $request->parent_id,
            'n' => $request->n,
        ];

        $exist = $this->referenceRepository->checkReferenceExists($id);
        if ($exist === null) {
            $result = $this->referenceRepository->saveReference($data);
        } else {
            $result = $this->referenceRepository->updateReference($exist, $data);
        }



        if ($result !== null) {
            return redirect()->action(
                'AdminController@reference',
                [
                    'id' => $result->id
                ]
            );
        }
    }

    /**
     * Показывает страницу со списком знаков
     *
     * @param Request $request     Запрос
     * @param int     $category_id Идентификатор категории знаков
     *
     * @return View
     */
    public function signs(Request $request, $category_id = 1)
    {
        $data['menu'] = $this->buildMenu();
        $data['root_items'] = $this->signRepository->getSignRootList();
        $data['items'] = $this->signRepository->getSignsByCategory($category_id);

        return view('admin.pages.signs', $data);
    }

    /**
     * Показывает страницу со знаком
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор
     *
     * @return View
     */
    public function sign(Request $request, $id = null)
    {
        $data['menu'] = $this->buildMenu();
        $data['root_items'] = $this->signRepository->getSignRootList();
        $data['category_list'] = $this->signRepository->getSignsCategoryList();
        $data['preset'] = $request->session()->has('sign') ? $request->session()->get('sign') : ['category_id' => null, 'last_item' => null];
        if ($id !== null) {
            $data['item'] = $this->signRepository->getSignById($id);
        } else {
            $data['item'] = new Sign();
        }

        return view('admin.pages.sign', $data);
    }

    /**
     * Сохраняет знак
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор знака
     *
     * @return RedirectResponse
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function saveSign(Request $request, $id = null)
    {
        $data = [
            'id' => $id,
            'text' => $request->text,
            'title' => $request->title,
            'category_id' => $request->category_id,
            'n' => $request->n,
        ];

        if ($request->hasFile('image')) {
            $data['image'] = $this->attachImage($request, 'signs');
        }


        $exist = $this->signRepository->checkSignExists($id);
        if ($exist === null) {
            $result = $this->signRepository->saveSign($data);
        } else {
            $result = $this->signRepository->updateSign($exist, $data);
        }

        if ($result !== null) {

            $put = [
                'last_item' => $result->n,
                'category_id' => $result->category_id
            ];
            $request->session()->put('sign', $put);

            return redirect()->action(
                'AdminController@sign',
                [
                    'id' => $result->id
                ]
            );
        }
    }

    /**
     * Возвращает результат запроса знаков
     *
     * @param Request $request Запрос
     *
     * @return array
     */
    public function getSignsByQuery(Request $request)
    {
        $query = $request->query('query');

        if (env('CACHE_DRIVER') === 'memcached') {
            $data = Cache::tags(['admin', 'queries'])->rememberForever(
                'admin__sign_query_' . md5($query),
                function () use ($query) {
                    return $this->signRepository->getSignByQuery($query);;
                }
            );
        } else {
            $data = $this->signRepository->getSignByQuery($query);;
        }

        return $data;
    }


    /**
     * Возвращает результат запроса правил
     *
     * @param Request $request Запрос
     *
     * @return array
     */
    public function getRulesByQuery(Request $request)
    {
        $query = $request->query('query');

        $data = Cache::tags(['admin', 'queries'])->rememberForever(
            'admin__rule_query ' . md5($query),
            function () use ($query) {
                return $this->referenceRepository->getReferenceList($query);;
            }
        );

        return $data;
    }

    /**
     * Отображает страницу статистики
     *
     * @param Request $request Запрос
     * @param string  $period  Период
     *
     * @return View
     */
    public function stat(Request $request, $period = null)
    {
        $snapshot = $this->statHelper->getLastStatSnapshotByPeriod($period);

        $data['menu'] = $this->buildMenu();
        $data['item'] = $snapshot['data'];
        $data['date'] = $snapshot['date'];
        $data['id'] = $snapshot['id'];

        return view('admin.pages.stat', $data);
    }

    /**
     * Возвращает список категорий вождения
     *
     * @return array
     */
    private function getCategories()
    {
        return [
            '1' => 'AB',
            '2' => 'CD',
        ];
    }

    /**
     * Вычленяет данные из модели в список
     *
     * @param array $data   Коллекция
     * @param array $fields Поля для вычленения
     *
     * @return array
     */
    private function extrudeData($data, array $fields = array('ticket_id', 'title'))
    {
        $response = [];
        foreach ($data as $k => $v) {
            $response[$v->$fields[0]] = 'Билет ' . $v->$fields[0];
        }

        return $response;
    }

    /**
     * Возвращает темы билетов
     *
     * @return array
     */
    private function getSubjects()
    {
        return [
            '1' => 'Тема 1'
        ];
    }

    /**
     * Добавляет ответы к вопросу
     *
     * @param Question $question Вопрос
     * @param Request  $request  Запрос
     *
     * @throws \Exception
     *
     * @return array
     */
    private function saveAnswersForQuestion(Question $question, Request $request)
    {
        $collection = [];

        foreach ($request->answers as $k => $answer) {
            $answer['n'] = ($k + 1);
            $answer['question_id'] = $question->id;
            $answer['right'] = (bool)(array_key_exists('right', $answer) === true && empty($answer['right']) === false) ? true : false;
            $save = new Answer($answer);
            if (array_key_exists('reference_', $answer) && (int) $answer['reference_'] > 0) {
                $save->withHint()->attach($answer['reference_id']);
            }

            $collection[] = $save;
        }

        Answer::where('question_id', $question->id)->delete();
        $question->answers()->saveMany($collection);

        return $collection;
    }

    /**
     * Сохраняет изображение
     *
     * @param Request $request Запрос
     * @param string  $dir     Директория
     *
     * @return bool
     */
    private function attachImage(Request $request, $dir)
    {
        $file = Input::file('image');
        return ImageHandler::upload($file, $dir);
    }
}
