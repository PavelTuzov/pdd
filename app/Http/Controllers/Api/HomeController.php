<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function appData(Request $request){
        return $request->__authenticatedApp;
    }

    public function userData(Request $request){}
}