<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthController
 */
class AuthController extends Controller
{
    /**
     * Авторизует приложение
     *
     * @param Request $request Запрос
     *
     * @return ResponseFactory|Response
     */
    public function authenticateApp(Request $request)
    {
        $authHeader = $request->header('authorization');
        $credentials = base64_decode(
            Str::substr($authHeader, 6)
        );

        try {
            list($appKey, $appSecret) = explode(':', $credentials);

            $app = Application::where('key', $appKey)->where('secret', $appSecret)->firstOrFail();
        } catch (\Throwable $e) {
            return response('invalid_credentials', 400);
        }

        if (!$app->is_active) {
            return response('app_inactive', 403);
        }

        return response([
            'token_type'   => 'Bearer',
            'access_token' => $app->generateAuthToken(),
        ]);
    }

    public function authenticateUser(Request $request)
    {
    }

    public function logoutUser(Request $request)
    {
    }
}

?>