<?php

namespace App\Http\Controllers\Api;

use App\Helpers\QuestionHelper;
use App\Models\Parsed\Question;
use App\Repositories\IRepository;
use App\Repositories\QuestionRepository;
use App\Repositories\StatRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;

/**
 * Class TicketController
 */
class TicketController extends ApiController
{

    /**
     * Репозиторий запросов к вопросам
     *
     * @var IRepository
     */
    private $questionRepository;

    /**
     * Репозиторий запросов к стастистике
     *
     * @var IRepository
     */
    private $statRepository;

    /**
     * TicketController constructor.
     *
     * @param QuestionRepository $questionRepository Репозиторий билетов
     * @param StatRepository     $statRepository     Экземпляр репозитория
     */
    public function __construct(QuestionRepository $questionRepository, StatRepository $statRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->statRepository = $statRepository;
    }

    /**
     * Возвращает коллекцию билетов
     *
     * @param Request $request Запрос
     *
     * @return string
     */
    public function tickets(Request $request)
    {
        $items = Cache::tags(['tickets'])->rememberForever('allTickets', function () {
            return $this->questionRepository->getTickets();
        });

        $data['items'] = QuestionHelper::getStat($items, $this->statRepository);

        return response($data);
    }
}
