<?php

namespace App\Http\Controllers;

use App\Helpers\AnswerHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;


/**
 * Class AnswerController
 */
class AnswerController extends Controller
{

    /**
     * Хэлпер ответов
     *
     * @var AnswerHelper
     */
    private $answerHelper;

    /**
     * AnswerController constructor.
     *
     * @param AnswerHelper $answerHelper Хэлпер ответов
     */
    public function __construct(AnswerHelper $answerHelper)
    {
        $this->answerHelper = $answerHelper;
    }

    /**
     * Проверяет ответ
     *
     * @param int     $id      Идентификатор ответа
     * @param Request $request Экземпляр запроса
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAnswer($id)
    {
        if (env('CACHE_DRIVER') === 'memcached2') {
            $data = Cache::tags(['answers'])->rememberForever('answer' . $id, function () use ($id) {
                return $this->answerHelper->checkAnswer($id, true, true);
            });
        } else {
            $data = $this->answerHelper->checkAnswer($id, true, true);
        }

        if (env('APP_ENV') !== 'debug') {
            $this->answerHelper->saveAnswerStat($data);
        }

        return response()->json($data);
    }

}
