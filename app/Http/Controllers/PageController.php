<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class PageController
 */
class PageController extends Controller
{
    /**
     * Возвращает отображение главной страницы
     *
     * @return View;
     */
    public function index()
    {
        $data = [];
        return view('pages.index', $data);
    }
}
