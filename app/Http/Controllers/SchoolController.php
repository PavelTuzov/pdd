<?php

namespace App\Http\Controllers;

use App\Helpers\DropdownHelper;
use App\Helpers\SchoolHelper;
use Illuminate\Http\Request;

/**
 * Class CityController
 */
class SchoolController extends Controller
{
    /**
     * Хелпер школ
     *
     * @var SchoolHelper
     */
    private $helper;

    /**
     * SchoolController constructor.
     *
     * @param SchoolHelper $helper Хэлпер школ
     */
    public function __construct(SchoolHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Возвращает json представление списка школ
     *
     * @param Request $request Запрос
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request, $city_id)
    {

        return response()->json(DropdownHelper::generateList($this->helper->getSchoolsByCityId($city_id)));
    }
}
