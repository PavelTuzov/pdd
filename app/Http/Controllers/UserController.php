<?php
/**
 * Created by PhpStorm.
 * User: pk
 * Date: 31.03.17
 * Time: 10:26
 */

namespace App\Http\Controllers;

use App\Handlers\ImageHandler;
use App\Helpers\MenuHelper;
use App\Helpers\StatHelper;
use App\Helpers\UserHelper;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use View;


/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * Путь к фотографиям пользователя
     */
    const IMAGE_PATH = '/images/user/%s/%s';

    /**
     * Размеры изображений
     */
    const IMAGE_SIZES = [
        'xs' => self::IMAGE_SIZE_XS,
        's' => self::IMAGE_SIZE_S,
        'm' => self::IMAGE_SIZE_M,
        'l' => self::IMAGE_SIZE_L
    ];

    /**
     * Очень маленькие изображения
     */
    const IMAGE_SIZE_XS = 'xs';

    /**
     * Маленькие изображения
     */
    const IMAGE_SIZE_S = 's';

    /**
     * Средние изображения
     */
    const IMAGE_SIZE_M = 'm';

    /**
     * Большие изображения
     */
    const IMAGE_SIZE_L = 'l';

    /**
     * Хелпер пользователей
     *
     * @var UserHelper
     */
    private $userHelper;

    /**
     * Хелпер статистики
     *
     * @var StatHelper
     */
    private $statHelper;

    private $menu = [];

    /**
     * UserController constructor.
     *
     * @param UserHelper $userHelper Хелпер пользователей
     * @param StatHelper $statHelper Хелпер статистики
     */
    public function __construct(UserHelper $userHelper, StatHelper $statHelper)
    {
        $this->userHelper = $userHelper;
        $this->statHelper = $statHelper;

        view()->share('innerMenu', MenuHelper::getMenu(MenuHelper::$userLinks));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data Данные для валидации
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'name'     => 'required|max:255',
                'email'    => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
            ]
        );
    }


    /**
     * Сохраняет данные о пользователе
     *
     * @param Request $request Запрос
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $item = $this->userHelper->getUser(Auth::user()->id);
        $item->name = $request->name;
        if ($request->hasFile('image')) {
            $file = Input::file('image');
            $name = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
            $image = ImageHandler::save($file, $name, 'user', self::IMAGE_SIZES);
            $item->image = $image;
        }

        if (isset($request->deleteAvatar) && $request->deleteAvatar) {
            $item->image = false;
        }

        if ($item->save()) {
            return Redirect::back();
        }
    }

    /**
     * Сохраняет настройки пользователя
     *
     * @param Request $request Запрос
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveOptions(Request $request)
    {
        $item = $this->userHelper->getUser(Auth::user()->id);
        $item->options = null;
        if (count($request->options) > 0) {
            $item->options = json_encode($request->options);
        }

        $item->save();

        return Redirect::back();

    }

    /**
     * Выводит индекс раздела пользователей
     *
     * @return view
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Возвращает статистику пользователя
     *
     * @return array
     */
    public function stat()
    {
        $data = [
            'stat' => $this->statHelper->getStatByUser(),
            'user' => $this->addAvatarPath($this->userHelper->getAuthorizedUserData()),
        ];

        return $data;
    }

    /**
     * Отображает страницу редактирования пользователя
     *
     * @return View
     */
    public function edit()
    {
        $data = [
            'wide' => true,
            'data' => $this->addAvatarPath(
                Auth::user(),
                self::IMAGE_SIZE_S
            ),
        ];

        return view('user.edit', $data);
    }

    /**
     * Вывод страницы с настройками пользователя
     *
     * @param null $id Идентификатор пользователя
     *
     * @return View
     */
    public function options($id = null)
    {
        $data = [
            'wide' => true,
            'options' => $this->userHelper->setOptions(json_decode(Auth::user()->options, true))
        ];


        return view('user.options', $data);
    }

    /**
     * Вывод страницы с историей
     *
     * @param \Request $request Запрос
     *
     * @return View
     */
    public function history(\Request $request)
    {
        $this->statHelper
            ->setRequest($request)
            ->getStat();

        return view('user.history');
    }

    /**
     * Устанавливает путь до изображения
     *
     * @param User   $user Экземпляр пользователя
     * @param string $size Размер изображения
     *
     * @return User
     */
    private function addAvatarPath($user, $size = null)
    {
        if ($user->image !== null) {
            $user->image = sprintf(self::IMAGE_PATH, ($size === null) ? self::IMAGE_SIZE_M : $size, $user->image);
        }

        return $user;
    }

}
