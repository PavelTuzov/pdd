<?php

namespace App\Http\Controllers;

use App\Helpers\Bot\Hooks\AcceptHookCommand;
use App\Helpers\Bot\Hooks\AddHookCommand;
use App\Helpers\Bot\Hooks\BotInfoHookCommand;
use App\Helpers\Bot\Hooks\GetUpdatesHookCommand;
use App\Helpers\Bot\Hooks\RemoveHookCommand;
use App\Helpers\BotHelper;
use Debugbar;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;

/**
 * Class TelegramController
 */
class TelegramController extends Controller
{

    /**
     * Хелпер ботов
     *
     * @var BotHelper
     */
    private $helper;

    /**
     * TelegramController constructor.
     *
     * @param BotHelper $helper Хэлпер
     */
    public function __construct(BotHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Возвращает данные
     *
     * @return array
     */
    public function getMe()
    {
        Log::debug('getMe requested');

        return $this->helper->execCommand(new BotInfoHookCommand());
    }

    /**
     * Возвращает данные
     *
     * @return mixed
     */
    public function getUpdate()
    {
        return $this->helper->execCommand(new GetUpdatesHookCommand());
    }

    /**
     * Добавляет хук
     *
     * @return mixed
     */
    public function addHook()
    {
        return $this->helper->execCommand(new AddHookCommand());
    }

    /**
     * Удаляет хук
     *
     * @return mixed
     */
    public function removeHook()
    {
        return $this->helper->execCommand(new RemoveHookCommand());
    }

    /**
     * Принимает запрос вебхука
     *
     * @param Request $request Запрос
     * @param string  $token   Токен
     *
     * @return array
     */
    public function webHook(Request $request, $token)
    {
        Debugbar::disable();

        return $this->helper->execCommand(new AcceptHookCommand($request, $token));
    }

    /**
     * Возвращает последний ответ вебхука
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getWebHookAnswer()
    {
        $client = new Client();
        $result = $client->request('get', 'https://api.telegram.org/bot' . getenv('TELEGRAM_BOT_TOKEN') . '/getWebhookInfo');

        return $result->getBody();
    }
}
