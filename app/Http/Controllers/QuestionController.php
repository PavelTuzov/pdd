<?php

namespace App\Http\Controllers;

use App\Helpers\ExamHelper;
use App\Helpers\QuestionHelper;
use App\Helpers\UtilsHelper;
use App\Repositories\IRepository;
use App\Repositories\QuestionRepository;
use App\Repositories\StatRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class QuestionContoller
 *
 * @package App\Http\Controllers
 */
class QuestionController extends Controller
{
    /**
     * Количество дополнительных вопросов
     */
    const COUNT_OF_ADDITIONAL_QUESTIONS = 5;

    /**
     * Репозиторий запросов к вопросам
     *
     * @var IRepository
     */
    private $questionRepository;

    /**
     * Репозиторий запросов к стастистике
     *
     * @var IRepository
     */
    private $statRepository;

    /**
     * QuestionContoller constructor
     *
     * @param QuestionRepository $questionRepository Экземпляр репозитория
     * @param StatRepository     $statRepository     Экземпляр репозитория
     */
    public function __construct(QuestionRepository $questionRepository, StatRepository $statRepository)
    {
        $this->statRepository = $statRepository;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Отображает страницу с вопросом
     *
     * @param int $id Идентификаторв вопроса
     *
     * @return view|NotFoundHttpException
     */
    public function view($id)
    {
        $data = [
            'item' => $this->questionRepository->getQuestionById((int)$id)
        ];

        if ($data['item'] === null) {
            return UtilsHelper::err404();
        }

        return view('question.item', $data);
    }

    /**
     * Отображает страницу со списком вопросов по идентификатору билета
     *
     * @param Request $request Запрос
     * @param int     $id      Идентификатор билета
     *
     * @return View|NotFoundHttpException
     */
    public function ticket(Request $request, $id)
    {
        $data['action'] = $request->get('action', '');
        $data['isExam'] = false;
        $key = 'ticket' . $id;
        if (env('CACHE_DRIVER') === 'memcached') {
            $data['items'] = Cache::tags(['tickets'])->rememberForever(
                $key,
                function () use ($id) {
                    return $this->questionRepository->getQuestionsByTicket($id);
                }
            );
        } else {
            $data['items'] = $this->questionRepository->getQuestionsByTicket($id);
        }

        if (count($data['items']) === 0) {
            return UtilsHelper::err404();
        }

        return view('question.list', $data);
    }

    /**
     * Возвращает страницу со списокм билетов
     *
     * @return View
     */
    public function tickets()
    {
        $data['title'] = 'Список билетов';
        $items = Cache::tags(['tickets'])->rememberForever('allTickets', function () {
            return $this->questionRepository->getTickets();
        });

        $data['items'] = QuestionHelper::getStat($items, $this->statRepository);

        return view('question.tickets', $data);
    }

    /**
     * Возвращает страницу экзамена
     *
     * @param Request $request Запрос
     *
     * @return View
     */
    public function exam(Request $request)
    {
        $helper = new ExamHelper();
        $helper->setRepository($this->questionRepository);

        $data['action'] = $request->get('action', '');
        $data['items'] = $helper->prepareData();
        $data['isExam'] = true;

        return view('question.list', $data);
    }

    /**
     * Возвращает страницу с ошибками
     *
     * @param Request $request Запрос
     *
     * @return View
     */
    public function errorsPage(Request $request)
    {
        $data['isExam'] = false;

        return view('question.list', $data);
    }

    /**
     * Возвращает вопросы
     *
     * @param Request $request Запрос
     *
     * @return mixed
     */
    public function getErrors(Request $request)
    {
        $ids = $request->get('ids', []);

        return $this->questionRepository->getQuestionsByIds($ids);
    }

    /**
     * Возвращает страницу с топ 100 вопросов
     *
     * @param Request $request Запрос
     *
     * @return View
     */
    public function top100(Request $request)
    {
        $data['action'] = $request->get('action', '');
        $data['items'] = $this->questionRepository->getTop100Hardest();
        $data['top100'] = false;

        return view('question.list', $data);
    }

    /**
     * Возвращает дополнительные вопросы к экзамену
     *
     * @param Request $request Запрос
     *
     * @return view|NotFoundHttpException
     */
    public function requestAdditionalQuestions(Request $request)
    {
        if ($request->list === null) {
            return UtilsHelper::err404();
        }

        $question = $request->question;
        $except = array_filter($request->list);
        $hash = implode($except) . implode($question);

        $data = Cache::tags(['tickets'])->rememberForever(
            'additional_' . $hash,
            function () use ($question, $except) {
                return $this->questionRepository->getAdditionalQuestionsForExam($except, $question, self::COUNT_OF_ADDITIONAL_QUESTIONS);
            }
        );

        return $data;
    }

    /**
     * Отображает страницу со списокм вопросов по теме
     *
     * @param int $id Идентификатор темы
     *
     * @return View
     */
    public function subject($id)
    {
        $key = 'subject' . $id;
        $data['items'] = Cache::tags(['subjects'])->rememberForever(
            $key,
            function () use ($id) {
                return $this->questionRepository->getQuestionsBySubject($id);
            }
        );

        return view('question.list', $data);
    }

    /**
     * Отображает страницу со списков вопросов по актуальности
     *
     * @param string $date Дата
     *
     * @return View
     */
    public function date($date)
    {
        $data = [
            'items' => $this->questionRepository->getQuestionByDate($date),
        ];

        return view('question.list', $data);
    }


    /**
     * Возвращает результат при истекшем времени на выполнение задания
     *
     * @param Request $request Запрос
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function timeOut(Request $request)
    {
        $data = [
            'rand' => mt_rand(1, 9999)
        ];

        return response()->json([
            'template' => view('modals._timeout', $data)->render(),
            'data' => $data,
        ]);
    }
}
