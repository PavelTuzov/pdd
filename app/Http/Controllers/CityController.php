<?php

namespace App\Http\Controllers;

use App\Helpers\CityHelper;

/**
 * Class CityController
 */
class CityController extends Controller
{
    /**
     * Хелпер городов
     *
     * @var CityHelper
     */
    private $helper;

    /**
     * CityController constructor.
     *
     * @param CityHelper $helper Хэлпер городов
     */
    public function __construct(CityHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Возвращает json представление списка городов
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        return response()->json($this->helper->getCities());
    }
}
