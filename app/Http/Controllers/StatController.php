<?php

namespace App\Http\Controllers;

use App\Repositories\StatRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class PageController
 */
class StatController extends Controller
{

    private $statRepository;

    /**
     * StatController constructor.
     *
     * @param StatRepository $statRepository Репозиторий запросов
     */
    public function __construct(StatRepository $statRepository)
    {
        $this->statRepository = $statRepository;
    }

    /**
     * Возвращает результат сохранения результатов решения билета
     *
     * @param Request $request Запрос
     *
     * @return mixed
     * @throws \Throwable
     */
    public function save(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->list = array_filter($request->list);
            $answers = $this->checkAnswers($request->list);
            $seconds = strtotime('1970-01-01 00:' . $request->timer . ' UTC');
            $data = [
                'answers' => count($request->list),
                'errors' => (int) $answers['errors'],
                'mode' => 0,
                'ticket_id' => (strlen($request->ticket) > 0) ? (int) $request->ticket : null,
                'time' => (int) $seconds,
                'user_id' => Auth::check() !== false ? Auth::user()->id : null,
            ];

            $modes = ['_is_exam', '_is_errors'];
            if (in_array($request->mode, $modes, true) !== true) {
                $request->mode = '';
            }

            $saved = $this->statRepository->add($data);
            if ($saved !== null) {
                $data['rand'] = mt_rand(1, 9999);

                return response()->json([
                    'template' => view('modals.' . $request->mode . '_result', $data)->render(),
                    'data' => $data,
                ]);

            }

            return false;
        }

        return abort('405');
    }

    /**
     * Возвращает количество правильных и неправильных ответов
     *
     * @param array $answers Ответы
     *
     * @return array
     */
    private function checkAnswers($answers)
    {
        $response = [
            'right' => 0,
            'errors' => 0,
        ];

        foreach ($answers as $answer) {
            if ($answer['right'] === 'true') {
                $response['right']++;
            } else {
                $response['errors']++;
            }
        }

        return $response;
    }
}
