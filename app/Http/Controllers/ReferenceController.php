<?php

namespace App\Http\Controllers;

use App\Repositories\AnswerRepository;
use App\Repositories\ReferenceRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Class AnswerController
 */
class ReferenceController extends Controller
{

    /**
     * Репозиторий правил
     *
     * @var ReferenceRepository
     */
    private $referenceRepository;

    /**
     * AnswerController constructor.
     *
     * @param ReferenceRepository $referenceRepository Репозиторий правил
     */
    public function __construct(ReferenceRepository $referenceRepository)
    {
        $this->referenceRepository = $referenceRepository;
    }
    
    public function rules($n = null)
    {
        $data['root'] = false;
        if ($n === null) {
            $data['items'] = $this->referenceRepository->getRootReferences();
            $data['root'] = true;
        } else {
            $data['item'] = $this->referenceRepository->getReferenceByN($n);
            $data['items'] = $this->referenceRepository->getReferenceByParentId($data['item']->id);
        }

        return view('rules.list', $data);
    }
}
