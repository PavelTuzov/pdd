<?php

namespace App\Repositories;

use App\Helpers\UtilsHelper;
use App\Models\Stat;
use App\Models\StatAnswer;
use App\Models\StatSnapshot;

/**
 * Class StatRepository
 */
class StatRepository implements IRepository
{
    /**
     * Возвращает данные об одном билете по идентификатору статистики
     *
     * @param int $id Идентификатор статистики
     *
     * @return Stat|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return Stat::find('where', $id)->first();
    }

    /**
     * Добавляет в базу данные о решенном билете
     *
     * @param array $data Данные
     *
     * @return bool
     */
    public function add($data)
    {
        $stat = new Stat();
        $stat->answers = $data['answers'];
        $stat->errors = $data['errors'];
        $stat->user_id = $data['user_id'];
        $stat->mode = $data['mode'];
        $stat->ticket_id = $data['ticket_id'];
        $stat->time = $data['time'];

        return $stat->save();
    }

    /**
     * Возвращает статистику по пользователю, группирую по билетам
     *
     * @param int $user_id Идентификатор пользователя
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getStat($user_id)
    {
        return Stat::selectRaw('distinct ticket_id, updated_at, errors, id')
            ->where('user_id', $user_id)
            ->orderBy('id', 'asc')
            ->get()->keyBy('ticket_id');
    }

    /**
     * Возвращает данные по вопросам за период
     *
     * @param string $from Дата от
     * @param string $to   Дата до
     *
     * @return mixed
     */
    public function getQuestionDataForPeriod($from, $to)
    {
        return Stat::selectRaw('distinct on (ticket_id) ticket_id, errors, id')
            ->where([
                ['created_at', '>=', $from],
                ['created_at', '<=', $to],
            ])
            ->get()->keyBy('id');
    }

    /**
     * Возвращает данные по ответам за период
     *
     * @param string $from Дата от
     * @param string $to   Дата до
     *
     * @return mixed
     */
    public function getAnswerDataForPeriod($from, $to)
    {
        return StatAnswer::where([
            ['date', '>=', $from],
            ['date', '<=', $to]
        ])
        ->get()->keyBy('id');
    }


    /**
     * Сохраняет данные статистики за период
     *
     * @param array $data   Данные статистики
     * @param array $params Параметры
     *
     * @return bool
     */
    public function saveStatSnapshot($data, $params)
    {
        $stat = new StatSnapshot();

        $stat->snapshot = serialize($data);
        $stat->from = $params['from'];
        $stat->to = $params['to'];
        $stat->period = UtilsHelper::getPeriodFromData($params);

        return $stat->save();
    }

    /**
     * Возвращает последний снэпшот по типу периода
     *
     * @param string $period Период
     *
     * @return StatSnapshot
     */
    public function getLastStatSnapshotByPeriod($period = null)
    {
        return StatSnapshot::where('period', ($period !== null) ? $period : UtilsHelper::PERIOD_ONE_MONTH)
            ->orderBy('id', 'DESC')
            ->first();
    }


    /**
     * Возвращает последний снэпшот по типу периода и дате
     *
     * @param string $period Период
     * @param string $date   Дата
     *
     * @return mixed
     */
    public function getLastStatSnapshotByPeriodAndDate($period = null, $date = null)
    {
        return StatSnapshot::select('*')
            ->where([
                ['period',  '=', ($period !== null) ? $period : UtilsHelper::PERIOD_ONE_MONTH],
                ['date', '>=', $date],
                ['date', '<=', $date]
            ])
            ->orderBy('id', 'DESC')
            ->first();
    }

}
