<?php

namespace App\Repositories;

use App\Models\Sign;
use App\Models\SignCategory;

/**
 * Class SignRepository
 */
class SignRepository
{
    public function getSignRootList()
    {
        return SignCategory::get();
    }

    public function getSignsByCategory($category_id)
    {
        return Sign::where('category_id', $category_id)->get();
    }

    public function getSignById($id)
    {
        return Sign::where('id', $id)->first();
    }

    public function getSignsCategoryList()
    {
        return SignCategory::pluck('title', 'id');
    }

    /**
     * Проверяет наличие правила
     *
     * @param int $id Идентификатор правила
     *
     * @return Sign
     */
    public function checkSignExists($id)
    {
        return Sign::where('id', $id)->first();
    }

    /**
     * Сохраняет данные
     *
     * @param array $data Данные
     *
     * @return Sign
     */
    public function saveSign($data)
    {
        $sign = new Sign();
        $sign->text = $data['text'];
        $sign->title = $data['title'];
        $sign->image = $data['image'];
        $sign->category_id = $data['category_id'];
        $sign->n = $data['n'];
        $sign->save();

        return $sign;
    }

    /**
     * Сохраняет данные
     *
     * @param Sign  $sign Обновляемый знак
     * @param array $data Данные для обновления
     *
     * @return Sign
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function updateSign(Sign $sign, array $data)
    {
        $sign->n = $data['n'];
        $sign->text = $data['text'];
        $sign->title = $data['title'];
        if (array_key_exists('image', $data)) {
            $sign->image = $data['image'];
        }

        $sign->category_id = $data['category_id'];
        $sign->save();

        return $sign;
    }

    /**
     * Возвращает знак по запросу
     *
     * @param string $query Запрос
     *
     * @return Sign
     */
    public function getSignByQuery($query)
    {
        return Sign::where('text', 'LIKE', '%' . trim($query) . '%')->get();
    }

}
