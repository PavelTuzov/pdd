<?php

namespace App\Repositories;

use App\Models\City;

/**
 * Репозиторий городов
 */
class CityRepository implements IRepository
{
    /**
     * Возвращает все города
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllCities()
    {
        return City::all();
    }

    /**
     * Возвращает город по идентификатору
     *
     * @param int $cityId Идентификатор города
     *
     * @return City
     */
    public function getCityById($cityId)
    {
        return City::where('id', $cityId)->first();
    }
}