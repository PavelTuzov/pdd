<?php

namespace App\Repositories;

use App\Models\Reference;

/**
 * Class ReferenceRepository
 */
class ReferenceRepository
{
    /**
     * Возвращает правило по идентификатору
     *
     * @param int $id Идентификатор пункта правила
     *
     * @return Reference
     */
    public function getReferenceById($id)
    {
        return Reference::where('id', $id)->with('parent')->first();
    }

    /**
     * Возвращает коллекцию правил
     *
     * @return Reference[]
     */
    public function getRootReferences()
    {
        return Reference::where('parent_id', 0)->orderBy('id', 'ASC')->get();
    }

    /**
     * Возвращает дерево правил
     *
     * @param int $id Идентификатор корневого пункта правил
     *
     * @return Reference[]
     */
    public function getReferenceTree($id)
    {
        return Reference::where('parent_id', $id)->with('tree')->orderBy('n', 'ASC')->orderBy('id', 'ASC')->get();
    }

    /**
     * Возвращает список правил
     *
     * @param string $query Запрос
     *
     * @return \App\Models\Reference[]
     */
    public function getReferenceList($query)
    {
        return Reference::orderBy('n', 'ASC')
            ->where('text', 'LIKE', '%' . $query . '%')
            ->orWhere('id', (int)$query)
            ->orWhere('n', $query)
            ->orderBy('id', 'ASC')
            ->get(['id', 'text', 'parent_id', 'n'])->toArray();
    }

    /**
     * Сохраняет данные
     *
     * @param array $data Данные
     *
     * @return Reference
     */
    public function saveReference($data)
    {
        $reference = new Reference();
        $reference->text = $data['text'];
        $reference->parent_id = $data['parent_id'];
        $reference->n = $data['n'];
        $reference->save();

        return $reference;
    }

    /**
     * Сохраняет данные
     *
     * @param Reference $reference Обновляемое правило
     * @param array     $data      Данные для обновления
     *
     * @return Reference
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function updateReference(Reference $reference, array $data)
    {
        $reference->n = $data['n'];
        $reference->text = $data['text'];
        $reference->parent_id = $data['parent_id'];
        $reference->save();

        return $reference;
    }

    /**
     * Проверяет наличие правила
     *
     * @param int $id Идентификатор правила
     *
     * @return Reference
     */
    public function checkReferenceExists($id)
    {
        return Reference::where('id', $id)->first();
    }

    /**
     * Возвращает правила по номеру пункта
     *
     * @param string $n Номер пункта
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getReferenceByN($n)
    {
        return Reference::where('n', $n)->orderBy('id', 'ASC')->first();
    }

    /**
     * Возвращает правила по родительскому идентификатору
     *
     * @param int $id Идентификатор родителя пункта правил
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getReferenceByParentId($id)
    {
        return Reference::where('parent_id', $id)->orderBy('id', 'ASC')->get();
    }
}
