<?php

namespace App\Repositories;

use App\Helpers\BotHelper;
use App\Models\Telegram\ChatAnswer;
use App\Models\Telegram\ChatMessage;
use App\Models\Telegram\ChatSession;
use App\Models\Telegram\ChatSubscriber;
use Barryvdh\Reflection\DocBlock\Type\Collection;
use Log;

/**
 * Class TelegramRepository
 */
class BotRepository
{

    private $isGroup = false;

    public function saveChatMessage($result)
    {
        $key = BotHelper::$actualKeyMessage;
        $message = new ChatMessage();

        if ($this->isGroup) {
            $message->chat_id = $result[$key]['chat']['id'];
            $message->title = $result[$key]['chat']['title'];
        } else {
            $message->chat_id = $result[$key]['from']['id'];
            $message->title = null;
        }

        $message->first_name = $result[$key]['from']['first_name'];
        $message->last_name = $result[$key]['from']['last_name'];
        $message->username = $result[$key]['from']['username'];
        $message->type = $result[$key]['chat']['type'];
        $message->text = $result[$key]['text'];
        $message->date = date("Y-m-d H:i:s", $result[$key]['date']);
        $message->message_id = $result[$key]['message_id'];
        $message->update_id	= $result['update_id'];

        $is_exist = $this->getChatMessageByUpdateId($message->update_id);

        if ($is_exist === null) {
            return $message->save();
        }

        return false;
    }

    /**
     * Проверяет наличие сообщения по идентификатору обновления
     *
     * @param int $update_id
     *
     * @return bool
     */
    private function getChatMessageByUpdateId($update_id)
    {
        return ChatMessage::select('id')->where('update_id', $update_id)->first();
    }

    /**
     * Сохраняет данные о подпичисчике
     *
     * @param array $result Массив данных
     *
     * @return bool
     */
    public function saveChatSubscriber($result)
    {
        $subscriber = new ChatSubscriber();

        if (isset($result['message']['chat']) === false) {
            return false;
        }

        $subscriber->chat_id = $result['message']['from']['id'];
        $subscriber->first_name = $result['message']['from']['first_name'];
        $subscriber->last_name = $result['message']['from']['last_name'];
        $subscriber->username = $result['message']['from']['username'];

        $is_exist = $this->getSubscriberByChatId($subscriber->chat_id);

        if ($is_exist === null) {
            return $subscriber->save();
        }

        return false;
    }

    /**
     * Возвращает подписчика по инентифактору чата
     *
     * @param int $chat_id Идентификатор чата
     *
     * @return ChatSubscriber
     */
    public function getSubscriberByChatId($chat_id)
    {
        return ChatSubscriber::select('id')->where('chat_id', $chat_id)->first();
    }

    /**
     * Сохраняет ответ сервиса пользователю
     *
     * @param array $data Массив данных
     *
     * @return bool
     */
    public function saveChatAnswer($data)
    {
        $answer = new ChatAnswer();
        $answer->fill($data);

        return $answer->save();
    }

    /**
     * Возвращает для бота вопрос, на который подписчик отвечает
     *
     * @param array $result Массив данных
     *
     * @return mixed
     */
    public function getLastQuestion($result)
    {
        $key = BotHelper::$actualKeyMessage;

        return ChatAnswer::where('chat_id', $result[$key]['from']['id'])
            ->where('date', '>', date('Y-m-d H:i:s', $result[$key]['date'] - BotHelper::TIME_TOANSWER))
            ->where('answered', false)
            ->orderBy('id', 'DESC')
            ->first();
    }

    /**
     * Устанавливает флаг ответа на вопрос подписчику
     *
     * @param ChatAnswer $question Вопрос
     *
     * @return bool
     */
    public function checkQuestionAnswered($question)
    {
        $question->answered = true;

        return $question->save();
    }

    /**
     * Сохраняет сессию
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public function putSession($key, $value)
    {
        $session = new ChatSession();
        $session->key = $key;
        $session->value = serialize($value);
        $session->date = date('Y-m-d H:i:s');

        return $session->save();
    }

    /**
     * Возвращает сессию
     *
     * @param $key
     * @return mixed
     */
    public function getSession($key)
    {
        $session = ChatSession::where('key', $key)->first();
        if ($session === null) {
            return false;
        }

        $session->value = unserialize($session->value);

        return $session;
    }

    /**
     * Обновляет сессию
     *
     * @param ChatSession $session Сессия
     * @param mixed       $value   Значение
     *
     * @return bool
     */
    public function updateSession(ChatSession $session, $value)
    {
        return $session->where('key', $session->key)->update(['value' => serialize($value)]);
    }

    public function removeSession($key)
    {
        return ChatSession::where('key', $key)->delete();
    }

    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;
    }
}
