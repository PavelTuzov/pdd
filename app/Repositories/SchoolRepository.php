<?php

namespace App\Repositories;

use App\Models\School;

/**
 * Class SchoolRepository
 */
class SchoolRepository
{
    /**
     * Возвращает все школы
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllSchools()
    {
        return School::all();
    }

    /**
     * Возвращает школы по идентификатору города
     *
     * @param int $cityId Идентификатор города
     *
     * @return School[]
     */
    public function getSchoolsByCity($cityId)
    {
        return School::where('city_id', $cityId)->get();
    }
}