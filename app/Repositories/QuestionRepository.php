<?php

namespace App\Repositories;

use App\Models\Answer;
use App\Models\Question;

/**
 * Class QuestionRepository
 */
class QuestionRepository implements IRepository
{
    const TOP_100_HARD = [
        1984, 2242, 1922, 1972, 2456, 2681, 2576, 2027, 2422, 1974, 2200, 1975, 2374, 2417, 2140, 2485, 2407, 2497, 2227, 2470, 2029, 2439, 2219, 1976, 2156, 2664, 1888, 2251, 2015, 2683, 2004, 1887, 2010, 2041, 1983, 2238, 2099, 2496, 2032, 2061, 2098, 2429, 2152, 1904, 1959, 2086, 2316, 2128, 2273, 2348, 2341, 1939, 2389, 2241, 1912, 2556, 2420, 2597, 2654, 2561, 2096, 2533, 2237, 1968, 2070, 2501, 2674, 2589, 1997, 2082, 1942, 2072, 2101, 1962, 2460, 2276, 2141, 2543, 2202, 2634, 2588, 1916, 1910, 2267, 2261, 2119, 2637, 2033, 2557, 2103, 2048, 2474, 2602, 2034, 2329, 2050, 2108, 2181, 2462, 1936
    ];

    /**
     * Возвращает вопрос по идентификатору
     *
     * @param int $id Идентификатор
     *
     * @return Question
     */
    public function getQuestionById($id)
    {
        return Question::where('id', $id)->first();
    }

    /**
     * Возвращает коллекцию вопросов по идентификатору темы
     *
     * @param int $id Идентификатор темы
     *
     * @return Question[]
     */
    public function getQuestionsBySubject($id)
    {
        return Question::where('subject_id', $id)->get();
    }

    /**
     * Возвращает коллекцию вопровос по идентификатору билета
     *
     * @param int $id Идентификатор Билета
     *
     * @return Question[]
     */
    public function getQuestionsByTicket($id)
    {
        return Question::where('ticket_id', $id)->orderBy('n', 'ASC')->with('answers')->get();
    }

    /**
     * Возвращает коллекцию вопросов по дате актульности
     *
     * @param string $date Дата
     *
     * @return mixed
     */
    public function getQuestionByDate($date)
    {
        return Question::where('date_from', '>', $date)->get();
    }

    /**
     * Возвращает рандомные вопросы для экзамена
     *
     * @param int $count Количество
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getQuestionsForExamByBlock($block, $ticket, $count)
    {
        return Question::limit($count)
            ->where('block_id', $block)
            ->where('ticket_id', $ticket)
            ->groupBy('id')
            ->with('answers')
            ->orderBy('n', 'ASC')
            ->get();
    }

    /**
     * Возвращает вопросы по массиву идентификаторов
     *
     * @param array $ids Массив идентификаторов
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getQuestionsByIds($ids)
    {
        return Question::groupBy('id')->whereIn('id', $ids)->with('answers')->get();
    }


    /**
     * Возвращает топ 100 сложных вопросов
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getTop100Hardest()
    {
        return $this->getQuestionsByIds(self::TOP_100_HARD);
    }

    /**
     * Возвращает
     *
     * @param array $except   Исключая эти идентификаторы
     * @param array $question Массив данных вопроса
     * @param int   $count    Количество
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAdditionalQuestionsForExam($except, $question, $count = 5)
    {
        return Question::limit($count)
            ->whereNotIn('id', $except)
            ->where('ticket_id', '!=', $question['ticket'])
            ->where('block_id', $question['block'])
            ->where('n', $question['n'])
            ->inRandomOrder()
            ->with('answers')
            ->get();
    }

    /**
     * Возвращает коллекцию билетов
     *
     * @return \Illuminate\Support\Collection
     */
    public function getTickets()
    {
        return Question::selectRaw('distinct on (ticket_id) ticket_id, image')
            ->whereNotNull('image')
            ->where('image', '!=', '')
            ->orderBy('ticket_id', 'ASC')
            ->get()->keyBy('ticket_id');
    }

    /**
     * Возвращает вопрос по номеру
     *
     * @param int $ticket Номер билета
     * @param int $n      Номер вопроса
     *
     * @return mixed
     */
    public function getQuestionByNumber($ticket, $n)
    {
        $question = Question::where('n', $n)->where('ticket_id', $ticket)->first();

        if ($question === null) {
            return false;
        }

        $answers = Answer::where('question_id', $question->id)->orderBy('n', 'ASC')->get();

        return [
            'question' => $question,
            'answers' => $answers
        ];
    }
}
