<?php

namespace App\Repositories;

use App\Models\Answer;
use Illuminate\Support\Collection;

/**
 * Class AnswerRepository
 */
class AnswerRepository
{
    /**
     * Получает ответ по идентификатору
     *
     * @param int $id Идентификатор
     *
     * @return Answer;
     */
    public function getAnswerById($id)
    {
        return Answer::where('id', $id)->first();
    }

    /**
     * Проверяет ответ на правильность
     *
     * @param int    $id   Идентфикатор ответа
     * @param string $with Завивисимости
     *
     * @return array
     */
    public function checkAnswer($id, $with)
    {
        return Answer::where('id', $id)->with($with)->first();
    }

    /**
     * Получает все ответы, привязанные к вопросу
     *
     * @param int $id Идентфикатор вопроса
     *
     * @return Collection|Answer[]
     */
    public function getAnswersByQuestion($id)
    {
        return Answer::where('question_id', $id)->get();
    }


    /**
     * Возвращает ответы по массиву идентификаторов
     *
     * @param array $ids Массив идентификаторов
     *
     * @return Collection
     */
    public function getAnswersByIds($ids)
    {
        return Answer::whereIn('id', $ids)
            ->with('question')
            ->get()
            ->keyBy('question_id');
    }
}
