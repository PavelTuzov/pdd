<?php

namespace App\Repositories;

use App\Models\Settings;

/**
 * Class SettingsRepository
 */
class SettingsRepository
{
    /**
     * Возвращает все школы
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllSettings()
    {
        return Settings::all(['value', 'key'])->keyBy('key')->toArray();
    }
}
