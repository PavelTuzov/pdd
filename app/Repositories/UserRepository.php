<?php

namespace App\Repositories;

use App\Models\School;
use App\Models\User;
use Barryvdh\Reflection\DocBlock\Type\Collection;

/**
 * Class UserRepository
 */
class UserRepository
{
    /**
     * Возвращает данные пользователя по идентификатору
     *
     * @param int $id Идентификатор статистики
     *
     * @return User|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getById($id)
    {
        return User::where('id', $id)->first();
    }

    /**
     * Устанавливает Пользователю школу
     *
     * @param User   $user   Экземпляр пользователя
     * @param School $school Экземпляр школы
     *
     * @return bool
     */
    public function setSchoolToUser($user, $school)
    {
        $user->school_id = $school->id;

        return $user->save();
    }

    /**
     * Удаляет школу у пользователя
     *
     * @param User $user Экземпляр пользователя
     *
     * @return mixed
     */
    public function unsetSchoolToUser($user)
    {
        $user->school_id = null;

        return $user->save();
    }

    /**
     * Возвращает данные пользователей по массиву идентификаторов
     *
     * @param array $ids Идентификаторы пользователей
     *
     * @return Collection
     */
    public function getByIds($ids)
    {
        return User::whereIn('id', $ids)->get();
    }
}
