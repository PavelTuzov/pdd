<?php

namespace App\Validators;


use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

/**
 * Class AnswerValidator
 */
class AnswerValidator
{

    /**
     * AnswerValidator constructor.
     */
    public function __construct()
    {
    }

    /**
     * Валидирует данные
     *
     * @param Request $request Запрос
     *
     * @return void
     */
    public function validate($request)
    {

    }
}
