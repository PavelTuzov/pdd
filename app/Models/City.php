<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reward
 */
class City extends Model
{
    public $fillable = ['id', 'text'];
    public $timestamps = false;
}
