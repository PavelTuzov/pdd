<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StatAnswer
 */
class StatSnapshot extends Model
{

    public $table = 'stats_snapshot';

    public $fillable = ['id', 'snapshot', 'from', 'to'];
    public $timestamps = false;

}
