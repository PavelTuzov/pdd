<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Questions
 *
 * @property integer $id
 * @property string $question
 * @property integer $subject_id
 * @property integer $ticket_id
 * @property integer $category_id
 * @property string $image
 * @property integer $n
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Answer[] $answers
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereSubjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereTicketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereN($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereCreatedAt($value)
 * @mixin \Eloquent
 */
class Question extends Model
{
    protected $fillable = ['id', 'question', 'subject_id', 'ticket_id', 'category_id', 'image', 'n'];

    private $stat;
    /**
     * Возвращает ответы вопроса
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id')->orderBy('n', 'ASC');
    }

    /**
     * Возвращает изображение первого ответа
     *
     * @return $this
     */
    public function getImage()
    {
        return $this->hasOne(Question::class, 'id', 'id')->whereNot('image', null);
    }

    /**
     * Сеттер аттрибута статистики
     *
     * @param Stat $stat Объект статистики
     *
     * @return void
     */
    public function setStatAttribute($stat)
    {
        $this->attributes['stat'] = $stat;
    }
}
