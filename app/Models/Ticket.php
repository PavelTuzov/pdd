<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Questions
 *
 * @property integer $id
 * @property string $title
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereTitle($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Question $questions
 */
class Ticket extends Model
{
    /**
     * Возвращает билеты
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->belongsTo(Question::class, 'ticket_id', 'id');
    }
}
