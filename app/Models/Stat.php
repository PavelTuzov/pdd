<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Questions
 *
 * @property integer $id
 * @property integer $user_id
 * @property \App\Models\Ticket $ticket
 * @property integer $mode
 * @property integer $errors
 * @property integer $answers
 * @property string $time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereTicket($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereMode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereErrors($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereAnswers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stat whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Stat extends Model
{

    /**
     * Количество вопросов в билете
     *
     * @var int
     */
    const QUESTIONS_IN_TICKET = 20;

    const QUESTION_LEVEL = [
        'EMPTY_LEVEL' => 'empty',
        'LOW_LEVEL' => 'low',
        'MEDIUM_LEVEL' => 'medium',
        'HIGH_LEVEL' => 'high',
        'FULL_LEVEL' => 'full'
    ];
    /**
     * Возвращает билет
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }

    /**
     * Возвращает аттрибут отношения ошибок к количеству вопросов
     *
     * @return float
     */
    public function getRatioAttribute()
    {
        if ($this->errors !== null) {
            return (self::QUESTIONS_IN_TICKET / ($this->errors + 1));
        }

        return null;
    }

    /**
     * Возвращает атрибут процентного соотношения неправильных ответов к кроличеству ответов
     *
     * @return float
     */
    public function getWrongPercentAttribute()
    {
        if ($this->errors !== null) {
            return (($this->errors / self::QUESTIONS_IN_TICKET) * 100);
        }

        return null;
    }

    /**
     * Возвращает атрибут процентного соотношения правильных ответов к кроличеству ответов
     *
     * @return float
     */
    public function getRightPercentAttribute()
    {
        if ($this->errors !== null) {
            return (((self::QUESTIONS_IN_TICKET - $this->errors) / self::QUESTIONS_IN_TICKET) * 100);
        }

        return null;
    }

    /**
     * Возвращает атрибут количества правильных ответов
     *
     * @return int
     */
    public function getRightAttribute()
    {
        if ($this->errors !== null) {
            return (self::QUESTIONS_IN_TICKET - $this->errors);
        }

        return null;
    }

    /**
     * Возвращает аттрибут уровня знаний
     *
     * @return mixed
     */
    public function getLevelAttribute()
    {
        if ($this->errors !== null) {
            if ($this->errors === 0) {
                return self::QUESTION_LEVEL['FULL_LEVEL'];
            }

            if ($this->errors === 20) {
                return self::QUESTION_LEVEL['EMPTY_LEVEL'];
            }

            if ($this->errors < (self::QUESTIONS_IN_TICKET / 2)) {
                return self::QUESTION_LEVEL['HIGH_LEVEL'];
            }

            if ($this->errors > (self::QUESTIONS_IN_TICKET / 2)) {
                return self::QUESTION_LEVEL['LOW_LEVEL'];
            }

            if ($this->errors === (self::QUESTIONS_IN_TICKET / 2)) {
                return self::QUESTION_LEVEL['MEDIUM_LEVEL'];
            }
        }

        return null;
    }
}
