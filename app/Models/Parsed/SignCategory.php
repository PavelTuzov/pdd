<?php

namespace App\Models\Parsed;

use App\Handlers\ImageHandler;

class SignCategory {

    /**
     * Категория
     *
     * @var string
     */
    private $title;

    /**
     * Изображение
     *
     * @var string
     */
    private $image;
    
    /**
     * Номер знака
     *
     * @var int
     */
    private $n;

    /**
     * Знаки в категории
     *
     * @var Sign[]
     */
    private $signs;

    /**
     * SignCategory constructor.
     *
     * @param array $data Данные
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function __construct($data)
    {
        $image = null;
        if (strlen($data['image']) > 0) {
            $image = ImageHandler::download($data['image'], 'signs', ['xs', 'm', 's']);
        }

        $this->image = $image;
        $this->title = $data['title'];
        $this->n = $data['n'];
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Sign[]
     */
    public function getSigns()
    {
        return $this->signs;
    }

    /**
     * @param Sign[] $signs
     */
    public function setSigns($signs)
    {
        $this->signs = $signs;
    }
}
