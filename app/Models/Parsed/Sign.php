<?php

namespace App\Models\Parsed;

use App\Handlers\ImageHandler;

class Sign {

    /**
     * Категория
     *
     * @var string
     */
    private $title;

    /**
     * Изображение
     *
     * @var array
     */
    private $image;

    /**
     * Номер знака
     *
     * @var int
     */
    private $n;

    /**
     * Описание знака
     *
     * @var string
     */
    private $text;

    /**
     * Sign constructor.
     *
     * @param array $data Данные
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function __construct($data)
    {
        $image = null;
        if (count($data['image']) > 0) {
            foreach ($data['image'] as $image_item) {
                $image[] = ImageHandler::download($image_item, 'signs',  ['xs', 'm', 's']);
            }
        }

        $this->image = $image;
        $this->title = $data['title'];
        $this->text = $data['text'];
        $this->n = $data['n'];
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
