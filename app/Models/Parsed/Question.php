<?php

namespace App\Models\Parsed;

use App\Handlers\ImageHandler;
use App\Helpers\UtilsHelper;

class Question {

    /**
     * Вопрос
     *
     * @var string
     */
    private $question;

    /**
     * Изображение
     *
     * @var string
     */
    private $image;

    /**
     * Ответы
     *
     * @var array
     */
    private $answers;

    /**
     * Номер вопроса
     *
     * @var int
     */
    private $n;

    /**
     * Идентификатор билета
     *
     * @var int
     */
    private $ticket;

    /**
     * Идентификатор типа
     *
     * @var int
     */
    private $type;

    /**
     * Идентификатор категории
     *
     * @var int
     */
    private $category;

    /**
     * Текст подсказки
     *
     * @var string
     */
    private $hint;

    /**
     * Question constructor.
     *
     * @param array $data Данные
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    public function __construct($data)
    {
        $data = $data['data'];
        $image = $data['question']['image'];
        if (strlen($image) > 0) {
            $image = ImageHandler::download($data['question']['image'], 'questions');
        }
        $this->question = $data['question']['text'];
        $this->image = $image;
        $this->answers = $data['question']['answers'];
        $this->n = $data['question']['orderNumber'];
        $this->hint = $data['question']['hint'];
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return array
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @return int
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * @return int
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @param int $ticket
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getHint()
    {
        return $this->hint;
    }
}
