<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Reward
 */
class School extends Model
{
    public $fillable = ['id', 'city_id', 'text', 'image', 'meta_description', 'meta_keywords'];
    public $timestamps = false;

    /**
     * Возвращает город автошколы
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Возвращает группы, который фигурируют в этой автошколе
     *
     * @return HasMany
     */
    public function classRooms()
    {
        $this->hasMany(Classroom::class);
    }
}
