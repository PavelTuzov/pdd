<?php

namespace App\Models\Entity;

/**
 * Class MenuItem
 */
class MenuItem
{
    /**
     * Заголовок
     *
     * @var string
     */
    private $title;

    /**
     * Название ссылки
     *
     * @var string
     */
    private $name;

    /**
     * Ссылка
     *
     * @var string
     */
    private $url;

    /**
     * Флаг активности
     *
     * @var bool
     */
    private $active;

    /**
     * Иконка
     *
     * @var string
     */
    private $icon;

    /**
     * Геттер заголовка
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Сеттер заголовка
     *
     * @param string $title Заголовок
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Геттер ссылки
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Геттер геттер названия ссылки
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Сеттер ссылки
     *
     * @param string $url Ссылка
     *
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Геттер флага активности
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Сеттер флага активности
     *
     * @param boolean $active Флаг активности
     *
     * @return void
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * MenuItem constructor.
     *
     * @param array $data Данные для заполнения
     */
    public function __construct($data)
    {
        $this->url = array_key_exists('url', $data) ? getenv('APP_URL') . $data['url'] : null;
        $this->title = array_key_exists('title', $data) ? $data['title'] : null;
        $this->name = array_key_exists('name', $data) ? $data['name'] : null;
        $this->active = array_key_exists('active', $data) ? $data['active'] : false;
        $this->icon = array_key_exists('icon', $data) ? $data['icon'] : false;
    }

    /**
     * Возвращает иконку меню
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
