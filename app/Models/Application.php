<?php

namespace App\Models;

use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Application
 */
class Application extends Model
{
    /**
     * Генерирует токен
     *
     * @return mixed
     */
    public function generateAuthToken()
    {
        $jwt = JWT::encode([
            'iss' => getenv('API_NAME'),
            'sub' => $this->key,
            'iat' => time(),
            'exp' => (time() + (getenv('API_TOKEN_LIFETIME') * 60 * 60)),
        ], getenv('APP_KEY'));

        return $jwt;
    }
}
