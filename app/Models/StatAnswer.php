<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StatAnswer
 */
class StatAnswer extends Model
{

    public $table = 'stats_answer';

    public $fillable = ['id', 'answer_id', 'ticket_id', 'user_id', 'is_right', 'date'];
    public $timestamps = false;

    /**
     * Возвращает билет
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }

    /**
     * Возвращает пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
