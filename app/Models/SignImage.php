<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SignCategory
 */
class SignImage extends Model
{
    public $fillable = ['id', 'sign_id', 'title', 'url', 'n'];
    public $timestamps = false;
    public $table = 'sign_image';
}

