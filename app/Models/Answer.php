<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Answer
 *
 * @property integer $id
 * @property string $answer
 * @property integer $question_id
 * @property boolean $right
 * @property integer $n
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereRight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereN($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Answer $withAnswer
 */
class Answer extends Model
{
    public $fillable = ['id', 'answer', 'question_id', 'n', 'right'];
    public $timestamps = false;

    /**
     * Возвращает вопрос ответа
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id', 'id');
    }

    /**
     * Возвращает ответ
     *
     * @return mixed
     */
    public function withAnswer()
    {
        return $this->hasOne(Answer::class, 'question_id', 'question_id')->where('right', true);
    }

}
