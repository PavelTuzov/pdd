<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatSubscriber
 */
class ChatSubscriber extends Model
{
    public $fillable = ['id', 'chat_id','first_name', 'last_name','username'];
    public $timestamps = false;

    public $table = 'chat_subscribers';
}
