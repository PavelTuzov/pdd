<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatAnswer
 */
class ChatSession extends Model
{
    public $fillable = ['key', 'value', 'date'];
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;

    public $table = 'chat_sessions';
}
