<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatAnswer
 */
class ChatAnswer extends Model
{
    /**
     * Тип вопроса
     */
    const TYPE_QUESTION = 1;

    public $fillable = ['id', 'chat_id','message_id', 'date','text', 'question_id', 'answered'];
    public $timestamps = false;

    public $table = 'chat_answers';
}
