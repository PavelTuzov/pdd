<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatMessage
 */
class ChatMessage extends Model
{
    public $fillable = ['id', 'chat_id','first_name', 'last_name','username','date','type', 'text'];
    public $timestamps = false;

    public $table = 'chat_messages';
}
