<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Subject
 *
 */
class Subject extends Model
{

    /**
     * Возвращает билет
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->belongsTo(Ticket::class, 'subject_id', 'id');
    }
}
