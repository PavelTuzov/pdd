<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Reward
 */
class Classroom extends Model
{
    public $fillable = ['id', 'school_id', 'title'];
    public $timestamps = false;

    /**
     * Возвращает автошколу
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Возвращает пользователей, которые в этой школе обучаются
     *
     * @return HasMany
     */
    public function students()
    {
        $this->hasMany(User::class);
    }
}
