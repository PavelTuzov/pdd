<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Answer
 *
 * @property int $id
 * @property int $parent_id
 * @property string $text
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reference whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reference whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reference whereText($value)
 * @mixin \Eloquent
 */
class Reference extends Model
{
    public $fillable = ['id', 'parent_id', 'text'];
    public $timestamps = false;

    /**
     * Возвращает дерево
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tree()
    {
        return $this->hasMany(Reference::class, 'parent_id', 'id')->orderby('n', 'ASC')->orderby('id', 'DESC');
    }

    /**
     * Возвращает родителя записи
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Reference::class, 'parent_id');
    }

    /**
     * Возвращает ответы
     *
     * @return BelongsToMany
     */
    public function answers()
    {
        return $this->belongsToMany(Answer::class, 'answer_reference', 'reference_id', 'answer_id');
    }
}
