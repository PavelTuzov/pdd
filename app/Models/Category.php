<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Categories
 *
 */
class Category extends Model
{

    /**
     * Возвращает билеты
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {   
        return $this->belongsTo(Ticket::class, 'category_id', 'id');
    }
}
