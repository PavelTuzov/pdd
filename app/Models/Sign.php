<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sign
 */
class Sign extends Model
{
    public $fillable = ['id', 'category_id', 'text', 'image', 'n'];
    public $timestamps = false;
}
