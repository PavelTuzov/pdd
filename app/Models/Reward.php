<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reward
 */
class Reference extends Model
{
    public $fillable = ['id', 'text', 'image'];
    public $timestamps = false;
}
