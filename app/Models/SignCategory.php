<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SignCategory
 */
class SignCategory extends Model
{
    public $fillable = ['id', 'title', 'text', 'image', 'n'];
    public $timestamps = false;
    public $table = 'sign_category';
}

