<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Settings
 */
class Settings extends Model
{
    public $fillable = ['id', 'name', 'key', 'value'];
    public $timestamps = false;

}
