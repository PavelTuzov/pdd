<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ParserHelper;

/**
 * Class getRates
 */
class ParseTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсит билеты';

    /**
     * Execute the console command.
     *
     * @param ParserHelper $parser Валюта
     *
     * @return mixed
     * @throws \RuntimeException
     */
    public function handle(ParserHelper $parser)
    {
        $parser->setVerbosity($this->verbosity);
        $data = $parser->getTickets();
        if ($data !== null) {
            $this->info('Данные получены');
        }

        if ($data === null) {
            $this->info('Данные не получены. Произошла ошибка');
        }
    }
}
