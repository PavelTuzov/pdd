<?php

namespace App\Console\Commands;

use App\Helpers\StatHelper;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class getRates
 */
class StatCalculate extends Command
{
    const RIGHT_ANSWERS_TEMPLATE = 'верных ответов: %s';
    const WRONG_ANSWERS_TEMPLATE = 'неверных ответов: %s';
    const SUCCESS_TEMPLATE = 'Данные успешно сохранены за период %s - %s';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stat:calc {--O|offset=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Считает статистику';

    /**
     * Единица времени
     *
     * @var string
     */
    protected $offsetUnit;

    /**
     * Список единиц смещения
     */
    const UNITS_OFFSET = [
        'min' => 'subMinutes',
        'sec' => 'subSecond',
        'm' => 'subMonth',
        'd' => 'subDays',
    ];

    /**
     * Смещение по-умолчанию
     */
    const DEFAULT_OFFSET = '1m';

    /**
     * Количество единиц смещения
     *
     * @var int
     */
    protected $offset;

    /**
     * Execute the console command.
     *
     * @param StatHelper $statHelper Обработка статистики
     *
     * @return mixed
     * @throws \RuntimeException
     */
    public function handle(StatHelper $statHelper)
    {
        $this->getAttr();

        $params = [
            'to' => Carbon::now(),
            'from' => Carbon::now()->{$this->offsetUnit}($this->offset),
            'offset' => (int)$this->offset,
            'offsetUnit' => $this->offsetUnit,
        ];
        $data = $statHelper->accumulateData($params);

        if ($data['saved'] !== false) {
            $this->info(sprintf(self::SUCCESS_TEMPLATE, $params['from'], $params['to']));
            $this->info(sprintf(self::RIGHT_ANSWERS_TEMPLATE, $data['count_answers']['right']));
            $this->info(sprintf(self::WRONG_ANSWERS_TEMPLATE, $data['count_answers']['wrong']));
        }
    }

    /**
     * Получает параметры
     *
     * @return void
     */
    private function getAttr()
    {
        $offset = ($this->option('offset') !== null) ? $this->option('offset') : self::DEFAULT_OFFSET;
        $this->offset = preg_replace('/[^0-9.]+/', '', $offset);

        foreach (self::UNITS_OFFSET as $k => $v) {
            if (strrpos($offset, $k) !== false) {
                $this->offsetUnit = $v;
                break;
            }
        }
    }
}
