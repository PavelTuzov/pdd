var Router = function () {
    this.routes = [];
    this.guard = $.Deferred();
};

$.extend(Router.prototype, {
    connect: function (path, action) {
        this.routes.push([path, action]);
    },
    dispatch: function (location) {
        this.guard.resolve(location);
        this.guard = $.Deferred();
        var path = location.pathname;
        for (var i = 0, route; route = this.routes[i]; i++) {
            var match = path.match(route[0]);
            if (!match) continue;
            route[1].call(this, match, location, this.guard.promise());
            return false;
        }
    }
});

window.Router = Router;