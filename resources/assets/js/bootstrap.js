try {
    window.$ = window.jQuery = require('jquery');
    require('../semantic/dist/semantic.js');
    require('./router');
    require('./config');
    window.NProgress = require('nprogress');
    window.Alertify = require('alertifyjs');
    window.Hammer = require('hammerjs');
} catch (e) {}