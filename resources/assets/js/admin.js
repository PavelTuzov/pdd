Admin = {
    init: function() {
        var those = this;

        $('.main.menu').visibility({
            type: 'fixed'
        });
        $('.overlay').visibility({
            type: 'fixed',
            offset: 80
        });

        // lazy load images
        $('.image').visibility({
            type: 'image',
            transition: 'vertical flip in',
            duration: 500
        });

        // show dropdown on hover
        $('.main.menu  .ui.dropdown').dropdown({
            on: 'hover'
        });

        $('.ui.buttons .dropdown.button').dropdown({
            action: 'combo'
        });
        $('.ui.dropdown').dropdown({
            on: 'click'
        });


        if ($('.admin__question_answers').length) {
            those.Question.init();
        }

        if ($('#reference_item_form').length) {
            those.Reference.init();
        }

        var sortable = $('.admin__question_answers').sortable({
            containerSelector: '.admin__question_answers',
            itemPath: '',
            itemSelector: '.answer__item',
            placeholder: '<div class="answer__item-placeholder"/>',
            group: 'serialization',
            onDrop: function  ($item, container, _super) {
                var data = sortable.sortable("serialize").get();
                var order = JSON.stringify(data, null, ' ');

                $.ajax({
                    type: "POST",
                    url: '/admin/changeAnswerOrder',
                    dataType: 'json',
                    data:{
                        order:order
                    },
                    success: function(response) {

                    }
                });

                _super($item, container);
            },
            serialize: function (parent, children, isContainer) {
                return isContainer ? children.join() : parent.data('id');
            }
        });
        tinymce.init({
            theme_url: '/js/components/tinymce/themes/modern/theme.js',
            skin_url: '/js/components/tinymce/skins/lightgray/',
            selector: 'textarea',
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            plugins: [
                'sign',
                'rule',
                'noneditable',
                'code',
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image lists | sign rule",
            external_plugins: {
                'sign': '/js/plugins/sign.js',
                'rule': '/js/plugins/rule.js',
                'noneditable' : '/js/components/tinymce/plugins/noneditable/plugin.js',
                'code' : '/js/components/tinymce/plugins/code/plugin.js'
            },
            content_css: '/css/content.css',
            noneditable_noneditable_class: "placeholder-block",
            setup: function(ed) {

            },
        });
    },
    'Question': {
        answerCount: 0,
        init: function(){
            var those = this;
            this.checkAnswers();
            this.selectAnswer();
            $('#addAnswer').on('click', function() {
                those.addAnswer();
            });
            this.searchReference();

        },
        checkAnswers: function() {
            this.answersCount = $('.answer__item').length;
            var those = this;
            if (this.answersCount >= 5) {
                $('#addAnswer').hide();
            } else {
                $('#addAnswer').show();
            }

            $('.deleteAnswer').off('click').on('click', function () {
                var answer = $(this).parent().parent().remove();
                those.checkAnswers();
            })
        },
        addAnswer: function() {
            var those = this;
            $('.admin__question_answers').append('<label data-id="" class=" fields answer__item " for="answer_' + those.answersCount + '"><div class="ui container" style="position:relative"><i class="icon close deleteAnswer" style="position:absolute; top:-3px;right: 0px;"></i><input id="answer_' + those.answersCount + '" style="opacity:0; height:0px; margin:0px;padding:0px;" name="answers[' + those.answersCount + '][right]" type="checkbox" value="right"><textarea class="ui" name="answers[' + those.answersCount + '][answer]" cols="50" rows="10"></textarea></div></label>');
            this.selectAnswer();
            this.checkAnswers();
        },
        selectAnswer: function() {
            $('.answer__item').off('click').on('click', function() {
                $('.answer__item')
                    .removeClass('answer__item-active')
                    .find('input').prop('checked', false);
                $(this)
                    .addClass('answer__item-active')
                    .find('input').prop('checked', true);
            });
        },
        searchReference: function() {
            $('.search.ui')
                .search({
                        apiSettings: {
                            url: '/admin/get_reference?query={query}',
                        },
                        fields: {
                            results : 'items',
                            title   : 'text',
                        },
                        cache: true,
                        onSelect: function(result, response) {
                            console.log($(this));
                            var text = result.text;
                            var id = result.id;

                            $(this).find('input').val(text);
                            $(this).next('.reference_id').find('input').val(id)
                            return false;
                        },
                        minCharacters : 1
                    }
                );
            $('.reference_id').find('input').on('keyup',function() {
                var id = $(this).val();
                var text = $(this).parent().prev('.search').find('input');
                if (id.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '/admin/get_referenceById?id=' + id,
                        dataType: 'json',
                        success: function (response) {
                            text.val(response.text)
                        }
                    });
                }
            });
        }
    },
    'Reference': {
        init: function() {
            $('.search.ui')
                .search({
                    apiSettings: {
                        url: '/admin/get_reference?query={query}',
                    },
                    fields: {
                        results : 'items',
                        title   : 'text',
                    },
                    cache: true,
                    onSelect: function(result, response) {
                        var text = result.text;
                        var id = result.id;
                        $('#searchReferenceInputId').val(id)
                        $('#searchReferenceInput').val(text)
                        return false;
                    },

                    minCharacters : 1
                }
            );
        }
    },

    'AutocompleteBlock': {
        config: {
            el:'',
            context: '',
            container: '',
            url: '',
            images: true,
            text: 'title',
            dataset: [],
            dropDownOptions: {},
            templates: {},
            activeTemplate:'',
        },
        init: function(config){
            $.extend(Admin.AutocompleteBlock.config, config);
            var those = this;
            Dropdown.init(this.config.dropDownOptions);


            $('.search.ui')
                .search({
                    apiSettings: {
                        url: those.config.url,
                    },
                    fields: {
                        results : 'items',
                        title   : 'text',
                    },
                    cache: true,
                    onSelect: function(result, response) {
                        return false;
                    },
                    onResults: function(response) {
                        Admin.AutocompleteBlock.fill(response);
                        $('.autocompleteBlockResults .placeholder-block').on('click', function(){
                            var html = $('<div/>').append($(this).clone()).html();
                            those.config.context.insertContent(html);
                            those.config.context.windowManager.close();
                            return false;
                        });
                    },
                    minCharacters : 1
                }
            );
        },
        setFormat: function(format){
            this.config.activeTemplate = this.config.templates[format];
        },
        fill: function(data) {
            var html = '';
            var compiledTemplate = Template7.compile(this.config.activeTemplate);
            for(var i=0; i < data.length; i++) {
                html += compiledTemplate({
                    image: data[i].image,
                    iteration: i,
                    text: data[i][this.config.text],
                    url: '/signs/' + data[i]['category_id'] + '/#' + data[i]['n'],
                    n: data[i]['n'],
                    title: data[i]['title'],
                    path: '/signs/' + data[i]['category_id'] + '/' + data[i]['id']
                });
            }
            $(this.config.container).html(html);
        },
    }
};

var Dropdown = {
    config: {
        el:'.ui.dropdown',
        callback: false,
        focusInput: false
    },
    init: function(config){
        $.extend(Dropdown.config, config);
        this.create()

    },
    create: function() {
        var those = this;
        $(this.config.el).dropdown();
        var value = this.getValue();
        this.setValue(value);

        if (this.config.cookie) {
            $(this.config.el).find('.item').on('click', function(){
                var id = $(this).data('id');
                Cookie.set(those.config.cookie, id);

                if (those.config.callback) {
                    Admin.AutocompleteBlock[those.config.callback](id);
                }

                if (those.config.focusInput) {
                    $(those.config.focusInput).trigger('focus');
                }
            });

            if (those.config.callback) {
                Admin.AutocompleteBlock[those.config.callback](value);
            }
        }
    },
    getValue: function() {
        var value = undefined;
        if (this.config.cookie) {
            value = Cookie.get(this.config.cookie);
        }

        if (value === undefined) {
            value = $(this.config.el).find('.menu').children('.item').data('id');
        }

        return value;
    },
    setValue: function(id) {
        if (id === undefined) {
            return false;
        }

        var el = $(this.config.el).find('.item[data-id="'+id+'"]').trigger('click');
        $(this.config.el).find('.default.text').text(el.text)
    }
};


var Cookie = {
    get: function(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },
    set: function(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }
};

$(document).ready(function() {
    Admin.init();
});
