tinymce.PluginManager.add('rule', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('rule', {
        text: 'Правила',
        icon: false,
        onclick: function() {
            rules();
        }
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('rule', {
        text: 'Правила',
        context: 'tools',
        onclick: function() {
            rules();
        }
    });

    function rules() {
        // Open window
        editor.windowManager.open({
            title: 'Правила',
            width: 500,
            height: 400,
            body: [
                {
                    type   : 'container',
                    name   : 'container',
                    label  : 'Поиск',
                    html   : '<div class="ui search" id="searchAutocompleteBLock"><div class="ui left icon fluid input"><input class="prompt" id="searchAutocompleteBlockInput" name="parent" type="text" value="" autocomplete="off"><i class="search icon"></i></div><div class="results transition hidden"><a class="result"><div class="content"><div class="title">Общие положения</div></div></a></div></div>'
                },
                {
                    type   : 'container',
                    name   : 'container-rule',
                    label  : '',
                    html   : '<div class="autocompleteBlockResult" style="height:310px;"></div>'
                }
            ],
            buttons: {
                text: 'закрыть',
                subtype: 'primary',
                onclick: function() {
                    (this).parent().parent().close();
                }
            },
            onsubmit: function(e) {
                editor.windowManager.close();
            }
        });
        Admin.AutocompleteBlock.init({
            el: '#rule_autocomplete',
            context: editor,
            container: '.autocompleteBlockResult',
            url: '/admin/get_rules_by_query?query={query}',
            text: 'text'
        });
    }
});