tinymce.PluginManager.add('sign', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('sign', {
        text: 'Знаки',
        icon: false,
        onclick: function() {
            signs();
        }
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('sign', {
        text: 'Знаки',
        context: 'tools',
        onclick: function() {
            signs();
        }
    });

    function signs() {
        // Open window
        editor.windowManager.open({
            title: 'Знаки',
            width: 600,
            height: 400,
            body: [
                {
                    type   : 'container',
                    name   : 'container',
                    label  : 'Поиск',
                    style  : 'z-index:10',
                    html   : '' +
                    '<div class="ui search" id="searchAutocompleteBlock">' +
                        '<div class="ui left icon fluid input">' +
                            '<input class="prompt" id="searchAutocompleteBlockInput" name="parent" type="text" value="" autocomplete="off">' +
                            '<div class="ui compact selection dropdown" id="sign-dd">  ' +
                                '<input name="sign-dd" type="hidden" value="default">' +
                                '<i class="dropdown icon"></i>' +
                                '<div class="default text"></div>' +
                                '<div class="menu">' +
                                    '<div class="item" data-id="3">Только пункт</div>' +
                                    '<div class="item" data-id="2">Только малое изображение</div>' +
                                    '<div class="item" data-id="0">Только среднее изображение</div>' +
                                    '<div class="item" data-id="1">Изображение и заголовок</div>' +
                                '</div>' +
                            '</div>' +
                            '<i class="search icon"></i>' +
                            '<div class="autocomplete-control"></div>' +
                        '</div>' +
                    '</div>'
                },
                {
                    type   : 'container',
                    name   : 'container-sign',
                    style  : 'z-index:9',
                    label  : '',
                    html   : '<div class="autocompleteBlockResults" style="height:310px;"></div>'
                }
            ],
            buttons: {
                text: 'закрыть',
                subtype: 'primary',
                onclick: function() {
                    (this).parent().parent().close();
                }
            },
            onsubmit: function(e) {
                editor.windowManager.close();
            }
        });
        Admin.AutocompleteBlock.init({
            el: '#sign_autocomplete',
            context: editor,
            container: '.autocompleteBlockResults',
            url: '/admin/get_signs_by_query?query={query}',
            text:'title',
            dropDownOptions: {
                el: '#sign-dd',
                cookie: 'sign-dd-value',
                callback: 'setFormat',
                focusInput: '#searchAutocompleteBlockInput'
            },
            templates: {
                3:'<a href="{{url}}" data-path="{{path}}" title="{{title}}" class="placeholder-block placeholder-inline sign__popover popover">{{n}}</a>',
                2:'<a href="{{url}}" data-path="{{path}}" title="{{title}}" class="placeholder-block placeholder-block_small sign__popover popover"><img src="/images/{{image}}"></a>',
                1:'<a href="{{url}}" data-path="{{path}}" title="{{title}}" class="placeholder-block"><img src="/images/{{image}}"><span class="caption">{{text}}</span></a>',
                0:'<a href="{{url}}" data-path="{{path}}" title="{{title}}" class="placeholder-block"><img src="/images/{{image}}"></a>'
            }
        });
    }
});