$.fn.api.settings.api = {
    'schools': '/lookup/name={query}&type=customer',
    'lookup user': '/lookup/name={query}&type=user',
};

var Overlay = {
    config: {
        wrapper: 'body',
        el: '#overlay',
        closable: true,
    },
    init: function(config) {
        $.extend(Popup.config, config);
    },
    show: function() {
        if ($(this.config.el).length) {
            return true;
        }

        $(this.config.wrapper).append('<div id="overlay"></div>');
        $(this.config.el).addClass('opened');

        this.triggers();
    },
    triggers: function() {
        var those = this;
        $(this.config.el).off().on('click', function() {
            those.close();
            Popup.close();
        })
    },
    close: function() {
        $(this.config.el).fadeOut(function() {
            $(this).remove();
        })
    }
};

var Popup = {
    config: {
        rewrite: true,
        parent: false,
        url: '',
        wrapper: 'body',
        delay: 500,
        afterLoad: function() {

        },

    },
    init: function(config) {
        $.extend(Popup.config, config);
        NProgress.start();
        this.config.parent = this.createModal();
        this.load();
    },
    createModal: function() {
        if (this.config.parent !== false && this.config.rewrite === true) {
            return this.config.parent;
        }
        Overlay.show();

        var parent = $('<div class="modal__dialog" id="modal_'+ $('.modal__dialog').length +'"></div>');
        $(this.config.wrapper).append(parent);

        return parent;
    },
    load: function() {
        var those = this;

        $.ajax({
            type: "GET",
            url: those.config.url,
            success: function(response) {
                those.loaded(response);
            }
        });
    },
    loaded: function(data) {
        this.config.parent.html(data);
        $(this.config.wrapper).addClass('modal__opened');
        NProgress.done();

        return this.config.afterLoad();
    },
    close: function() {
        $(this.config.wrapper).removeClass('modal__opened');
        var self = this;
        setTimeout(function() {
            self.destroy();
        }, this.config.delay);

    },
    destroy: function() {
        this.config.parent.remove();
        this.config.parent = false;
        Overlay.close();
    }
};

var Utils = {
    config: {
        popoverDelay: 500,
        popoverTimer:0
    },
    seconds: 0,
    timer: function (duration, display) {
        var timer = duration, minutes, seconds;
        var interval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
            if (Questions.config.timeLimit != 0 && Questions.config.timeLimit <= seconds) {
                Questions.timeout();
                clearInterval(interval);
                return false;
            }

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.text(minutes + ":" + seconds);

            if (++timer < 0) {
                timer = duration;
            }
        }, 1000);
    },
    count: function (obj) {
        var count = 0;
        for (var prs in obj) {
            if (obj.hasOwnProperty(prs)) count++;
        }
        return count;
    },
    popover: function() {
        $('.popover').on('mouseover', function() {
            Utils.config.popoverTimer = setTimeout(function () {
                alert()
            }, Utils.config.popoverDelay);
        }).on('mouseleave', function(){
            clearTimeout(Utils.config.popoverTimer);
        });
    },
    checkUnique: function(list, item, key) {
        for (var i = 0; i < list.length; i++) {
            if(list[i][key] == item[key]) {
                return false;
            }
        }

        return true;
    },
    checkUniqueError: function(list, item) {
        for (var i = 0; i < list.length; i++) {
            if (list[i]['question'] == item.question) {
                return i;
            }
        }

        return false;
    }
};

var User = {
    config: {
        userBlock: $('.auth__block.authorized'),
        user:false,
        countInTicket: 20,
        ticketsCount: 40,
    },
    init: function() {
        if (this.config.userBlock.length !== 0) {
            this.config.user = {
                id: this.config.userBlock.data('id')
            };

            this.loadUserData();
        }
    },
    loadUserData: function() {
        var html = '';
        var those = this;
        $('.auth__block #stat').html(App.config.loader);
        $.ajax({
            type: "GET",
            url: '/user/stat',
            dataType: 'json',
            success: function(response) {
                var overall = response.stat.count * 20;
                var right = overall - response.stat.errors;

                html += '<a href="/user/edit/" class="auth__block-userdata"><img class="auth__block-userdata_image"src="'+ response.user.image +'" alt="Аватар пользователя '+ response.user.name +'"/><span> '+ response.user.name +' </span></a>';
                html += '<dl><dt>Билетов завершено:</dt><dd>' + response.stat.count + '</dd></dl><dl><dt>Ошибок допущено:</dt><dd>' + response.stat.errors  + '</dd></dl><br class="cl"/>';


                $('.auth__block #stat').html(html);
            }
        });
    },
    fillTicketData: function() {
        var keys = Storage.getKeys();
        var html = '';
        if (keys.length > 0) {
            html += 'Незавершенные задания';
            for (var i = 0; i <= keys.length; i++) {
                var data = Storage.getFromLocalStorage(keys[i]);
                var length = data.length;
                var level = (length / 20) * 100;
                var overall = 20;
                var right = 0;

                for (var j = 0; j <= (length-1); j++) {
                    right +=data[j].right;
                }

                if(typeof (length) !== 'undefined') {
                    var rightBar = '<div class="bar__level bar__level-right" style="width:'+ (right / 20) * 100 +'%"></div>';
                    var wrongBar = '<div class="bar__level bar__level-wrong" style="width:'+ ((parseInt(length) - parseInt(right)) / 20) * 100 +'%"></div>';
                    html += '<div class="bar">'+ rightBar +'' + wrongBar + '</div>';
                }
            }
            html += '<br class="cl"/>';
        }

        $('.auth__block').append(html)
    }
};

var Menu = {
    config: {
        opened: false,
        el: '#column_left',

    },
    init: function() {

    },

    open: function() {
        this.config.opened = true;
        $(this.config.el).addClass('opened');
    },
    close: function() {
        this.config.opened = false;
        $(this.config.el).removeClass('opened');
    }
};

var App = {
    config: {
        width: false,
        height: false,
        isMobile: false,
        loader: '<div class="loader" title="0"><svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0C22.32,8.481,24.301,9.057,26.013,10.047z"><animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatCount="indefinite"/></path></svg></div>',
        debug: true,
    },
    init: function() {
        this.route();
        this.resize();
        var those = this;
        $(window).on('resize', function() {
            those.resize();
        });
        var errorList = Storage.getFromLocalStorage('errors') ? Storage.getFromLocalStorage('errors') : [];

        if (errorList.length > 0) {
            Questions.renderErrors(errorList);
        }
    },
    resize: function() {
        this.config.width = $(window).width();
        this.config.height = $(window).height();
        if (this.config.width && this.config.width <= 768) {
            $('.question__navigation').addClass('large');
            this.config.isMobile = true;
            $('.keys__legend').html('Используйте свайп влево и вправо для смены вопроса')
        } else {
            $('.question__navigation').removeClass('large');
            this.config.isMobile = false;
            $('.keys__legend').html('<span class="ui horizontal label">&larr;</span> <span class="ui horizontal label">&uarr;</span> <span class="ui horizontal label">&darr;</span> <span class="ui horizontal label">&rarr;</span> <span class="ui horizontal label">enter</span> используйте горячие клавиши')
        }
        $('#column_left').css({'height': this.config.height})
    },
    route: function() {
        var router = new Router();
        router.connect(/^\/$/, function () {
            console.log('index init');
        });

        router.connect(/^\/tickets\/(\w+)/, function () {
            Questions.init();
        });

        router.connect(/^\/tickets/, function () {
            QuestionsList.init();
        });

        router.connect(/^\/examination/, function () {
            Questions.init({
                isExam: true
            });
        });

        router.connect(/^\/errors/, function () {
            Questions.init({
                isExam: false,
                isErrors: true
            });
        });

        router.connect(/^\/top100hardest/, function () {
            Questions.init({
                isExam: false,
                isErrors: false
            });
        });

        router.connect(/^\/signs\/(\w+)/, function () {
            Signs.init({

            });
        });

        router.dispatch(location);
    },
    window: function() {

    }
};
Signs = {
    config: {

    },
    init: function(config) {
        $.extend(Signs.config, config);
        Utils.popover();
    }
};

QuestionsList = {
    config: {
        count: 40

    },
    init: function(config) {
        $.extend(QuestionsList.config, config);
        this.fillProgress();
    },
    fillProgress: function(){
        for (var i = 1; i <= 40; i++) {
            var data = Storage.getFromLocalStorage(i);
            if (data) {
                $('#bar_' + i + ' .bar').remove();
                var length = data.length;
                var level = (length / 20) * 100;
                var right = 0;
                for (var j = 0; j <= (length-1); j++) {
                    right +=data[j].right;
                }
                var wrong = ((length-right) === 0) ? '' : (length-right);

                var level_wrong = (wrong / 20) * 100;
                var level_right = (right / 20) * 100;

                var inProgress = (20 === (wrong + right)) ? '' : ' inprogress';

                $('#progress_' + i).find('span').text(length);
                $('#bar_' + i).append('<div class="bar ' + inProgress + '" style="display:none;"><div class="bar__level bar__level-right" style="width: ' + level_right + '%">'+ right +'</div><div class="bar__level bar__level-wrong" style="width: ' + level_wrong + '%">' + wrong + '</div></div>');
                $('#bar_' + i + ' .bar').show();

                var ticket = $('#ticket_' + i);
                var url = ticket.attr('href');
                ticket.children('.ticket__item-content').append('<div class="ui buttons icon"><a href="'+ url +'" class="ui button" title="Продолжить"><i class="icon arrow right"></i></a><a href="'+ url +'?action=restart" class="ui button" title="Перезапустить"><i class=" icon refresh"></i></a></div>');
            }
        }
        $('.bar').addClass('visible');
    }
};

Questions = {
    config: {
        items: $('.question__item'),
        list: $('.question__items'),
        paginate: $('.question__pagination'),
        paginateWrapper: $('.question__pagination-wrapper'),
        pages: '',
        answers: $('.answer__item'),
        pageNextButton: $('#page_next'),
        pagePrevButton: $('#page_prev'),
        reloadButton: $('#restart_ticket'),
        currentPage: 1,
        currentQuestion: 0,
        currentQuestionItem: 0,
        currentTicket: 0,
        answersList: [],
        errorList: [],
        hasErrorInExam: false,
        questionsCount: 0,
        timeLimit: 20 * 60,
        additionalLimit: 0,
        additionalCount: 5,
        isExam: false,
        isErrors: false,
        loading: false
    },
    _construct: function(){
        this.items = this.config.items;
        this.list = this.config.list;
        this.paginate = this.config.paginate;
        this.paginateWrapper = this.config.paginateWrapper;
        this.answers = this.config.answers;
        this.pageNextButton = this.config.pageNextButton;
        this.pagePrevButton = this.config.pagePrevButton;
        this.reloadButton = this.config.reloadButton;
        this.currentPage = this.config.currentPage;
        this.answersList = this.config.answersList;
        this.timeLimit = this.config.timeLimit;
        this.currentQuestion = this.config.currentQuestion;
        this.currentQuestionItem = this.config.currentQuestionItem;
        this.currentTicket = this.config.currentTicket;
        this.isExam = this.config.isExam;
        this.isErrors = this.config.isErrors;
        this.loading = this.config.loading;
        this.additionalCount = this.config.additionalCount;
        this.additionalLimit = this.config.additionalLimit;
        this.errorList = Storage.getFromLocalStorage('errors') ? Storage.getFromLocalStorage('errors') : [];
        this.hasErrorInExam = this.config.hasErrorInExam;

        if (this.isErrors === true) {
            this.requestErrorsQuestions();
        }

        this.pages = this.config.paginateWrapper.find('a.paginate__item');
        this.questionsCount = this.pages.length;

        this.hash = window.location.hash.split('_');
        if (typeof this.hash[1] !== 'undefined' && this.hash[1].length > 0) {
            this.currentPage = this.hash[1];
        }

        var ticket_id = $('#question_' + this.currentPage).data('ticket_id');
        var actionInput = $('#action').val();
        if (actionInput === 'restart') {
            Storage.deleteFromLocalStorage(ticket_id);
            history.replaceState('page', "", ticket_id);
        }

        var answerList = Storage.getFromLocalStorage(ticket_id);

        if (answerList && !this.isExam) {
            var count = answerList.length;
            this.answersList = answerList;
            this.change(answerList[count-1].n)
            this.fillNavigation(answerList);
        } else{
            if (!$('#page_' + this.currentPage).length) {
                this.change(1);
            } else {
                this.change(this.currentPage);
            }
        }

        this.control();
        this.setTimer();
    },
    init: function(config){
        $.extend(Questions.config, config);
        this._construct(config);
        this.actions();
        this.constantActions();
    },
    refresh: function() {
        this.items = $('.question__item');
        this.list = $('.question__items');
        this.paginate = $('.question__pagination');
        this.paginateWrapper = $('.question__pagination-wrapper');
        this.answers = $('.answer__item');
        this.pageNextButton = $('#page_next');
        this.pagePrevButton = $('#page_prev');
        this.reloadButton = $('#restart_ticket');
        this.currentPage = this.config.currentPage;
        this.answersList = this.config.answersList;
        this.timeLimit = this.config.timeLimit;
        this.currentQuestion = this.config.currentQuestion;
        this.currentQuestionItem = this.config.currentQuestionItem;
        this.currentTicket = this.config.currentTicket;
        this.isExam = this.config.isExam;
        this.loading = this.config.loading;

        this.pages = this.config.paginateWrapper.find('a.paginate__item');
        this.questionsCount = this.pages.length;
        this.actions();
    },
    constantActions: function() {
        var those = this;

        var touch = new Hammer(document.getElementById('questions'));
        touch.on('swipe', function(ev) {
            ev.preventDefault();

            var dir = ev.direction;
            if (dir === 2) {
                those.next();
            }

            if (dir === 4) {
                those.prev();
            }
        });
    },
    actions: function() {
        var those = this;
        this.pages.off('click').on('click', function() {
            var page = $(this).data('order');
            those.change(page);
        });

        this.pageNextButton.off('click').on('click', function(e) {
            those.pagePrevButton.removeClass('disabled');
            those.next();
            e.preventDefault();
        });

        this.pagePrevButton.off('click').on('click', function(e) {
            those.pageNextButton.removeClass('disabled');
            those.prev();
            e.preventDefault();
        });

        this.reloadButton.off('click').on('click', function(e) {
            var yes = window.confirm('Начать сначала?');
            if (yes === false) {
                return false;
            }
        });


        this.answers.off('click').on('click', function(e) {
            if (!$(this).parent().hasClass('notAnswered')) {
                return false;
            }

            var el = $(this),
                answer_id  =  el.data('id');

            those.answers.removeClass('active');
            el.addClass('active');

            those.checkAnswer(answer_id);
            e.preventDefault();
        });
    },
    reload: function() {
        window.location.hash = '#q_1';
        window.location.reload();
    },
    setTimer: function() {
        Utils.timer(0, $('.question__timer'));
    },
    change: function(page){
        this.currentPage = page;
        this.items.removeClass('active');
        this.pages.removeClass('active');
        this.currentItemBlock = $('#question_' + page);
        this.currentItemBlock.addClass('active');
        this.currentPageButton = $('#page_' + page);
        this.currentPageButton.addClass('active');
        this.currentQuestion = this.currentItemBlock.data('n');
        this.currentQuestionItem = {
            n: this.currentItemBlock.data('n'),
            id: this.currentItemBlock.data('id'),
            block: this.currentItemBlock.data('block_id'),
            ticket: this.currentItemBlock.data('ticket_id'),
        };
        this.currentTicket = this.currentItemBlock.data('ticket_id');
        NProgress.start();
        NProgress.done();
        this.resetAnswer();
        if (this.isExam != true) {
            window.location.hash = '#q_' + page;
        }
        $('#question_id').html(this.currentQuestion);
        $('#ticket_id').html(this.currentTicket);
    },
    fillNavigation: function(list) {
        var those = this;
        for (var i = 0; i < list.length; i++) {
            var data = list[i];
            if (data.right === true) {
                those.setRight(data);
            } else {
                those.setWrong(data);
            }

            if (typeof data.answer !== 'undefined') {
                those.showAnswer(data);
            }

            $('.notAnswered#page_' + data.n).removeClass('notAnswered');
            $('#answerList_' + data.question).removeClass('notAnswered').find('.button').removeClass('primary').addClass('disabled');
        }
    },
    afterAnswer: function(right) {
        var those = this;
        this.config.loading = false;

        if (right === true && window.autoNextOnSuccess === 1) {
            this.next();
        }
        if (Utils.count(this.answersList) == this.questionsCount) {
            $.ajax({
                type: "POST",
                url: '/stats/save',
                dataType: 'json',
                data: {
                    list: those.answersList,
                    timer: $('.question__timer').html(),
                    ticket: (those.isExam === false && those.isErrors === false) ? those.items.last().data('ticket_id') : null,
                    mode: (those.isErrors) ? '_is_errors' : (those.isExam) ? '_is_exam' : null
                },
                success: function(response) {
                    $('#_modals').append(response.template);
                    setTimeout(function(){
                        $('#modal_'+response.data.rand).modal('show');
                    }, 600);

                    $('.question__timer').fadeOut(function(){
                        $(this).remove();
                    });
                    if (!those.isExam) {
                        Storage.deleteFromLocalStorage(those.currentTicket);
                    }

                    User.init();
                }
            });
        }
    },
    timeout: function() {
        var those = this;

        $.ajax({
            type: "POST",
            url: '/question/timeout',
            dataType: 'json',
            data: {
                list: those.answersList,
                timer: $('.question__timer').html(),
                ticket: those.items.last().data('ticket_id')
            },
            success: function(response) {
                $('#_modals').append(response.template);
                setTimeout(function(){
                    $('#modal_'+response.data.rand).modal('show');
                }, 600);

                $('.question__timer').fadeOut(function(){
                    $(this).remove();
                });

                if (!those.isExam) {
                    Storage.deleteFromLocalStorage(those.currentTicket);
                }
            }
        });
    },
    next: function() {
        var lastButton = this.items.last(),
            last = lastButton.data('order'),
            id = this.currentItemBlock.next('.question__item').data('order');

        if (id > last || id === undefined) {
            return false;
        }

        this.change(id);
    },
    prev: function() {
        var firstButton = this.items.first(),
            first = firstButton.data('order'),
            id = this.currentItemBlock.prev('.question__item').data('order');

        if (id < first || id === undefined) {
            return false;
        }

        this.change(id);
    },
    control: function() {
        var those = this;
        $(document).on('keydown', function(e) {
            switch(e.which) {
                case 37: // left
                    those.prev();
                    break;
                case 38: // top
                    those.prevAnswer();
                    break;
                case 39: // right
                    those.next();
                    break;
                case 40: // bottom
                    those.nextAnswer();
                    break;
                case 13: // enter
                    those.enterAnswer();
                    break;
                case 49: // 1
                case 97: // numpad 1
                    those.enterAnswerByNumber(1);
                    break;
                case 50: // 2
                case 98: // numpad 2
                    those.enterAnswerByNumber(2);
                    break;
                case 51: // 3
                case 99: // numpad 3
                    those.enterAnswerByNumber(3);
                    break;
                case 52: // 4
                case 100: // numpad 4
                    those.enterAnswerByNumber(4);
                    break;
                case 53: // 5
                case 101: // numpad 5
                    those.enterAnswerByNumber(4);
                    break;
                default:
                    return true;
            }
            e.preventDefault();
        });
    },
    checkAnswer: function(id) {
        NProgress.start();
        var those = this;
        those.currentItemBlock.addClass('loading');
        if (this.config.loading === false) {
            this.config.loading = true;
            $.post('/answers/' + id, function (data) {
                var item = {
                    id: data.id,
                    n: data.n,
                    right: data.right,
                    answer: data.answer,
                    question: data.question
                };

                if (Utils.checkUnique(those.answersList, item, 'question')) {
                    those.answersList.push(item);
                    if (!those.isExam) {
                        Storage.addToLocalStorage(those.currentTicket, those.answersList);
                    }
                }

                var errorList = Storage.getFromLocalStorage('errors');
                var checkInErrorList = Utils.checkUniqueError(errorList, item);

                if (data.right === false && checkInErrorList === false) {
                    those.errorList.push(item);
                    Storage.addToLocalStorage('errors', those.errorList);
                    those.renderErrors(those.errorList);
                }

                if (data.right === true && checkInErrorList !== false && those.isErrors === true) {
                    those.errorList.splice(checkInErrorList, 1);
                    Storage.addToLocalStorage('errors', those.errorList);
                    those.renderErrors(those.errorList);
                }

                if (data.right === true) {
                    those.setRight(data);
                } else {
                    those.setWrong(data);
                }

                if (typeof data.answer !== 'undefined') {
                    those.showAnswer(data);
                }

                $('.notAnswered#page_' + data.n).removeClass('notAnswered');
                $('#answerList_' + data.question).removeClass('notAnswered').find('.button').removeClass('primary').addClass('disabled');
                those.currentItemBlock.removeClass('loading');
                NProgress.done();
                those.afterAnswer(data.right);
            }).fail({

            });
        }
    },
    showAnswer: function(data) {
        $('#answer_' + data.answer).find('.button').addClass('green');
        if (typeof data.hint !== 'undefined') {
            this.showHint(data)
        }
    },
    showHint: function(data) {
        $('#answer_' + data.answer).after('<div id="hint_' + data.id + '" class="hint__item">' + data.hint + '</div>');
    },
    nextAnswer: function() {
        if (!this.currentItemBlock.find('.selectedAnswer').length) {
            this.currentItemBlock.find('.answer__item').first().addClass('selectedAnswer');
        } else {
            this.currentItemBlock
                .find('.selectedAnswer')
                .removeClass('selectedAnswer')
                .next('.answer__item')
                .addClass('selectedAnswer');
        }
    },
    prevAnswer: function() {
        if (!this.currentItemBlock.find('.selectedAnswer').length) {
            this.currentItemBlock.find('.answer__item').last().addClass('selectedAnswer');
        } else {
            this.currentItemBlock
                .find('.selectedAnswer')
                .removeClass('selectedAnswer')
                .prev('.answer__item')
                .addClass('selectedAnswer');
        }
    },
    enterAnswerByNumber: function(number) {
        if (number && this.currentItemBlock.find('.answer_' + number).length) {
            var id = this.currentItemBlock.find('.answer__item.answer_' + number).data('id');
            if (!this.currentItemBlock.find('.answers__block').hasClass('notAnswered')) {
                return false;
            }
            this.checkAnswer(id);
            return true;
        }
    },
    enterAnswer: function() {
        if (this.currentItemBlock.find('.selectedAnswer').length) {
            var id = this.currentItemBlock.find('.selectedAnswer').data('id');
            if (!this.currentItemBlock.find('.answers__block').hasClass('notAnswered')) {
                return false;
            }
            this.checkAnswer(id);
            return true;
        }
    },
    resetAnswer: function() {
        $('.selectedAnswer').removeClass('selectedAnswer');
    },
    setRight: function(data) {

        $('.paginate__item[data-id="' + data.question + '"]').addClass('green');
        $('#answer_' + data.id).find('.button').addClass('green');
    },
    setWrong: function(data) {
        $('.paginate__item[data-id="' + data.question + '"]').addClass('red');
        $('#answer_' + data.id).find('.button').addClass('red');

        if (this.isExam == true && this.additionalLimit < 2) {
            this.additionalLimit +=1;
            this.requestAdditionalQuestions();
        } else {

        }
    },
    requestErrorsQuestions: function() {
        var ids = [];
        var those = this;

        var list = Storage.getFromLocalStorage('errors');
        for (var i=0; i<list.length; i++) {
            ids.push(list[i]['question']);
        }

        $.ajax({
            type: "post",
            url: '/getErrors',
            dataType: 'json',
            data: {
                ids: ids
            },
            success: function(response) {
                those.fillAdditional(response)
                those.change(1);
            }
        });
    },
    requestAdditionalQuestions: function() {
        var list = [];
        var those = this;
        this.items.each(function(i, e) {
            list.push($(this).data('id'));
        });

        $.ajax({
            type: "post",
            url: '/additional',
            dataType: 'json',
            data: {
                list: list,
                count: those.additionalCount,
                timer: $('.question__timer').html(),
                question: those.currentQuestionItem
            },
            success: function(response) {
                those.fillAdditional(response);

                if (those.hasErrorInExam === false) {
                    alertify.log("Вам добавлено 5 вопросов");
                } else {
                    alertify.log("Вам добавлено еще 5 вопросов");
                }

                those.hasErrorInExam = true;
            }
        });
    },
    fillAdditional: function (data) {
        var those = this;
        var pagesHtml = '<div class="question__pagination ui buttons mini">';
        var questionsHtml = '';
        var order = 0;
        var length = data.length;
        var length_answers = 0;
        var i=0;
        var image = '';
        for (; i < length; i++) {
            order = (those.questionsCount + (i+1));
            pagesHtml += '<a name="page_' + order + '" data-id="' + data[i].id + '" data-ticket-id="' + data[i].ticket_id + '" data-order="' + order + '" id="page_' + order +'" class="notAnswered ui button paginate__item ">' + order + '</a> ';
            if (data[i].image) {
                image = '<img src="/images/questions/'+ data[i].image +'"/>';
            } else {
                image = '<div class="question__image question__noimage segment">Вопрос без изображения</div>';
            }
            this.list.append('<div class="question__item " data-order="' + order + '" data-id="' + data[i].id + '" data-subject_id="' + data[i].subject_id + '" data-category_id="' + data[i].category_id + '" data-ticket_id="'+data[i].ticket_id+'" data-block_id="'+data[i].block_id+'" data-n="'+data[i].n+'" id="question_'+order+'"><h2>'+data[i].question+'</h2><div class="ui divider"></div><div class="segment question__image">'+image+'</div><div class="ui middle aligned divided selection list answers__block notAnswered" id="answerList_'+data[i].id+'"></div></div>');
            length_answers = data[i].answers.length;
            for (j=0; j < length_answers; j++) {
                $('#answerList_'+data[i].answers[j].question_id).append('<label class="item answer__item vertical answer_' + (j+1) + '" data-id="' + data[i].answers[j].id + '" data-question-id="1" data-answer-num="'+ (j+1) +'" id="answer_'+data[i].answers[j].id+'"><div class="right floated content"><div class="ui vertical  button primary" tabindex="'+(j+1)+'"><div class="content"><i class="checkmark icon"></i></div></div></div><div class="header"><p class="content"><a class="ui blue circular label">'+(j+1)+'</a> '+data[i].answers[j].answer+'</p></div></label>');
            }
        }
        pagesHtml += '</div>';

        this.paginateWrapper.append(pagesHtml);
        this.fillAdditionalAnswers(data);
        this.refresh();
    },
    fillAdditionalAnswers: function(data) {
        var length = data.length;
    },
    renderErrors: function(errors) {
        var length = errors.length;
        $('#errors__button').show();
        $('.errors__count').html(length);
        if (length <= 0) {
            $('#errors__button').hide();
        }
    }
};

Storage = {
    addToLocalStorage: function (key, list) {
        var getItem = localStorage.getItem(key);
        if (getItem) {
            localStorage.removeItem(key);
        }

        localStorage.setItem(key, JSON.stringify(list));
    },
    getFromLocalStorage: function (key) {
        var getItem = localStorage.getItem(key);
        if (getItem) {
            return JSON.parse(getItem);
        }

        return false;
    },
    deleteFromLocalStorage: function (key) {
        return localStorage.removeItem(key);
    },
    getKeys: function() {
        return Object.keys(localStorage);
    }
};

var Modal = {
    onOpen: function() {
        $(document).off('keydown');
    },
    onClose: function() {
        App.init();
    }
};

var Auth = {
    init: function() {
        var roleItem = $('.modal__select_role li');
        var role = 0;
        var those = this;
        roleItem.off().on('click', function() {
            role = $(this).data('role');
            those.hideByRole();
            those.showByRole(role)
            roleItem.removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input').attr('checked', true);
        });

        $('#selectbox__school').dropdown();
        $('#selectbox__city').dropdown('setting', 'onChange', function(city_id, text, $selectedItem){
            if (city_id != 0) {
                School.loadListByCityId(city_id, $('#selectbox__school'));
            } else {
                $('#selectbox__school').addClass('disabled').dropdown('clear');
            }
        });

    },
    hideByRole: function() {
        $('.roled').hide();
    },
    showByRole: function(role) {
        $('.roled-id-' + role).show();
    },
};

var School = {
    loadListByCityId: function(city_id, el) {
        el.removeClass('disabled')
            .dropdown('clear')
            .dropdown({
            apiSettings: {
                url: '/school/list/' + city_id,
            },
            saveRemoteData: false,
            filterRemoteData: true
        });
    }
};

$(document).ready(function() {
    App.init();
    User.init();
    Menu.init();


    $('.modal__call-login').on('click', function () {
        Popup.init({
            url: $(this).attr('href'),
        });

        return false;
    });

    $('.modal__call-register').on('click', function () {
        Popup.init({
            url: $(this).attr('href'),
            afterLoad: function() {
                Auth.init();
            },
        });

        return false;
    });






   /* $('.ui.dropdown').dropdown({
        on: 'click'
    });*/

});


