<div class="menu">
    <ul>
        @if (isset($innerMenu) && count($innerMenu) > 0)
            <li class="menu_back"><a href="/">&larr; <span class="link">Назад</span></a></li>
            @foreach ($innerMenu as $item)
                <li class="@if(str_contains(Route::getCurrentRoute()->getName(), $item->getName()))active @endif">
                    <a href="{{$item->geturl()}}">@if($item->getIcon())<i class="icon {{ $item->getIcon() }}"></i> @endif<span>{{$item->getTitle()}}</span></a>
                </li>
            @endforeach
        @else
            @foreach ($menu as $item)
                <li class="@if(str_contains(Route::getCurrentRoute()->getName(), $item->getName()))active @endif">
                    <a href="{{$item->geturl()}}">@if($item->getIcon())<i class="icon {{ $item->getIcon() }}"></i> @endif<span>{{$item->getTitle()}}</span></a>
                </li>
            @endforeach

            @if (Auth::guest())
                <li><a class="modal__call-login pseudo" href="{{ url('/login') }}"><span>Вход</span></a></li>
                <li><a class="modal__call-register pseudo" href="{{ url('/register') }}"><span>Регистрация</span></a></li>
            @endif
            <li class="menu_user">
            @include('_blocks.auth')
            </li>
        @endif
    </ul>
</div>