
<canvas id="chart" class="statChart"></canvas>
<script src="/js/components/chart.js/dist/Chart.js"></script>

<script>
    var ctx = document.getElementById("chart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [{!! implode(array_map(function($e) {
                return '\'' . $e['question_id'] . '\'';
            }, $data), ',') !!}],
            datasets: [{
                label: '# of Votes',
                data: [{{ implode(array_map(function($e) {
                    return $e['wrong_answer_count'];
                }, $data), ',') }}],
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>

<div class="ui relaxed divided list">
@foreach ($data as $item)
    <div class="item">
        <i class="large middle aligned"></i>
        <div class="content">
            <a class="header" href="/tickets/{{$item['ticket_id']}}#{{$item['question_n']}}">{{$item['question_title']}}</a>
            <div class="description"><div class="ui red small circular label">{{$item['wrong_answer_count']}}</div> {{$item['wrong_answer_text']}}</div>
        </div>
    </div>
@endforeach
</div>