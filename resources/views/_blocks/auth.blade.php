@if (Auth::guest())

@else
    <div class="block auth__block authorized" data-id="{{Auth::user()->id}}">
        <div id="stat"></div>
        <ul class="auth__block_menu">
            <li class="left">
                <a title="Настройки аккаунта" href="/user/settings"><i class="setting icon"></i></a>
            </li>
            <li class="right">
                <a title="Выход из аккаунта" class="item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="sign out icon"></i></a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </li>
        </ul>
    </div>
@endif