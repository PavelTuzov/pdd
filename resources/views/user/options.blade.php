@extends('layouts.app')
@section('content')
    <h1>Редактирование опций пользователя {{Auth::user()->name}}</h1>
    <div class="ui segment">
        @include('nav.inner')
        <div class="ui form">
            <div class="grouped fields">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/save_options') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="field">
                    <div class="ui slider checkbox">
                        <input id="autoNextOnSuccess" type="checkbox" name="options[autoNextOnSuccess]" {{ ($options['autoNextOnSuccess']) ? 'checked' : ''}} value="1">
                        <label for="autoNextOnSuccess">Автоматически переходить на следующий вопрос при правильном ответе</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary">
                    <i class="fa fa-btn fa-user"></i> Сохранить
                </button>
            </form>
            </div>
        </div>
    </div>
@endsection