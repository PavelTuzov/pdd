@extends('layouts.app')
@section('content')
<h1>Редактирование пользователя {{Auth::user()->name}}</h1>
<div class="ui segment">
    @include('nav.inner')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/save') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
            <div class="col-md-6">
                @if(Auth::user()->image)
                    <div class="col-md-2">
                        <img class="navbar__auth__avatar" src="{{$data->image}}" alt="Аватар пользователя {{Auth::user()->name}}"/>
                    </div>
                    <label class="col-md-2 control-label">Удалить аватар?</label>
                    <div class="col-md-8">
                        <input type="checkbox" name="deleteAvatar" value="1" id="deleteAvatar" class="hidden" @if((isset($item->validated) && $item->validated))checked @endif/>
                        <label class="pure-toggle wide" for="deleteAvatar"><span class="fa-remove"></span><span class="fa-check"></span></label>
                    </div>
                @endif
                <input type="file" class="form-control" name="image" value="">
                @if ($errors->has('image'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                @endif
            </div>

                <input type="password" class="form-control" name="password">

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

    <!--div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
            </div>
        </div-->


                <button type="submit" class="btn btn-block btn-primary">
                    <i class="fa fa-btn fa-user"></i> Сохранить
                </button>

    </form>
</div>
@endsection