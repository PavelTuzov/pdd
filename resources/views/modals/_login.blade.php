
<form class="ui small form" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="field">
            <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="input" name="email" value="{{ old('email') }}"  placeholder="Почтовый ящик" required autofocus>
            </div>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" placeholder="Пароль" required>
            </div>
        </div>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <div class="field">
            <div class="ui left icon input">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                    <a class="right" href="{{ url('/password/reset') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui input">
                <input type="submit" class="ui button primary" value="Вход"/>
            </div>
        </div>

</form>