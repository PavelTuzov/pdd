<form class="ui small form" role="form" method="POST" action="{{ url('/register') }}">
    <div class="ui grid stackable modal__window">
        <div class="ten wide column">
            {{ csrf_field() }}
            <div class="">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="input" name="email" value="{{ old('email') }}"  placeholder="Почтовый ящик" required autofocus>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Пароль" required>
                    </div>
                </div>
                <div class="roled-id-1 roled-id-2 roled">
                    <div class="ui grid">
                        <div class="eight wide column">
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" name="name" placeholder="Имя">
                                </div>
                            </div>
                        </div>
                        <div class="eight wide column">
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" name="name" placeholder="Фамилия">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ui grid field focus__block">
                        <div class="eight wide column">
                            <div class="field">
                                <div class='ui search selection dropdown' id="selectbox__city">
                                    <input name="city" id="city" value="" type="hidden">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Город</div>
                                    <div class="menu">
                                        <div class="item" data-value="0">---</div>
                                        @foreach ($cities as $city)
                                            <div class="item" data-value="{{$city->id}}">{{$city->name}}</div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="eight wide column">
                            <div class="field">
                                <div class='ui search selection dropdown' id="selectbox__school">
                                    <input name="school" id="school" value="" type="hidden">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Автошкола</div>
                                    <div class="menu">
                                        @foreach ($cities as $city)
                                            <div class="item" data-value="{{$city->id}}">{{$city->name}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <div class="ui grid">
                    <div class="eight wide column">
                        <div class="field">
                            <div class="ui input">
                                <input type="submit" class="ui button primary" value="Регистрация"/>
                            </div>
                        </div>
                    </div>
                    <div class="eight wide column fright">
                        <a class="ui google plus button compact">
                            <i class="google plus icon"></i>
                        </a>
                        <a class="ui vk button compact">
                            <i class="vk icon"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="six wide column auth__background">
            <ul class="modal__select_role">
                <li data-role="0" class="selected"><label><input type="radio" name="role" value="0" checked/>Пользователь</label></li>
                <li data-role="1"><label><input type="radio" name="role" value="1"/>Курсант автошколы</label></li>
                <li data-role="2"><label><input type="radio" name="role" value="2"/>Представитель автошколы</label></li>
            </ul>
        </div>
    </div>
</form>