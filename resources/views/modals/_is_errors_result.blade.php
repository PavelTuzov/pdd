<div class="ui modal" id="modal_{{$rand}}">
    <i class="close icon"></i>
    <div class="header">
        Результат
    </div>
    <div class="image content">
        <div class="description">
            <div class="ui header">Вопросов: <span class="question__count">{{$answers}}</span>. Ошибок: <span class="errors__count">{{$errors}}</span></div>
            @if ($errors === 0)
                <p>Перейти к билетам?</p>
            @else
                <p>Повторить работу над ошибками или перейти к билетам?</p>
            @endif
        </div>
    </div>
    <div class="actions">
        @if ($errors !== 0)
            <a href="{{url('errors')}}" class="ui black deny button">
                Повторить работу над ошибками
            </a>
        @endif
        <a href="{{url('tickets')}}" class="ui positive right labeled icon button">
            Перейти к билетам
            <i class="checkmark icon"></i>
        </a>
    </div>
</div>