<div class="ui modal" id="modal_{{$rand}}">
    <i class="close icon"></i>
    <div class="header">
        Результат
    </div>
    <div class="image content">
        <div class="description">
            <div class="ui header">Вопросов: <span class="question__count">{{$answers}}</span>. Ошибок: <span class="errors__count">{{$errors}}</span></div>
            <p>Начать сначала или перейти к следующему билету?</p>
        </div>
    </div>
    <div class="actions">
        <div class="ui black deny button">
            Не, давай сначала
        </div>
        <div class="ui positive right labeled icon button">
            Да, поехали
            <i class="checkmark icon"></i>
        </div>
    </div>
</div>