@extends('layouts.app')
@section('content')
    @if($item)
        <h1>{{$item->question}}</h1>
        @if($item->image)
            {{$item->image}}
        @endif
        <hr/>
        <ul class="answers__block">
        @foreach($item->answers as $answer)
            <li><label><input type="radio" name="answer"/> {{$answer->answer}}</label></li>
        @endforeach
        </ul>
    @endif
@endsection