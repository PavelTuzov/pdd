@extends('layouts.app')
@section('content')
    @if(isset($items) && count($items) > 0)
        <input type="hidden" id="action" value="{{$action}}"/>
        <div class="ui segment">
            <div class="row question__pagination-wrapper">
                @foreach($items as $i => $item)
                    @if ($item->n % 5 === 1)
                    @if ($item->n !== 1) </div>@endif
                <div class="question__pagination ui buttons ">
                    @endif
                    <a name="page_{{$i+1}}"
                       data-id="{{$item->id}}"
                       data-ticket-id="{{$item->ticket_id}}"
                       data-order="{{($i+1)}}"
                       data-block="{{$item->block_id}}"
                       id="page_{{($i+1)}}"
                       class="ui button notAnswered paginate__item @if ($loop->first) active @endif">{{$i+1}}</a>
                @endforeach
                </div>
            </div>
        </div>
        <div class="ui row">
            <div class="ui two column grid">
                <div class="column question__navigation-wrapper">
                    <div class="ui buttons width mini question__navigation">
                        <a id="page_prev" data-type="next" class="ui button mini">
                            <div class="content">
                                <i class="arrow left icon"></i>
                            </div>
                        </a>
                        @if (isset($isExam) && $isExam === false)
                        <a href="?action=restart" id="restart_ticket" data-type="restart" class="ticket__nav-restart ui button mini">
                            <div class="content">
                                <i class="refresh icon"></i>
                            </div>
                        </a>
                        @endif
                        <a id="page_next" data-type="prev" class="ui mini button">
                            <div class="content">
                                <i class="arrow right icon"></i>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="column question__label-wrapper">
                    <div class="">Билет №<span id="ticket_id">{{$item->ticket_id}}</span>, Вопрос №<span id="question_id"></span></div>
                </div>
            </div>
        </div>
        <div class="question__items ui stacked segment" id="questions">
            <a class="ui teal right  label question__timer">00:00</a>
        @foreach($items as $i => $item)
            <div class="question__item @if ($loop->first) active @endif"
                 data-order="{{($i+1)}}"
                 data-id="{{$item->id}}"
                 data-subject_id="{{$item->subject_id}}"
                 data-category_id="{{$item->category_id}}"
                 data-ticket_id="{{$item->ticket_id}}"
                 data-block_id="{{$item->block_id}}"
                 data-n="{{$item->n}}"
                 id="question_{{($i+1)}}">
                <h2>{{$item->question}}</h2>
                <div class="ui divider"></div>
                @if($item->image)
                    <div class="segment question__image">
                        <img src="{{getenv('APP_URL')}}/images/questions/{{$item->image}}"/>
                    </div>
                @else
                    <div class="question__image question__noimage segment">Вопрос без изображения</div>
                @endif
                <div class="ui middle aligned divided selection list answers__block notAnswered" id="answerList_{{$item->id}}">
                    @foreach($item->answers as $key => $answer)
                        <label
                                class="item answer__item attached segment ui answer_{{($key+1)}}"
                                data-id="{{$answer->id}}"
                                data-question-id="{{$item->n}}"
                                data-answer-num="{{($key+1)}}"
                                id="answer_{{$answer->id}}"
                        >
                            <div class="right floated content">
                                <div class="ui vertical  button primary" tabindex="0">
                                    <div class="content">
                                        <i class="checkmark icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="header">
                                <p class="content"><a class="ui blue circular label">{{($key+1)}}</a> {{$answer->answer}}</p>
                            </div>
                        </label>
                    @endforeach
                </div>
            </div>
        @endforeach
        </div>

    @else
        <input type="hidden" id="action" value=""/>
        <div class="ui segment">
            <div class="row question__pagination-wrapper">
                <div class="question__pagination">

                </div>
            </div>
        </div>
        <div class="ui row">
            <div class="ui two column grid">
                <div class="column question__navigation-wrapper">
                    <div class="ui buttons width mini question__navigation">
                        <a id="page_prev" data-type="next" class="ui button mini">
                            <div class="content">
                                <i class="arrow left icon"></i>
                            </div>
                        </a>
                        <a id="page_next" data-type="prev" class="ui mini button">
                            <div class="content">
                                <i class="arrow right icon"></i>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="column question__label-wrapper">
                    <div class="">Билет №<span id="ticket_id"></span>, Вопрос №<span id="question_id"></span></div>
                </div>
            </div>
        </div>
        <div class="question__items ui stacked segment" id="questions">

        </div>
    @endif
    <div class="keys__legend">

    </div>
@endsection