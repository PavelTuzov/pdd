@extends('layouts.app')
@section('content')
    <h1>{{$title}}</h1>
    <div class="ui cards stackable three">
    @foreach($items as $k => $ticket)
            <a href="/tickets/{{$ticket->ticket_id}}" class="ui card ticket__item" id="ticket_{{$k}}">
                <div class="ticket__item-content" style="background-image: url({{getenv('APP_URL')}}/images/questions/{{$ticket->image}});">
                    <div class="ticket__item-header"><span class="ui blue circular huge label">{{$ticket->ticket_id}}</span></div>
                </div>
                <div class="extra content" id="bar_{{$k}}">
                    @if (isset($ticket->stat->errors))
                        <div class="bar"><div class="bar__level bar__level-right" style="width: {{$ticket->stat->getRightPercentAttribute()}}%">{{$ticket->stat->getRightAttribute()}}</div><div class="bar__level bar__level-wrong" style="width: {{$ticket->stat->getWrongPercentAttribute()}}%">{{$ticket->stat->errors}}</div></div>
                    @endif
                    <span class="left floated">
                        @if (isset($ticket->stat->level))
                            <i class="battery {{$ticket->stat->level}} icon"></i>
                        @endif
                    </span>
                    <span class="right floated error">
                        <span class="ticket__item-progress" id="progress_{{($k)}}">Прогресс <span>0</span>/20</span>
                        @if (Auth::check() === false)
                            <i class="icon warning circle" title="Вы не авторизованы. Ваш прогресс не сохранится по завершению билета."></i>
                        @endif
                    </span>
                </div>
            </a>
        @endforeach
    </div>
@endsection