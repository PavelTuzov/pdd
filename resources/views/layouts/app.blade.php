<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{$settings::meta_keywords()}}">
    <meta name="description" content="{{$settings::meta_desc()}}">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title>{{$settings::title()}}</title>
    <link href="/css/app.css" rel="stylesheet">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

<div id="app">

    <div id="column_left" class="column__left">
        @include('_blocks.menu')
    </div>

    <div id="column_right" class="column__right">
        <div class="wrapper">
        @yield('content')
        </div>
        @include('_blocks.footer')
    </div>
</div>


@if (Auth::guest() === false && Auth::user()->options !== null)
    <script>
        @foreach (json_decode(Auth::user()->options, true) as $option => $value)
        var {{$option}} = {{$value}};
        @endforeach
    </script>
@endif
<script src="/js/app.js"></script>
</body>
</html>
