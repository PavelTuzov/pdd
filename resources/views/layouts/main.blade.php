<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>inpdd.ru</title>
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body id="{{Route::getCurrentRoute()->getName()}}">
<div  id="header">
    <div class="ui container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <!--a href="/"><img src="/images/logo.png" alt="" id="header__logo"/></a>
                <div id="header__title">
                    <h2>Подготовься к эзамену сейчас</h2>
                    <p>ПДД навигатор- бесплатный онлайн экзамен ПДД 2016 по билетам ГИБДД</p>
                </div-->
                <div class="icon" id="header__menu">
                    <div class="ui container menu-wrapper">
                        <ul class="left">
                            @foreach ($menu as $item)
                                <li class="@if(str_contains(Route::getCurrentRoute()->getName(), $item->getName()))active @endif"><a href="{{$item->geturl()}}">{{$item->getTitle()}}</a></li>
                            @endforeach
                        </ul>
                        <ul class="right">
                            @if (Auth::guest())
                                <li><a class="modal__call-login pseudo" href="{{ url('/login') }}"><span>Вход</span></a></li>
                                <li><a class="modal__call-register pseudo" href="{{ url('/register') }}"><span>Регистрация</span></a></li>
                            @else
                                <li><a href="/user/edit">{{Auth::user()->name}}</a></li>
                                <li>
                                    <a class="item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ui container" id="body">
    <div class="ui stackable grid">
        <div class="{{(isset($wide) && $wide === true) ? 'sixteen' : 'twelve'}} wide column">
            @if (isset($innerMenu) && count($innerMenu) > 0)
                <div class="icon" id="inner__menu">
                    <div class="ui container menu-wrapper">
                        <ul class="left">
                            @foreach ($innerMenu as $item)
                                <li class="@if(str_contains(Route::getCurrentRoute()->getName(), $item->getName()))active @endif"><a href="{{$item->geturl()}}">{{$item->getTitle()}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @yield('content')
        </div>
        @if ((isset($wide) && $wide === false) || isset($wide) === false)
        <div class="four wide column" id="sideblock">
            <div class="block-group">
                @if (Auth::guest())
                <div class="block auth__block">
                    <p>
                        Если вы <a class="modal__call-login pseudo" href="{{ url('/login') }}">авторизуетесь</a>, то сможете наблюдать за своим прогрессом.
                    </p>
                </div>
                @else
                    <div class="block auth__block authorized" data-id="{{Auth::user()->id}}">
                        <div id="stat"></div>
                    </div>
                @endif
                <div class="block errors__block">
                    <p>
                        <a class="ui fluid deny button primary" href="{{ url ('top100hardest') }}">топ 100 сложных</a>
                    </p>
                    <p  id="errors__button">
                        <a class="ui red fluid deny button" href="{{ url ('errors') }}">Повторить <b class="errors__count">0</b> вопросов</a>
                    </p>
                </div>
                <div class="block telegram__block">
                    <a target="_blank" href="https://telegram.me/inpdd_bot" title="Телеграм бот ПДД">
                        <img src="/images/telegram.png" alt="пдд бот"/>

                        <p>@inpdd_bot</p>
                    </a>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>



</body>
@include('_blocks.footer')
@if (Auth::guest() === false && Auth::user()->options !== null)
    <script>
        @foreach (json_decode(Auth::user()->options, true) as $option => $value)
    var {{$option}} = {{$value}}
        @endforeach
    </script>
@endif
<script src="{{ elixir('js/app.js') }}"></script>
</html>
