@extends(isset($isAjax) && $isAjax ? 'layouts.ajax' : 'layouts.app')

@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui header">
                <div class="content">
                    Регистрация
                </div>
            </h2>

            @include('modals._register')
        </div>
    </div>
@endsection
