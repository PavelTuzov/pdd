@extends(isset($isAjax) && $isAjax ? 'layouts.ajax' : 'layouts.app')

@section('content')
<div class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui header">
            <div class="content">
                Войти
            </div>
        </h2>

       @include('modals._login')
    </div>
</div>
@endsection
