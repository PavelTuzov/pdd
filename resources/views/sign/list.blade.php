@extends('layouts.app')
@section('content')
    @if ($items)
        @if($root === true)
            <h1>{{$item->title}}</h1>
        @endif
        @foreach($items as $item)
            <div class="question__items ui stacked segment" name="">
                <b>{{$item->n}}</b>
                @if($root === true)
                    <a href="/signs/{{$item->category_id}}"> {{$item->title}}</a>
                @else
                    {!! nl2br($item->text) !!}
                @endif
            </div>
        @endforeach
    @endif
@endsection