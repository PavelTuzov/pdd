@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">
        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($root_items as $category)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/references/{{$category->id}}" class="header">п. правил №{{$category->n}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column grid ui">
            <div class="ui list divided">
                {!! Form::model($item,['route'=>['admin.reference.save', $item->id],'class'=>'form ui','id'=>'reference_item_form','files'=>false]) !!}
                @include('admin._blocks._reference')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection