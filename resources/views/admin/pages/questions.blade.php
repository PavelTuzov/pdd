@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">

        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($tickets as $ticket)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/questions/{{$ticket->ticket_id}}" class="header">Билет №{{$ticket->ticket_id}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column">
            <div class="ui list divided">
            @foreach ($items as $k => $item)
                <div class="item">
                    @if($item->image)
                        <img class="ui avatar image" src="/images/{{$item->image}}">
                    @endif
                    <div class="content">
                        <a href="/admin/question/{{$item->ticket_id}}/{{$item->id}}" class="header">Вопрос №{{$item->n}} из билета №{{$item->ticket_id}}</a>
                        <div class="description">{{$item->question}}</div>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="container">
                <a href="/admin/question" class="ui left icon button">
                    <i class="plus icon"></i> Добавить вопрос
                </a>
            </div>
        </div>
    </div>
@endsection