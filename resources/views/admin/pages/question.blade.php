@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">

        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($tickets as $ticket)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/questions/{{$ticket->ticket_id}}" class="header">Билет №{{$ticket->ticket_id}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column grid ui">
            <div class="ui list divided">
                {!! Form::model($item,['route'=>['admin.question.save', $item->id],'class'=>'form ui','id'=>'question_item_form','files'=>true]) !!}
                @include('admin._blocks._question')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection