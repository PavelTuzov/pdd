@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">
        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($root_items as $category)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/references/{{$category->id}}" class="header">п. правил №{{$category->n}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column">
            <div class="ui list divided">
                @foreach ($items as $k => $item)
                    <div class="item">
                        <div class="content">
                            <p>{{$item->text}}</p>
                            <a href="/admin/reference/{{$item->id}}" class="">Редактировать</a>
                        </div>
                    </div>
                    @if ($item->has('tree') && count($item->tree) > 0)
                        @foreach($item->tree as $subitem)
                            <div class="item">
                                <div class="content">
                                    <p>
                                        {{$subitem->text}}
                                    </p>
                                    <a href="/admin/reference/{{$subitem->id}}" class="">Редактировать</a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                @endforeach
            </div>
            <div class="container">
                <a href="/admin/reference" class="ui left icon button">
                    <i class="plus icon"></i> Добавить правило
                </a>
            </div>
        </div>
    </div>
@endsection