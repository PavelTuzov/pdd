@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">

        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($items as $item)
                    <div class="item">
                        <div class="content">
                            Билет № {{$item->id}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection