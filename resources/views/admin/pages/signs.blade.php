@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">
        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($root_items as $category)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/signs/{{$category->id}}" class="header">{{$category->title}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column">
            <div class="ui list divided">
                @foreach ($items as $k => $item)
                    <div class="item">
                        <div class="content">
                            <p>{{$item->title}}</p>
                            <a href="/admin/sign/{{$item->id}}" class="">Редактировать</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="container">
                <a href="/admin/sign" class="ui left icon button">
                    <i class="plus icon"></i> Добавить знак
                </a>
            </div>
        </div>
    </div>
@endsection