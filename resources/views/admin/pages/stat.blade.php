@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">
        <div class="sixteen wide equal column">
            @include('_blocks.chart', ['data' => $item['topWrongAnswer']])
        </div>
    </div>
@endsection