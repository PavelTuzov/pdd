@extends('admin.layouts.main')
@section('content')
    <div class="ui stackable equal divided grid ">
        <div class="four wide equal column">
            <div class="ui list relaxed divided">
                @foreach ($root_items as $category)
                    <div class="item">
                        <div class="content">
                            <a href="/admin/signs/{{$category->id}}" class="header">{{$category->title}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="twelve wide column grid ui">
            <div class="ui list divided">
                {!! Form::model($item,['route'=>['admin.sign.save', $item->id],'class'=>'form ui','id'=>'sign_item_form','files'=>true]) !!}
                @include('admin._blocks._sign')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection