<div class="field{{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Заголовок') !!}</label>
    <div class="">
        {!! Form::text('title', null, ['class'=>'ui', 'field']) !!}
    </div>
</div>
<div class="field{{ $errors->has('text') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Текст') !!}</label>
    <div class="">
        {!! Form::textarea('text',null,['class'=>'ui']) !!}
    </div>
</div>
<div class="field">
    <label>{!! Form::label('Изобаржение знака') !!}</label>
    <div>
        @if ($item->image)
            <img src="/images/{!! $item->image !!}"/>
        @endif
        {!! Form::file('image', []) !!}
    </div>
</div>
<div class="field{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Категория') !!}</label>
    <div class="">
        {!! Form::select('category_id',$category_list, $item->category_id ?: $preset['category_id'] ,['class'=>'ui dropdown fluid']) !!}
    </div>
</div>
<div class="two fields">
    <div class="field{{ $errors->has('n') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Номер знака') !!}</label>
        <div class="">
            {!! Form::text('n', null, ['class'=>'ui']) !!}
        </div>
    </div>
</div>

<div class="container">
    {!! Form::submit('Сохранить', ['class' => 'ui submit button right floated']) !!}
</div>