<div class="field{{ $errors->has('text') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Текст') !!}</label>
    <div class="">
        {!! Form::textarea('text',null,['class'=>'ui', 'type']) !!}
    </div>
</div>

<div class="field{{ $errors->has('parent_id') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Родительское правило') !!}</label>
    <div class="ui search" id="searchReference">
        <div class="ui left icon fluid input">
            {!! Form::text('parent', isset($item->parent->text) ? $item->parent->text : null,['class'=>'prompt', 'id' => 'searchReferenceInput']) !!}
            <i class="search icon"></i>
        </div>
    </div>
    {!! Form::hidden('parent_id', null, ['id' => 'searchReferenceInputId']) !!}
</div>
<div class="two fields">
    <div class="field{{ $errors->has('n') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Номер пункта правил') !!}</label>
        <div class="">
            {!! Form::text('n', null, ['class'=>'ui']) !!}
        </div>
    </div>
</div>

<div class="container">
    {!! Form::submit('Сохранить', ['class' => 'ui submit button right floated']) !!}
</div>