<div class="field{{ $errors->has('text') ? ' has-error' : '' }}">
    <label class="">{!! Form::label('Текст') !!}</label>
    <div class="">
        {!! Form::textarea('question',null,['class'=>'ui', 'type']) !!}
    </div>
</div>
<div class="field">
    <label>{!! Form::label('Изобаржение вопроса') !!}</label>
    <div>
        @if ($item->image)
            <img src="/images/{!! $item->image !!}"/>
        @endif
        {!! Form::file('image', []) !!}
    </div>
</div>

<div class="four fields">
    <div class="field{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Билет') !!}</label>
        <div class="">
            {!! Form::select('ticket_id',$tickets_list, $item->ticket_id ?: $preset['ticket_id'] ,['class'=>'ui dropdown fluid']) !!}
        </div>
    </div>

    <div class="field{{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Категория') !!}</label>
        <div class="">
            {!! Form::select('category_id',$categories, null,['class'=>'ui dropdown fluid']) !!}
        </div>
    </div>

    <div class="field{{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Тема') !!}</label>
        <div class="">
            {!! Form::select('subject_id', $subjects, null,['class'=>'ui dropdown fluid']) !!}
        </div>
    </div>

    <div class="field{{ $errors->has('n') ? ' has-error' : '' }}">
        <label class="">{!! Form::label('Номер') !!}</label>
        <div class="">
            {!! Form::number('n', $item->n ?: $preset['last_item'], ['class'=>'ui']) !!}
        </div>
    </div>
</div>

@if($item->has('withHint'))
    @foreach($item->withHint as $hint)
    <div class="fields">
        <div class="fourteen field wide ui search">
            <div class="ui left icon fluid input">
                {!! Form::text('reference[text]', $hint->text ?: null, ['class'=>'prompt fluid']) !!}
                <i class="search icon"></i>
            </div>
        </div>
        <div class="two wide field reference_id">
            {!! Form::text('reference[id]', $hint->id ?: null) !!}
        </div>
    </div>
    @endforeach
@else
    add a hint
@endif
<div class=" admin__question_answers">
    @if(count($item->answers) > 0)
        <div class="ui divider"></div>
        @foreach($item->answers as $k => $answer)
        <label data-id="{{$answer->id}}" class=" fields answer__item @if((bool)$answer->right === true) answer__item-active @endif" for="answer_{{$k}}">
            <div class="ui container" style="position:relative;">
                <i class="icon close deleteAnswer" style="position:absolute; top:-3px;right: 0px;"></i>
                {!! Form::checkbox('answers[' . $k . '][right]', 'right', (bool)$answer->right, ['id' => 'answer_' . $k, 'style' => 'opacity:0; height:0px; margin:0px;padding:0px;']) !!}
                {!! Form::textarea('answers[' . $k . '][answer] ', $answer->answer, ['class' => 'ui']) !!}
            </div>
        </label>
        <div class="ui divider"></div>
        @endforeach
    @endif
</div>
<div class="container" id="questionPanel">
    <span class="ui left icon button " id="addAnswer">
        <i class="plus icon"></i> Добавить ответ
    </span>
    {!! Form::submit('Сохранить', ['class' => 'ui submit button right floated']) !!}
</div>