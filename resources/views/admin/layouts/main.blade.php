
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">


    <title>Laravel</title>
    <link rel="stylesheet" href="{{ elixir('css/admin.css') }}">
</head>
<body>
<div class="ui borderless main menu">
    <div class="ui  container">
        <div href="#" class="header item">
            <img class="logo" src="assets/images/logo.png">
            Project Name
        </div>
        <a href="#" class="item">Blog</a>
        <a href="#" class="item">Articles</a>
        <a href="#" class="ui right floated dropdown item">
            Dropdown <i class="dropdown icon"></i>
            <div class="menu">
                <div class="item">Link Item</div>
                <div class="item">Link Item</div>
                <div class="divider"></div>
                <div class="header">Header Item</div>
                <div class="item">
                    <i class="dropdown icon"></i>
                    Sub Menu
                    <div class="menu">
                        <div class="item">Link Item</div>
                        <div class="item">Link Item</div>
                    </div>
                </div>
                <div class="item">Link Item</div>
            </div>
        </a>
    </div>
</div>
<div class="ui ">
    <div class="ui vertical segment">
        <div class="ui aligned ">
            <div class="ui stackable equal divided grid ">
                @if(count($menu) > 0)
                <div class="four wide equal column">
                    <div class="ui segment">
                        <div class="ui relaxed divided list">
                            @foreach($menu as $k => $v)
                            <div class="item">
                                @if(array_key_exists('icon', $v))
                                    <i class="large {{$v['icon']}} middle aligned icon"></i>
                                @endif

                                <div class="content">
                                    <a href="{{$v['url']}}" class="header">{{$v['title']}}</a>
                                </div>
                            </div>
                           @endforeach
                        </div>
                    </div>
                </div>
                @endif
                @if(count($menu) > 0)
                <div class="twelve wide column">
                @else
                <div class="sixteen wide column">
                @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
</body>
@include('admin._blocks.footer')
<script src="{{ elixir('js/admin.js') }}"></script>
</html>
